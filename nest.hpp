#pragma once

#include "./triple.hpp"
#include "./uses.hpp"
#include "./duo.hpp"

namespace pstd {
  template <typename ...A>
  class nested;

  template <>
  class nested<> {
    public:
      template <typename T>
      nested& operator=(const T&) { // No-op for the base case
        return *this;
      }
  };

  template <typename H, typename ...T>
  class nested<H, T...> : public nested<T...> {
    protected:
      H it;

      template <is::iterator I>
      void operator_equals(I iter, I last) {
        if (iter != last) { 
          it = *iter;

          if constexpr (sizeof ...(T) > 0)
            nested<T...>::operator_equals(
              std::next(iter),
              last
            );
        }
      }

    public:
      nested(H&& h, T&&... t): nested<T...>(std::forward<T>(t)...), it(std::forward<H>(h)) {

      }

      nested() = default;

      // ---

      template <typename T1, typename T2, typename T3>
      nested& operator=(const triplet<T1, T2, T3> &input) {
        nested<T...>::operator=(duo<T2, T3>{input.b, input.c});
        it = input.a;
        return *this;
      }

      // ---

      template <typename T1, typename T2>
      nested& operator=(const duo<T1, T2> &input) {
        if constexpr (sizeof...(T) > 0)
          nested<T...>::it = input.b;

        it = input.a;
        return *this;
      }

      // ---

      template <uses::iteration X>
      nested& operator=(const X &input) {
        operator_equals(
          input.begin(),
          input.end()
        );

        return *this;
      }
  
      // ---

      template <std::size_t I>
      auto& get() {
        if constexpr (I == 0)
          return it;

        else
          return nested<T...>::template get<I - 1>();
      }

      template <std::size_t I>
      const auto& get() const {
        if constexpr (I == 0)
          return it;
          
        else
          return nested<T...>::template get<I - 1>();
      }
  };

  // ---

  template <typename... A>
  nested<A&...> nest(A&... args) {
    return nested<A&...>(args...);
  }

  template <typename... A>
  nested<A...> make_nested(A&&... args) {
    return nested<A...>(std::forward<A>(args)...);
  }
}