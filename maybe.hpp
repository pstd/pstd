#pragma once

#include "./value.hpp"

namespace pstd {
  template <typename T>
  class maybe {
    private:
      class value it;

      // ---

      using const_reference = const T&;
      using const_pointer = const T*;
      using reference = T&;
      using pointer = T*;

    public:
      pstd_error(logic_error);

      // ---

      maybe(T input): it(input) {

      }

      maybe() = default;

      // ---

      operator bool() const {
        return !empty();
      }

      // ---

      const_pointer operator->() const {
        if (!empty())
          return &it.template cast<T>();

        return nullptr;
      }

      pointer operator->() {
        if (!empty())
          return &it.template cast<T>();

        return nullptr;
      }

      const_reference operator*() const {
        if (empty())
          throw logic_error("dereferencing an empty maybe");

        return it.template cast<T>();
      }

      reference operator*() {
        if (empty())
          throw logic_error("dereferencing an empty maybe");

        return it.template cast<T>();
      }

      // ---

      bool operator==(const T& value) const {
        if (empty())
          return !1;

        return it.template cast<T>() == value;
      }

      bool operator!=(const T& value) const {
        return !(*this == value);
      }

      // ---

      bool empty() const {
        return it.empty();
      }

      T value() const {
        if (empty())
          throw logic_error("accessing value of an empty maybe");

        return it.template cast<T>();
      }
  };
}