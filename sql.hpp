#pragma once

#include "./pointer/basic.hpp"
#include "./web/url.hpp"
#include "./variant.hpp"
#include "./vector.hpp"

#include <mariadb/conncpp.hpp>
#include <mutex>

namespace pstd::sql {
  static bool __sql_reporting = !1;

  void reporting(bool enabled = !0) {
    __sql_reporting = enabled;
  }

  // ---

  struct column {
    public:
      text name;
      text type;
      bool primary_key = !1;
      bool foreign_key = !1;
      text references;
  };

  using schema = map<text, vector<column>>;

  // ---

  class result {
    friend class database;

    private:
      basic_pointer<::sql::Statement> state; // needed to keep the connection alive
      basic_pointer<::sql::ResultSet> it;

      // ---

      result(::sql::Statement* state, text query): state(state) {
        it = state->executeQuery(query.raw());
        
        if (__sql_reporting)
          printf("%s\n", query.raw());      }

      result() = default;

      // ---

    public:
      bool next() {
        return it->next();
      }

      // ---

      template <typename T>
      requires type_allowed_v<T, int8_t, int16_t, int32_t, int64_t, uint8_t, uint16_t, uint32_t, uint64_t, float, double, bool, text>
      T value(text input) {
        if constexpr (std::is_same_v<T, int8_t>)
          return it->getByte(input.raw());
          
        else if constexpr (std::is_same_v<T, int16_t>)
          return it->getShort(input.raw());

        else if constexpr (std::is_same_v<T, int32_t>)
          return it->getInt(input.raw());

        else if constexpr (std::is_same_v<T, int64_t>)
          return it->getInt64(input.raw());

        else if constexpr (std::is_same_v<T, uint8_t>)
          return it->getUInt(input.raw());

        else if constexpr (std::is_same_v<T, uint16_t>)
          return it->getUInt(input.raw());

        else if constexpr (std::is_same_v<T, uint32_t>)
          return it->getUInt(input.raw());

        else if constexpr (std::is_same_v<T, uint64_t>)
          return it->getUInt64(input.raw());

        else if constexpr (std::is_same_v<T, float>)
          return it->getFloat(input.raw());

        else if constexpr (std::is_same_v<T, double>)
          return it->getDouble(input.raw());

        else if constexpr (std::is_same_v<T, text>)
          return it->getString(input.raw()).c_str();

        else if constexpr (std::is_same_v<T, bool>)
          return it->getBoolean(input.raw());
      }

      template <typename T>
      requires type_allowed_v<T, int8_t, int16_t, int32_t, int64_t, uint8_t, uint16_t, uint32_t, uint64_t, float, double, bool, text>
      T value(int32_t column) {
        if constexpr (std::is_same_v<T, int8_t>)
          return it->getByte(column);

        else if constexpr (std::is_same_v<T, int16_t>)
          return it->getShort(column);

        else if constexpr (std::is_same_v<T, int32_t>)
          return it->getInt(column);

        else if constexpr (std::is_same_v<T, int64_t>)
          return it->getInt64(column);

        else if constexpr (std::is_same_v<T, uint8_t>)
          return it->getUInt(column);

        else if constexpr (std::is_same_v<T, uint16_t>)
          return it->getUInt(column);

        else if constexpr (std::is_same_v<T, uint32_t>)
          return it->getUInt(column);

        else if constexpr (std::is_same_v<T, uint64_t>)
          return it->getUInt64(column);

        else if constexpr (std::is_same_v<T, float>)
          return it->getFloat(column);

        else if constexpr (std::is_same_v<T, double>)
          return it->getDouble(column);

        else if constexpr (std::is_same_v<T, text>)
          return text(it->getString(column));

        else if constexpr (std::is_same_v<T, bool>)
          return it->getBoolean(column);
      }
  };

  // ---

  class database {
    private:
      basic_pointer<::sql::Connection> it;
      std::recursive_mutex mtx;

    public:
      database(text ip_address, uint16_t port, text username, text password) {
        auto driver = ::sql::mariadb::get_driver_instance();

        // ---

        ::sql::Properties properties({
          {"user", username.raw()},
          {"password", password.raw()}
        });

        auto dest = web::url::create({
          scheme: "jdbc:mariadb",
          host: ip_address,
          port: port,
        });

        it = driver->connect(
          dest.raw(),
          properties
        );
      }

      // ---

      template <typename ...A>
      result query(text input, A ...args) {
        std::lock_guard<std::recursive_mutex> lock(mtx);

        if (sizeof...(A) > 0)
          input = input.format(args ...);

        try {
          return result(
            it->createStatement(),
            input
          );
        }

        catch (::sql::SQLException &e) {
          if (__sql_reporting)
            printf("query error: %s query (%s)\n", e.what(), input.raw());

          return {};
        }
      }

      template <typename ...A>
      bool run(text input, A ...args) {
        try {
          query(input, args ...);
          return !0;
        }

        catch (::sql::SQLException &e) {
          if (__sql_reporting)
            printf("run error: %s\n", e.what());

          return !1;
        }
      }

      // ---

      auto create_table(const schema::value_type::a_type name, schema::value_type::b_type &columns) {
        text out("CREATE TABLE %s (");

        out.format(name);

        // ---

        vector<text> foreign_keys;

        each(columns, [&](uint i, column &next) -> void {
          if (i != 0)
            out += ", ";

          out += text("%s %s")
            .format(next.name, next.type);

          if (next.primary_key)
            out += " PRIMARY KEY";

          if (next.foreign_key)
            foreign_keys.push_back(
              text("FOREIGN KEY (%s) REFERENCES %s")
                .format(next.name, next.references)
            );
        });

        each(foreign_keys, [&](text &next) -> void {
          out += text(", %s").format(next);
        });

        out += ");";

        return run(out);
      }

      auto drop_table(const schema::value_type::a_type name) {
        text out("DROP TABLE %s;");

        out.format(name);

        return run(out);
      }

      // ---

      using whereables = variant<
        int8_t,
        int16_t,
        int32_t,
        int64_t,
        float,
        double,
        long double,
        uint8_t,
        uint16_t,
        uint32_t,
        text,
        bool,
        std::nullptr_t
      >;

      result select(const schema::value_type::a_type name, map<text, text> columns = {}, triple<text, text, whereables> conditions = {}) {
        text out("SELECT ");

        // Add column names with aliases if specified
        if (!columns.empty())
          each(columns, [&](uint i, duo<text, text> &next) -> void {
            if (i != 0)
              out += ", ";

            // alias
            if (!next.b.empty())
              out += text("%s AS %s").format(next.a, next.b);

            else
              out += next.a;
          });
        
        else
          out += "*";  // If no columns are specified, select all columns

        // ---

        out += text(" FROM %s").format(name);

        // ---

        if (!conditions.empty()) {
          out += " WHERE ";

          each(conditions, [&](uint i, triplet<text, text, whereables> &next) -> void {
            if (i != 0)
              out += " AND ";

            // Handle the whereables variant and generate the correct SQL condition
            out += text("%s %s ")
              .format(next.a, next.b);

            next.c.visit([&](auto&& value) {
              using T = std::decay_t<decltype(value)>;

              if constexpr (type_allowed_v<T, int8_t, int16_t, int32_t, int64_t, uint8_t, uint16_t, uint32_t, uint64_t, float, double, long double>)
                out += text("%d").format(value);

              else if constexpr (std::is_same_v<T, bool>)
                out += value ? "true" : "false";

              else if constexpr (std::is_same_v<T, std::nullptr_t>)
                out += "NULL";

              else
                out += text("\"%s\"").format(
                  text::replace(value, '"', "\\\"")
                );
            });
          });
        }

        // ---

        out += ";";

        return query(out);
      }
  
      auto insert(const schema::value_type::a_type name, map<text, whereables> data) {
        text out("INSERT INTO %s (");
        
        out.format(name);

        vector<text> columns;
        vector<text> values;

        // Populate the columns and values vectors
        each(data, [&](uint, duo<text, whereables> &next) -> void {
          columns.push_back(next.a);

          // Handle the whereables variant to append the correct value representation
          next.b.visit([&](auto&& value) {
            using T = std::decay_t<decltype(value)>;

            if constexpr (type_allowed_v<T, int8_t, int16_t, int32_t, int64_t, uint8_t, uint16_t, uint32_t, uint64_t, float, double, long double>)
              values.push_back(
                text("%d").format(value)
              );

            else if constexpr (std::is_same_v<T, bool>)
              values.push_back(value ? "true" : "false");

            else if constexpr (std::is_same_v<T, std::nullptr_t>)
              values.push_back("NULL");

            else
              values.push_back(
                text("\"%s\"").format(
                  text::replace(value, '"', "\\\"")
                )
              );
          });
        });

        // Add columns to the query
        each(columns, [&](uint i, text &col) -> void {
          if (i != 0)
            out += ", ";

          out += col;
        });

        out += ") VALUES (";

        each(values, [&](uint i, text &val) -> void {
          if (i != 0)
            out += ", ";
            
          out += val;
        });

        out += ");";

        return run(out);
      }
  
      // delete
      auto remove(const schema::value_type::a_type name, triple<text, text, whereables> conditions = {}) {
        text out("DELETE FROM %s");
        
        out.format(name);

        // ---

        if (!conditions.empty()) {
          out += " WHERE ";

          each(conditions, [&](uint i, triplet<text, text, whereables> &next) -> void {
            if (i != 0)
              out += " AND ";

            // Handle the whereables variant and generate the correct SQL condition
            out += text("%s %s ")
              .format(next.a, next.b);

            next.c.visit([&](auto&& value) {
              using T = std::decay_t<decltype(value)>;

              if constexpr (type_allowed_v<T, int8_t, int16_t, int32_t, int64_t, uint8_t, uint16_t, uint32_t, uint64_t, float, double, long double>)
                out += text("%d").format(value);

              else if constexpr (std::is_same_v<T, bool>)
                out += value ? "true" : "false";

              else if constexpr (std::is_same_v<T, std::nullptr_t>)
                out += "NULL";

              else
                out += text("\"%s\"").format(
                  text::replace(value, '"', "\\\"")
                );
            });
          });
        }

        // ---

        out += ";";

        return run(out);
      }
  
      auto update(const schema::value_type::a_type name, map<text, whereables> data, triple<text, text, whereables> conditions = {}) {
        text out("UPDATE %s SET ");
        
        out.format(name);

        vector<text> columns;

        // Populate the columns vector
        each(data, [&](uint, duo<text, whereables> &next) -> void {
          // Handle the whereables variant to append the correct value representation
          next.b.visit([&](auto&& value) {
            using T = std::decay_t<decltype(value)>;

            if constexpr (type_allowed_v<T, int8_t, int16_t, int32_t, int64_t, uint8_t, uint16_t, uint32_t, uint64_t, float, double, long double>)
              columns.push_back(
                text("%s = %d")
                  .format(next.a, value)
              );

            else if constexpr (std::is_same_v<T, bool>)
              columns.push_back(
                text("%s = %s")
                  .format(next.a, value ? "true" : "false")
              );

            else if constexpr (std::is_same_v<T, std::nullptr_t>)
              columns.push_back(
                text("%s = NULL")
                  .format(next.a)
              );

            else
              columns.push_back(
                text("%s = \"%s\"")
                  .format(
                    next.a,
                    text::replace(value, '"', "\\\"")
                  )
              );
          });
        });

        // Add columns to the query
        each(columns, [&](uint i, text &col) -> void {
          if (i != 0)
            out += ", ";

          out += col;
        });

        // ---

        if (!conditions.empty()) {
          out += " WHERE ";

          each(conditions, [&](uint i, triplet<text, text, whereables> &next) -> void {
            if (i != 0)
              out += " AND ";

            // Handle the whereables variant and generate the correct SQL condition
            out += text("%s %s ")
              .format(next.a, next.b);

            next.c.visit([&](auto&& value) {
              using T = std::decay_t<decltype(value)>;

              if constexpr (type_allowed_v<T, int8_t, int16_t, int32_t, int64_t, uint8_t, uint16_t, uint32_t, uint64_t, float, double, long double>)
                out += text("%d").format(value);

              else if constexpr (std::is_same_v<T, bool>)
                out += value ? "true" : "false";

              else if constexpr (std::is_same_v<T, std::nullptr_t>)
                out += "NULL";

              else
                out += text("\"%s\"").format(
                  text::replace(value, '"', "\\\"")
                );
            });
          });
        }


        // ---

        out += ";";

        return run(out);
      }
  };
}