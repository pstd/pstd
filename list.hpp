#pragma once

#include "./error.hpp"

#include <initializer_list>

namespace pstd {
  template <typename T>
  class list {
    private:
      struct node {
        public:
          T it;
          node* prev;
          node* next;

          node(const T& value) : it(value), prev(nullptr), next(nullptr) {

          }
      };

      std::size_t len;
      node* head;
      node* tail;

      // ---

      void copy_from(const list& other) {
        for (node* current = other.head; current != nullptr; current = current->next)
          push_back(current->it);
      }

      void move_from(list&& other) noexcept {
        head = other.head;
        tail = other.tail;
        len = other.len;

        other.head = other.tail = nullptr;
        other.len = 0;
      }

    public:
      pstd_error(out_of_range);

      using value_type = T;

      // ---

      list(std::initializer_list<value_type> init): head(nullptr), tail(nullptr), len(0) {
        for (auto next : init)
          push_back(next);
      }

      list(): head(nullptr), tail(nullptr), len(0) {

      }

      ~list() {
        clear();
      }

      // ---

      list(list&& other) noexcept : head(other.head), len(other.len) {
        move_from(std::move(other));
      }

      list(const list& other): head(nullptr), len(0) {
        copy_from(other);
      }

      // ---

      list& operator=(const list& other) {
        if (this != &other) {
          clear();
          copy_from(other);
        }

        return *this;
      }

      list& operator=(list&& other) noexcept {
        if (this != &other) {
          clear();
          move_from(std::move(other));
        }

        return *this;
      }

      // ---

      T& operator[](std::size_t index) const {
        return at(index);
      }

      // ---

      void push_back(const T& value) {
        node* new_node = new node(value);

        if (!head)
          head = tail = new_node;
        
        else {
          tail->next = new_node;
          new_node->prev = tail;
          tail = new_node;
        }

        ++len;
      }

      void push_front(const T& value) {
        node* new_node = new node(value);

        if (!head)
          head = tail = new_node;
        
        else {
          new_node->next = head;
          head->prev = new_node;
          head = new_node;
        }

        ++len;
      }

      // ---

      T& back() const {
        if (!tail)
          throw out_of_range("list is empty");
        
        return tail->it;
      }

      T& front() const {
        if (!head)
          throw out_of_range("list is empty");
        
        return head->it;
      }

      // ---

      void pop_back() {
        if (!tail)
          throw out_of_range("list is empty");

        node* to_delete = tail;

        tail = tail->prev;

        if (tail)
          tail->next = nullptr;

        else
          head = nullptr;

        delete to_delete;
        --len;
      }

      void pop_front() {
        if (!head)
          throw out_of_range("list is empty");

        node* to_delete = head;

        head = head->next;

        if (head)
          head->prev = nullptr;

        else
          tail = nullptr;

        delete to_delete;
        --len;
      }

      // ---

      T pull_back() {
        auto out = back();
        pop_back();
        return out;
      }

      T pull_front() {
        auto out = front();
        pop_front();
        return out;
      }

      // ---

      std::size_t size() const {
        return len;
      }

      bool empty() const {
        return len == 0;
      }

      void clear() {
        while (head)
          pop_front();
      }

      T& at(std::size_t index) const {
        if (index >= len)
          throw out_of_range("index out of range");

        node* current = head;
        
        for (std::size_t i = 0; i < index; ++i)
          current = current->next;

        return current->it;
      }

      bool contains(const T& value) const {
        node* current = head;

        while (current) {
          if (current->it == value)
            return !0;

          current = current->next;
        }

        return !1;
      }

      // ---

      class iterator {
        friend class list;

        private:
          node* current;

        public:
          using iterator_category = std::bidirectional_iterator_tag;
          using difference_type = std::ptrdiff_t;
          using value_type = T;
          using reference = T&;
          using pointer = T*;

          // ---

          explicit iterator(node* node): current(node) {

          }

          T& operator*() const {
            return current->it;
          }

          iterator& operator++() {
            if (current)
              current = current->next;

            return *this;
          }

          iterator operator++(int) {
            iterator temp = *this;
            ++(*this);
            return temp;
          }

          iterator& operator--() {
            if (current)
              current = current->prev;

            return *this;
          }

          iterator operator--(int) {
            iterator temp = *this;
            --(*this);
            return temp;
          }

          bool operator==(const iterator& other) const {
            return current == other.current;
          }

          bool operator!=(const iterator& other) const {
            return !(*this == other);
          }
      };

      // ---

      iterator erase(iterator pos) {
        if (!pos.current)
          throw out_of_range("invalid iterator");

        node* to_delete = pos.current;

        iterator prev_iter(to_delete->prev);

        if (to_delete->prev)
          to_delete->prev->next = to_delete->next;

        else
          head = to_delete->next;

        if (to_delete->next)
          to_delete->next->prev = to_delete->prev;

        else
          tail = to_delete->prev;

        delete to_delete;

        --len;

        return prev_iter;
      }

      // ---

      iterator begin() const {
        return iterator(head);
      }

      iterator end() const {
        return iterator(nullptr);
      }

      iterator insert(iterator pos, const T& input) {
        if (!pos.current && pos != end())
          throw out_of_range("invalid iterator");

        node* new_node = new node(input);
        node* current = pos.current;

        if (current) {
          new_node->next = current;
          new_node->prev = current->prev;

          if (current->prev)
            current->prev->next = new_node;

          else
            head = new_node;

          current->prev = new_node;
        }
        
        else {
          if (tail) {
            tail->next = new_node;
            new_node->prev = tail;
            tail = new_node;
          }
          
          else
            head = tail = new_node;
        }

        ++len;
        return iterator(new_node);
      }

      // ---

      class reverse_iterator {
        private:
          node* current;

        public:
          explicit reverse_iterator(node* node): current(node) {

          }

          T& operator*() {
            return current->it;
          }

          reverse_iterator& operator++() {
            if (current)
              current = current->prev;

            return *this;
          }

          reverse_iterator& operator--() {
            if (current)
              current = current->next;

            return *this;
          }

          bool operator!=(const reverse_iterator& other) const {
            return current != other.current;
          }
      };

      // ---

      reverse_iterator rbegin() const {
        return reverse_iterator(tail);
      }

      reverse_iterator rend() const {
        return reverse_iterator(nullptr);
      }

      // ---

      list reverse() const {
        list out;
        
        for (node* next = tail; next != nullptr; next = next->prev)
          out.push_back(next->it);

        return out;
      }
  };
}