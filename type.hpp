#pragma once

#include <typeindex>
#include <utility>

namespace pstd {
  template <typename ...A>
  struct types;

  // ---

  template <std::size_t I, typename... L>
  struct type_at;

  // Specialization for parameter pack
  template <typename F, typename... R>
  struct type_at<0, F, R...> {
    using type = F;
  };

  template <std::size_t I, typename F, typename... R>
  struct type_at<I, F, R...> {
    using type = typename type_at<I - 1, R...>::type;
  };

  // Specialization for types
  template <std::size_t I, typename... A>
  struct type_at<I, types<A...>> {
    static_assert(I < sizeof...(A), "index out of bounds");
    using type = typename type_at<I, A...>::type;
  };

  template <std::size_t I, typename L>
  using type_at_t = typename type_at<I, L>::type;

  // ---

  template <typename T, typename... L>
  struct type_index;

  template <typename T>
  struct type_index<T> {
    static constexpr std::ptrdiff_t value = -1; // Type not found
  };

  template <typename T, typename F, typename... R>
  struct type_index<T, F, R...> {
    static constexpr std::ptrdiff_t value = (
      std::is_same_v<T, F> ? 0 : (type_index<T, R...>::value == -1 ? -1 : 1 + type_index<T, R...>::value)
    );
  };

  // Specialization for types
  template <typename T, typename... L>
  struct type_index<T, types<L...>> : type_index<T, L...> {

  };

  template <typename T, typename... L>
  constexpr std::ptrdiff_t type_index_v = type_index<T, L...>::value;

  // ---

  // Type allowed
  template <typename T, typename... L>
  struct type_allowed {
    static constexpr bool value = type_index_v<T, L...> != -1;
  };

  // Specialization for types
  template <typename T, typename... L>
  struct type_allowed<T, types<L...>> : type_allowed<T, L...> {

  };

  template <typename T, typename... L>
  constexpr bool type_allowed_v = type_allowed<T, L...>::value;

  // ---

  // Count type occurrences
  template <typename T, typename... L>
  struct count_type;

  template <typename T>
  struct count_type<T> {
    static constexpr std::size_t value = 0;
  };

  template <typename T, typename F, typename... R>
  struct count_type<T, F, R...> {
    static constexpr std::size_t value = std::is_same_v<T, F> + count_type<T, R...>::value;
  };

  // Specialization for types
  template <typename T, typename... L>
  struct count_type<T, types<L...>> : count_type<T, L...> {

  };

  template <typename T, typename... L>
  constexpr std::size_t count_type_v = count_type<T, L...>::value;
  
  // ---

  // Check for no duplicates
  template <typename... L>
  struct no_duplicates;

  template <typename F, typename... R>
  struct no_duplicates<F, R...> {
    static_assert(count_type_v<F, R...> == 0, "duplicate type detected");
    no_duplicates<R...> next;
  };

  template <typename... L>
  struct no_duplicates<types<L...>> : no_duplicates<L ...> {

  };

  template <>
  struct no_duplicates<> {

  };

  // ---

  class type {
    private:
      std::type_index ind;

    public:
      type(const std::type_info& info): ind(info) {

      }

      template <typename T>
      static type create() {
        return type(typeid(T));
      }

      // ---

      // Equality operator
      bool operator==(const std::type_info& info) const {
        return ind == std::type_index(info);
      }

      // Inequality operator
      bool operator!=(const std::type_info& info) const {
        return ind != std::type_index(info);
      }

      // Equality operator for pstd::type
      bool operator==(const type& other) const {
        return ind == other.ind;
      }

      // Inequality operator for pstd::type
      bool operator!=(const type& other) const {
        return ind != other.ind;
      }

      // ---

      auto name() const {
        return ind.name();
      }
  };
}