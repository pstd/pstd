#pragma once

#include <concepts>

namespace pstd::uses {
  template <typename T>
  concept front = requires(T t) {
    { t.front() } -> std::same_as<typename T::value_type &>;
  };

  template <typename T>
  concept pop_front = requires(T t) {
    { t.pop_front() } -> std::same_as<void>;
  };

  template <typename T>
  concept size = requires(T t) {
    { t.size() } -> std::same_as<std::size_t>;
  };

  // ---

  template <typename T>
  concept iterator = requires {
    typename T::iterator;
  };

  template <typename T>
  concept const_iterator = requires {
    typename T::const_iterator;
  };

  // ---

  template <typename T>
  concept begin = requires(T t) {
    { t.begin() } -> std::same_as<typename T::iterator>;
  };

  template <typename T>
  concept end = requires(T t) {
    { t.end() } -> std::same_as<typename T::iterator>;
  };

  // ---

  template <typename T>
  concept cbegin = requires(T t) {
    { t.cbegin() } -> std::same_as<typename T::const_iterator>;
  };

  template <typename T>
  concept cend = requires(T t) {
    { t.cend() } -> std::same_as<typename T::const_iterator>;
  };

  // ---

  template <typename T>
  concept erase = requires(T t, T::iterator iter) {
    { t.erase(iter) } -> std::same_as<typename T::iterator>;
  };

  template <typename T>
  concept push_back = requires(T t, typename T::value_type value) {
    { t.push_back(value) } -> std::same_as<void>;
  };

  template <typename T>
  concept length = requires(T t) {
    { t.length() } -> std::same_as<std::size_t>;
  };

  // ---

  template <typename T>
  concept iteration = iterator<T> && begin<T> && end<T>;

  template <typename T>
  concept const_iteration = const_iterator<T> && cbegin<T> && cend<T>;

  // ---

  template <typename T>
  concept equality_comparable = requires(T a, T b) {
    { a == b } -> std::same_as<bool>;
  };

  // Add concept for operator!=
  template <typename T>
  concept inequality_comparable = requires(T a, T b) {
    { a != b } -> std::same_as<bool>;
  };

  template <typename T>
  concept indexing_operator = requires(T t, std::size_t i) {
    { t[i] } -> std::same_as<typename T::value_type>;
  };
}