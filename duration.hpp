#pragma once

#include <chrono>
#include <ctime>

namespace pstd {
  class duration {
    private:
      std::chrono::milliseconds dur;

      template <typename D>
      static duration with(double value) {
        duration out;

        out.dur = std::chrono::duration_cast<std::chrono::milliseconds>(
          std::chrono::duration<double>(value)
        );

        out.dur *= D::period::num;

        out.dur /= static_cast<double>(D::period::den);

        return out;
      }

    public:
      static duration milliseconds(double value) {
        return with<std::chrono::milliseconds>(value);
      }

      static duration seconds(double value) {
        return with<std::chrono::seconds>(value);
      }

      static duration minutes(double value) {
        return with<std::chrono::minutes>(value);
      }

      static duration hours(double value) {
        return with<std::chrono::hours>(value);
      }

      // ---

      explicit duration(std::chrono::milliseconds d): dur(d) {

      }

      duration(): dur(std::chrono::milliseconds(0)) {

      }

      // ---

      std::chrono::milliseconds operator*() const {
        return dur;
      }

      // ---

      bool operator==(const duration& other) const {
        return dur == other.dur;
      }

      bool operator!=(const duration& other) const {
        return dur != other.dur;
      }

      bool operator<(const duration& other) const {
        return dur < other.dur;
      }

      bool operator<=(const duration& other) const {
        return dur <= other.dur;
      }

      bool operator>(const duration& other) const {
        return dur > other.dur;
      }

      bool operator>=(const duration& other) const {
        return dur >= other.dur;
      }

      // ---

      duration& operator+=(const duration& other) {
        dur += other.dur;
        return *this;
      }

      duration& operator-=(const duration& other) {
        dur -= other.dur;
        return *this;
      }

      // ---

      template <typename R, typename P>
      duration& operator+=(const std::chrono::duration<R, P>& other) {
        dur += std::chrono::duration_cast<std::chrono::milliseconds>(other);
        return *this;
      }

      template <typename R, typename P>
      duration& operator-=(const std::chrono::duration<R, P>& other) {
        dur -= std::chrono::duration_cast<std::chrono::milliseconds>(other);
        return *this;
      }

      template <typename R, typename P>
      duration operator+(const std::chrono::duration<R, P>& other) const {
        return duration(dur + std::chrono::duration_cast<std::chrono::milliseconds>(other));
      }

      template <typename R, typename P>
      duration operator-(const std::chrono::duration<R, P>& other) const {
        return duration(dur - std::chrono::duration_cast<std::chrono::milliseconds>(other));
      }

      // ---

      template <typename D = std::chrono::milliseconds>
      auto count() const {
        if constexpr (std::is_same_v<D, std::chrono::milliseconds>)
          return dur.count();

        else
          return std::chrono::duration_cast<D>(dur).count();
      }

      // ---

      int hours() const {
        return std::chrono::duration_cast<std::chrono::hours>(dur).count();
      }
    
      int minutes() const {
        return std::chrono::duration_cast<std::chrono::minutes>(dur).count() % 60;
      }
    
      int seconds() const {
        return std::chrono::duration_cast<std::chrono::seconds>(dur).count() % 60;
      }
  };

  class timestamp {
    private:
      std::chrono::milliseconds t;

    public:
      static timestamp now() {
        timestamp out;

        out.t = std::chrono::duration_cast<std::chrono::milliseconds>(
          std::chrono::system_clock::now().time_since_epoch()
        );

        return out;
      }

      // ---

      duration operator-(const timestamp& other) const {
        return duration(t - other.t);
      }
  };
}