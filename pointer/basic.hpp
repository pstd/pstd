#pragma once

// Copyright (C) 2024 Dave Perry (dbdii407)

#include "./../error.hpp"

#include <utility>
#include <atomic>

namespace pstd {
  template <typename T>
  class basic_pointer {
    template <typename U>
    friend class basic_pointer;

    public:
      using difference_type = std::ptrdiff_t;
      using size_type = std::size_t;
      using value_type = T;

      using const_reference = const T&;
      using const_iterator = const T*;
      using const_pointer = const T*;
      using reference = T&;
      using pointer = T*;

      // ---

      explicit basic_pointer(T* ptr): it(ptr), i(new std::atomic<size_type>(1)) {

      }

      basic_pointer(std::nullptr_t): it(nullptr), i(nullptr) {

      }

      // Default constructor
      basic_pointer(): it(nullptr), i(nullptr) {

      }

      // Destructor
      ~basic_pointer() {
        decrement();

        if (i && i->load() == 0)
          remove_pointers();
      }

      // ---

      // Constructor for derived-to-base conversions
      template <typename U>
      requires std::is_base_of_v<T, U>
      basic_pointer(const basic_pointer<U>& other): it(static_cast<T*>(other.it)), i(other.i) {
        increment();
      }

      // Copy constructor
      basic_pointer(const basic_pointer& other): it(other.it), i(other.i) {
        increment();
      }

      // Copy assignment operator
      basic_pointer& operator=(const basic_pointer& other) {
        if (this != &other) {
          decrement();

          if (i && i->load() == 0)
            remove_pointers();

          it = other.it;
          i = other.i;

          increment();
        }

        return *this;
      }

      // Move constructor
      basic_pointer(basic_pointer&& other) noexcept : it(std::exchange(other.it, nullptr)), i(std::exchange(other.i, nullptr)) {

      }

      // Move assignment operator
      basic_pointer& operator=(basic_pointer&& other) noexcept {
        if (this != &other) {
          decrement();

          if (i && i->load() == 0)
            remove_pointers();

          it = std::exchange(other.it, nullptr);
          i = std::exchange(other.i, nullptr);
        }

        return *this;
      }

      basic_pointer& operator=(pointer ptr) {
        reset(ptr);
        return *this;
      }

      // ---

      // Dereference operators
      const_reference operator*() const {
        return *it;
      }
      
      reference operator*() {
        return *it;
      }

      // ---

      const_pointer operator->() const {
        return it;
      }

      pointer operator->() {
        return it;
      }

      // ---

      // Equality operators
      bool operator==(const basic_pointer& other) const {
        return it == other.it;
      }

      bool operator!=(const basic_pointer& other) const {
        return !(*this == other);
      }

      // ---

      // Boolean conversion
      explicit operator bool() const {
        return it != nullptr;
      }

      // ---

      // Raw pointer access
      const_pointer raw() const {
        return it;
      }
      
      pointer raw() {
        return it;
      }

      // ---

      // Check if empty
      bool empty() const {
        return it == nullptr;
      }

      // Reference count
      size_type count() const {
        return i ? i->load() : 0;
      }

      // Release the pointer this instance points to
      void reset(pointer ny = nullptr) {
        decrement();

        if (i && i->load() == 0)
          remove_pointers();

        i = ny ? new std::atomic<size_type>(1) : nullptr;
        it = ny;
      }

    private:
      std::atomic<size_type>* i;
      pointer it;

      void increment() {
        if (i)
          i->fetch_add(1, std::memory_order_relaxed);
      }

      void decrement() {
        if (i && i->fetch_sub(1, std::memory_order_acq_rel) == 1)
          remove_pointers();
      }

      void remove_pointers() {
        delete it;
        delete i;

        it = nullptr;
        i = nullptr;
      }
  };

  // Factory function for basic_pointer
  template <typename T, typename... Args>
  basic_pointer<T> make_pointer(Args&&... args) {
    return basic_pointer<T>(new T(std::forward<Args>(args)...));
  }
}
