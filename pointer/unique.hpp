#pragma once

#include "./../error.hpp"

namespace pstd {
  template <typename T>
  class unique_pointer {
    template <typename U>
    friend class unique_pointer;

    public:
      using difference_type = std::ptrdiff_t;
      using size_type = std::size_t;
      using value_type = T;

      using const_reference = const T&;
      using const_iterator = const T*;
      using const_pointer = const T*;
      using reference = T&;
      using pointer = T*;
    
      // Conversion constructor for derived types

      template <typename U>
      requires std::is_base_of_v<T, U>
      unique_pointer(unique_pointer<U>&& other) noexcept {
        it = other.release();  // Take ownership of the raw pointer
      }

      // --- Constructors

      unique_pointer(value_type input) {
        it = new value_type(input);
      }

      unique_pointer(pointer ptr): it(ptr) {

      }

      unique_pointer() : it(nullptr) {

      }

      // cannot be copied
      unique_pointer(const unique_pointer&) = delete;

      unique_pointer(unique_pointer&& other) noexcept {
        it = other.it;
        other.it = nullptr;
      }

      // Destructor
      ~unique_pointer() {
        reset();
      }

      // ---

      // cannot be copied
      unique_pointer& operator=(const unique_pointer&) = delete;

      unique_pointer& operator=(unique_pointer&& other) noexcept {
        if (this != &other) {
          reset();
          it = other.it;
          other.it = nullptr;
        }
        return *this;
      }

      // Access methods
      const_reference operator*() const {
        return *it;
      }

      reference operator*() {
        return *it;
      }

      const_pointer operator->() const {
        return it;
      }

      pointer operator->() {
        return it;
      }

      // ---

      operator bool() const {
        return !empty();
      }

      // ---

      const_pointer raw() const {
        return it;
      }

      pointer raw() {
        return it;
      }

      // ---

      bool empty() const {
        return it == nullptr;
      }

      // ---

      // Reset method
      void reset(pointer ptr = nullptr) {
        if (it)
          delete it;

        it = ptr;
      }

      // Release ownership of the pointer
      pointer release() {
        pointer temp = it;
        it = nullptr;
        return temp;
      }

      void swap(unique_pointer& other) noexcept {
        if (this != &other)
          std::swap(it, other.it);
      }

    private:
      pointer it;
  };

  template <typename T, typename... A>
  unique_pointer<T> make_unique(A&&... args) {
    return unique_pointer<T>(
      new T(std::forward<A>(args)...
    ));
  }
}