#pragma once

#include "./error.hpp"
#include "./is.hpp"

#include <initializer_list>
#include <memory>

namespace pstd {
  template <typename T>
  class vector {
    private:
      T* it;
      std::size_t len;
      std::size_t capac;

      void reshape(std::size_t new_capacity) {
        T* new_data = new T[new_capacity];

        for (std::size_t i = 0; i < len; ++i)
          new_data[i] = it[i];

        delete[] it;

        capac = new_capacity;
        it = new_data;
      }

    public:
      using value_type = T;
      
      pstd_error(out_of_range);

      // Constructor
      vector(): it(nullptr), len(0), capac(0) {

      }

      // Constructor with initial len
      explicit vector(std::size_t i, const T& value = T()): len(i), capac(i) {
        it = new T[capac];

        for (std::size_t i = 0; i < len; ++i)
          it[i] = value;
      }

      // Constructor from initializer list
      vector(std::initializer_list<T> init): len(init.size()), capac(init.size()) {
        it = new T[capac];

        std::size_t i = 0;

        for (const auto& val : init)
          it[i++] = val;
      }

      // Copy constructor
      vector(const vector& other) : len(other.len), capac(other.capac) {
        it = new T[capac];

        for (std::size_t i = 0; i < len; ++i)
          it[i] = other.it[i];
      }

      // Move constructor
      vector(vector&& other) noexcept : it(other.it), len(other.len), capac(other.capac) {
        other.it = nullptr;
        other.len = 0;
        other.capac = 0;
      }

      template <is::iterator I>
      vector(I first, I last) {
        len = std::distance(first, last);
        capac = len;
        it = new T[capac];

        std::size_t i = 0;
        
        for (auto iter = first; iter != last; ++iter)
          it[i++] = *iter;
      }

      // Destructor
      ~vector() {
        delete[] it;
      }

      // ---

      // Copy assignment
      vector& operator=(const vector& other) {
        if (this == &other)
          return *this;
        
        delete[] it;
        
        len = other.len;
        capac = other.capac;
        it = new T[capac];
        
        for (std::size_t i = 0; i < len; ++i)
          it[i] = other.it[i];

        return *this;
      }

      // Move assignment
      vector& operator=(vector&& other) noexcept {
        if (this == &other)
          return *this;
        
        delete[] it;

        it = other.it;
        len = other.len;
        capac = other.capac;

        other.it = nullptr;
        other.len = 0;
        other.capac = 0;

        return *this;
      }

      // ---

      // Access elements
      T& operator[](std::size_t index) {
        if (index >= len) {
          throw std::out_of_range("index out of range");
        }
        return it[index];
      }

      const T& operator[](std::size_t index) const {
        if (index >= len) {
          throw std::out_of_range("index out of range");
        }
        return it[index];
      }

      // ---

      T& at(std::size_t index) {
        if (index >= len)
          throw out_of_range("index out of range");

        return it[index];
      }

      const T& at(std::size_t index) const {
        if (index >= len)
          throw out_of_range("index out of range");

        return it[index];
      }

      // Get len
      std::size_t size() const {
        return len;
      }

      // Get capac
      std::size_t capacity() const {
        return capac;
      }

      // ---

      // Add element to the end
      void push_back(const T& value) {
        if (len == capac)
          reshape(capac == 0 ? 1 : capac * 2);

        it[len++] = value;
      }

      // Remove last element
      void pop_back() {
        if (len > 0)
          --len;
      }

      // Check if the vector is empty
      bool empty() const {
        return len == 0;
      }

      // Clear the vector
      void clear() {
        len = 0;
      }

      T* raw() {
        return it;
      }

      // Resize the vector
      void resize(std::size_t new_size, const T& value = T()) {
        if (new_size > capac)
          reshape(new_size);

        if (new_size > len)
          for (std::size_t i = len; i < new_size; ++i)
            it[i] = value;

        len = new_size;
      }

      // Remove element at position
      void erase(std::size_t index) {
        if (index >= len)
          throw std::out_of_range("index out of range");

        for (std::size_t i = index; i < len - 1; ++i)
          it[i] = it[i + 1];

        --len;
      }

      auto reverse() {
        for (std::size_t i = 0; i < len / 2; ++i)
          std::swap(it[i], it[len - i - 1]);

        return *this;
      }

      // ---

      class const_iterator;

      class iterator {
        friend class const_iterator;

        public:
          using iterator_category = std::random_access_iterator_tag;
          using difference_type = std::ptrdiff_t;
          using value_type = T;
          using reference = T&;
          using pointer = T*;

          // ---

          iterator(const const_iterator& other): ptr(const_cast<pointer>(other.ptr)) {

          }

          // Constructor
          explicit iterator(pointer p): ptr(p) {

          }

          // Dereference operator
          reference operator*() const {
            return *ptr;
          }

          // Arrow operator
          pointer operator->() {
            return ptr;
          }

          // Increment operator
          iterator& operator++() {
            ++ptr;
            return *this;
          }

          // Post-increment operator
          iterator operator++(int) {
            iterator tmp = *this;
            ++(*this);
            return tmp;
          }

          // Prefix decrement
          iterator& operator--() {
            ptr--;
            return *this;
          }

          // Postfix decrement
          iterator operator--(int) {
            iterator tmp = *this;
            --(*this);
            return tmp;
          }

          // Equality operator
          bool operator==(const iterator& other) const {
            return ptr == other.ptr;
          }

          bool operator==(const const_iterator& other) const {
            return ptr == other.ptr;
          }

          // Inequality operator
          bool operator!=(const iterator& other) const {
            return ptr != other.ptr;
          }

          bool operator!=(const const_iterator& other) const {
            return ptr != other.ptr;
          }

          // Advance by n steps (for std::next compatibility)
          iterator& operator+=(difference_type n) {
            ptr += n;
            return *this;
          }

          // Subscript operator for std::next
          reference operator[](difference_type n) {
            return *(ptr + n);
          }

          // Subtraction operator for std::distance compatibility
          difference_type operator-(const iterator& other) const {
            return ptr - other.ptr;
          }

          difference_type operator-(const const_iterator& other) const {
            return ptr - other.ptr;
          }

          // Addition operator for random access
          iterator operator+(difference_type n) const {
            return iterator(ptr + n);
          }

          iterator operator-(difference_type n) const {
            return iterator(ptr - n);
          }

          iterator& operator-=(difference_type n) {
            ptr -= n;
            return *this;
          }

          bool operator<(const iterator& other) const {
            return ptr < other.ptr;
          }

        private:
          pointer ptr;
      };

      class const_iterator {
        friend class iterator;

        public:
          using iterator_category = std::random_access_iterator_tag;
          using difference_type = std::ptrdiff_t;
          using value_type = T;
          using reference = const T&;
          using pointer = const T*;

          // ---

          // Conversion constructor from iterator
          const_iterator(const iterator &other): ptr(other.ptr) {

          }

          // Constructor
          explicit const_iterator(pointer p): ptr(p) {

          }

          // Dereference operator
          reference operator*() const {
            return *ptr;
          }

          // Arrow operator
          pointer operator->() const {
            return ptr;
          }

          // Increment operator
          const_iterator& operator++() {
            ++ptr;
            return *this;
          }

          // Post-increment operator
          const_iterator operator++(int) {
            const_iterator tmp = *this;
            ++(*this);
            return tmp;
          }

          // Prefix decrement
          const_iterator& operator--() {
            ptr--;
            return *this;
          }

          // Postfix decrement
          const_iterator operator--(int) {
            const_iterator tmp = *this;
            --(*this);
            return tmp;
          }

          // Equality operator
          bool operator==(const const_iterator& other) const {
            return ptr == other.ptr;
          }

          bool operator==(const iterator& other) const {
            return ptr == other.ptr;
          }

          // Inequality operator
          bool operator!=(const const_iterator& other) const {
            return ptr != other.ptr;
          }

          bool operator!=(const iterator& other) const {
            return ptr != other.ptr;
          }

          // Advance by n steps (for std::next compatibility)
          const_iterator& operator+=(difference_type n) {
            ptr += n;
            return *this;
          }

          // Subscript operator for std::next
          reference operator[](difference_type n) const {
            return *(ptr + n);
          }

          // Subtraction operator for std::distance compatibility
          difference_type operator-(const const_iterator& other) const {
            return ptr - other.ptr;
          }

          difference_type operator-(const iterator& other) const {
            return ptr - other.ptr;
          }

          // Addition operator for random access
          const_iterator operator+(difference_type n) const {
            return const_iterator(ptr + n);
          }

          const_iterator operator-(difference_type n) const {
            return const_iterator(ptr - n);
          }

          const_iterator& operator-=(difference_type n) {
            ptr -= n;
            return *this;
          }

          bool operator<(const const_iterator& other) const {
            return ptr < other.ptr;
          }

        private:
          pointer ptr;
      };

      // ---

      const_iterator cbegin() const {
        return const_iterator(it);
      }

      const_iterator cend() const {
        return const_iterator(it + len);
      }

      // ---

      iterator begin() const {
        return iterator(it);
      }

      iterator end() const {
        return iterator(it + len);
      }
  
      // ---

      void erase(iterator first, iterator last) {
        if (first == last)
          return;

        std::size_t pos = first - begin();
        std::size_t range = last - first;

        for (std::size_t i = pos; i < len - range; ++i)
          it[i] = it[i + range];

        // Update the size of the vector
        len -= range;
      }

      template <is::iterator I>
      iterator insert(iterator pos, I start, I stop) {
        // Calculate the insertion point and range size
        std::size_t insert_pos = pos - begin();
        std::size_t range_size = std::distance(start, stop);

        // Resize the vector if needed
        if (len + range_size > capac)
          reshape(std::max(capac == 0 ? 1 : capac * 2, len + range_size));

        // Shift elements to make room for the new range
        for (std::size_t i = len; i > insert_pos; --i)
          it[i + range_size - 1] = it[i - 1];

        // Insert the elements from the range
        for (std::size_t i = 0; i < range_size; ++i)
          it[insert_pos + i] = *start++;

        // Update the length
        len += range_size;

        // Return iterator to the first newly inserted element
        return iterator(it + insert_pos);
      }

      iterator insert(iterator pos, const T& input) {
        // Calculate the insertion point
        std::size_t insert_pos = pos - begin();

        // Resize the vector if needed
        if (len + 1 > capac)
          reshape(capac == 0 ? 1 : capac * 2);

        // Shift elements to make room for the new element
        for (std::size_t i = len; i > insert_pos; --i)
          it[i] = it[i - 1];

        // Insert the new element
        it[insert_pos] = input;

        // Update the length
        ++len;

        // Return iterator to the newly inserted element
        return iterator(it + insert_pos);
      }
  };
}