#pragma once

#include "./value.hpp"
#include "./nest.hpp"

namespace pstd {
  template <typename ...A>
  class variant {
    private:
      no_duplicates<A ...> duplicate_check;
      value it;

    public:
      pstd_error(invalid_cast);
      pstd_error(index_error);

      // ---

      template <typename T>
      requires type_allowed_v<T, A ...>
      variant(const T &input): it(input) {

      }

      // creates an empty variant without a value
      variant(): it() {

      }

      // ---

      // Copy constructor
      variant(const variant &other): it(other.it) {

      }

      // Move constructor
      variant(variant&& other) noexcept : it(std::move(other.it)) {

      }

      // ---

      template <typename T>
      requires type_allowed_v<T, A ...>
      variant& operator=(const T &input) {
        it = input;
        return *this;
      }

      variant& operator=(const variant &other) {
        it = other.it;
        return *this;
      }

      // ---

      bool operator==(const std::type_info& other_type) const {
        return it == other_type;
      }

      bool operator!=(const std::type_info& other_type) const {
        return !(it == other_type);
      }

      bool operator==(const variant& other) const {
        return it == other.it;
      }

      bool operator!=(const variant& other) const {
        return !(it == other.it);
      }

      // ---

      template <typename T>
      requires type_allowed_v<T, A ...>
      T cast() const {
        if (it != typeid(T))
          throw invalid_cast("invalid type provided");

        return it.cast<T>();
      }

      // use & for referencing
      template <typename T>
      requires type_allowed_v<T, A ...>
      T& cast() {
        if (it != typeid(T))
          throw invalid_cast("invalid type provided");

        return it.cast<T>();
      }

      // ---

      bool empty() const {
        return it.empty();
      }

      template <typename T>
      requires type_allowed_v<T, A ...>
      bool is_type() const {
        if (empty())
          return !1;

        return it == typeid(T);
      }

      // ---

      std::size_t index() const {
        return ([&]() -> std::size_t {
          if ((is_type<A>() || ...))
            return ((is_type<A>() ? type_index_v<A, A...> : 0) + ...);
          
          else
            throw invalid_cast("unknown type");
        }());
      }

      // ---

      template <typename V>
      decltype(auto) visit(V&& visitor) {
        return ([&visitor, this]() -> decltype(auto) {
          if ((is_type<A>() || ...))
            return ((is_type<A>() ? visitor(cast<A>()) : void()), ...);
          
          else
            throw invalid_cast("unknown type");
        }());
      }

      template <typename V>
      decltype(auto) visit(V&& visitor) const {
        return ([&visitor, this]() -> decltype(auto) {
          if ((is_type<A>() || ...))
            return ((is_type<A>() ? visitor(cast<A>()) : void()), ...);

          else
            throw invalid_cast("unknown type");
        }());
      }
  };
}