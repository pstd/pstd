#pragma once

#include <bits/char_traits.h>
#include <bits/allocator.h>
#include <exception>

#define pstd_error(NAME) \
  struct NAME : public pstd::exception { \
    public: \
      NAME(pstd::exception::const_pointer input) : pstd::exception(input) { \
      } \
  }


namespace pstd {
  struct exception : std::exception {
    public:
      using value_type = char;
      using pointer = value_type *;
      using const_pointer = const value_type *;

      using traits = std::char_traits<value_type>;

      // ---

      exception(const_pointer input) {
        len = traits::length(input);
        it = alloc.allocate(len + 1);

        traits::copy(
          it,
          input,
          len
        );

        it[len] = '\0';
      }

      ~exception() {
        if (it)
          alloc.deallocate(it, len + 1);
      }

      const_pointer what() const noexcept {
        return it;
      }

    private:
      std::allocator<value_type> alloc;
      std::size_t len;
      pointer it;
  };

  // ---

  pstd_error(invalid_argument);
  pstd_error(out_of_range);
}