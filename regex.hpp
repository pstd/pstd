#pragma once

#include "./funktion.hpp"
#include "./vector.hpp"
#include "./text.hpp"

#include <regex>

namespace pstd {
  template <is::iterator I>
  class match_result : public duo<I, I> {
    private:
      using duo<I, I>::a;
      using duo<I, I>::b;

    public:
      match_result(I start, I stop): duo<I, I>(start, stop) {

      }

      match_result() = default;

      // ---

      I begin() const {
        return this->a;
      }

      I end() const {
        return this->b;
      }

      std::size_t length() const {
        return std::distance(
          this->a,
          this->b
        );
      }

      template <typename T>
      auto to() const {
        return T(
          this->a,
          this->b
        );
      }

      bool empty() const {
        return this->a == this->b;
      }
  };

  template <is::iterator I>
  class match_results : public vector<match_result<I>> {
    public:
      using value_type = match_result<I>;

      match_results() = default;

      // ---

      auto position(std::size_t index = 0) const {
        return std::distance(
          this->at(0).begin(),
          this->at(index).end()
        );
      }

      template <typename T>
      auto to(std::size_t index = 0) const {
        value_type next = this->at(index);

        return T(
          next.begin(),
          next.end()
        );
      }
  };

  using match = match_results<text::iterator>;

  // ---

  class regex {
    private:
      text pattern;

    public:
      regex(const text& pattern): pattern(pattern) {

      }

      // ---

      bool match(const text& input, match& results) const {
        std::regex r(pattern.raw());
        std::cmatch matches;

        if (std::regex_match(input.raw(), matches, r)) {
          each(matches, [&](uint i, const std::cmatch::value_type &match) {
            results.push_back({
              input.begin() + matches.position(i),
              input.begin() + matches.position(i) + match.length()
            });
          });

          return !0;
        }

        return !1;
      }

      bool search(const text& input, pstd::match& results) const {
        std::regex r(pattern.raw());
        std::cmatch matches;

        if (std::regex_match(input.raw(), matches, r)) {
          each(matches, [&](uint i, const std::cmatch::value_type &match) {
            results.push_back({
              input.begin() + matches.position(i),
              input.begin() + matches.position(i) + match.length()
            });
          });

          return !0;
        }

        return !1;
      }
  };

  // ---

  bool regex_match(const text& input, const regex& pattern, match& results) {
    return pattern.match(input, results);
  }

  bool regex_match(const text& input, const regex& pattern) {
    match results;

    return regex_match(input, pattern, results);
  }

  // ---

  bool regex_search(const text& input, const regex& pattern, match& result) {
    return pattern.search(input, result);
  }

  bool regex_search(const text& input, const regex& pattern) {
    match result;

    return regex_search(input, pattern, result);
  }

  // ---

  text regex_replace(text input, const regex& pattern, const text& replacement) {
    text result;
    auto input_it = input.begin();
    auto stop = input.end();

    match match_results;
    auto prev_pos = input_it;

    while (regex_search(input, pattern, match_results)) {
      // Extract match positions
      auto match_start = match_results.position();
      auto match_end = match_start + match_results.size();

      // Append text before the match
      result.append(prev_pos, input.begin() + match_start);

      // Append the replacement text
      result.append(replacement.begin(), replacement.end());

      // Update the position for the next search
      prev_pos = input.begin() + match_end;

      // Update input for the next search
      input = text(prev_pos, stop);
    }

    // Append the remaining text after the last match
    result.append(prev_pos, stop);

    return result;
  }
}