#pragma once

#include "./vector.hpp"
#include "./thread.hpp"
#include "./value.hpp"

#include <condition_variable>
#include <mutex>

namespace pstd {
  template <typename R>
  class chain_resolver;

  template <>
  struct chain_resolver<void> {
    public:
      using type = funktion<void()>;
  };

  template <typename R>
  struct chain_resolver {
    public:
      using type = funktion<void(R)>;
  };

  template <typename R>
  using chain_resolver_t = typename chain_resolver<R>::type;

  // ---

  using chain_rejector = funktion<void(std::exception_ptr)>;

  // ---

  template <typename T>
  class chain;

  // ---

  template <>
  class chain<void> {
    private:
      std::condition_variable_any cond;
      std::recursive_mutex mutex;
      std::exception_ptr error;
      bool resolved;
      bool rejected;

    public:
      chain(funktion<void(chain_resolver_t<void>, chain_rejector)> executor): resolved(!1), rejected(!1) {
        threading::call([&, executor]() mutable -> void {
          try {
            executor(
              [&]() {
                {
                  std::lock_guard<std::recursive_mutex> lock(mutex);
                  resolved = !0;
                }
                
                cond.notify_all(); // Notify any waiting threads
              },

              [&](std::exception_ptr err) {
                std::lock_guard<std::recursive_mutex> lock(mutex);
                error = err;
                rejected = !0;
                cond.notify_all(); // Notify any waiting threads
              }
            );
          }

          catch (...) {
            // Handle any exception by setting the error and marking as rejected
            std::lock_guard<std::recursive_mutex> lock(mutex);
            error = std::current_exception();
            rejected = !0;
            cond.notify_all(); // Notify any waiting threads
          }
        });
      }

      // ---

      bool done() const {
        return resolved || rejected;
      }

      auto wait() {
        std::unique_lock<std::recursive_mutex> lock(mutex);

        cond.wait(lock, [&]() -> bool {
          return done();
        });
      }
  
      // ---

      static chain<void> resolve() {
        return chain<void>([](chain_resolver_t<void> res, chain_rejector rej) mutable -> void {
          try {
            res();
          }

          catch (...) {
            rej(std::current_exception());
          }
        });
      }

      static chain<void> resolve(funktion<void()> next) {
        return chain<void>([next](chain_resolver_t<void> res, chain_rejector rej) mutable -> void {
          try {
            next();
            res();
          }

          catch (...) {
            rej(std::current_exception());
          }
        });
      }

      // ---

      template <typename R>
      chain<R> then(funktion<R()> next) {
        return chain<R>([this, next](chain_resolver_t<R> res, chain_rejector rej) mutable -> void {
          wait();

          try {
            R result = next();
            res(result);
          }

          catch (...) {
            rej(std::current_exception());
          }
        });
      }
  
      chain<void> then(funktion<void()> next) {
        return chain<void>([this, next](chain_resolver_t<void> res, chain_rejector rej) mutable -> void {
          wait();

          try {
            next();
            res();
          }

          catch (...) {
            rej(std::current_exception());
          }
        });
      }
  
      // ---

      chain<void> then(funktion<chain<void>()> next) {
        return chain<void>([this, next](chain_resolver_t<void> res, chain_rejector rej) mutable -> void {
          wait();

          try {
            chain<void> kedja = next();

            kedja.then([&]() -> void {
              res();
            });
          }

          catch (...) {
            rej(std::current_exception());
          }
        });
      }

      template <typename R>
      chain<R> then(funktion<chain<R>()> next) {
        return chain<R>([this, next](chain_resolver_t<R> res, chain_rejector rej) mutable -> void {
          wait();

          try {
            chain<R> kedja = next();

            kedja.then([&](R result) -> void {
              res(result);
            });
          }

          catch (...) {
            rej(std::current_exception());
          }
        });
      }
  };

  template <typename T>
  class chain {
    private:
      std::condition_variable_any cond;
      std::recursive_mutex mutex;
      std::exception_ptr error;
      bool resolved;
      bool rejected;
      value it;

    public:
      chain(funktion<void(chain_resolver_t<T>, chain_rejector)> executor): resolved(!1), rejected(!1) {
        threading::call([&, executor]() mutable -> void {
          try {
            executor(
              [&](T result) {
                {
                  std::lock_guard<std::recursive_mutex> lock(mutex);
                  it = result;
                  resolved = !0;
                }
                
                cond.notify_all(); // Notify any waiting threads
              },

              [&](std::exception_ptr err) {
                std::lock_guard<std::recursive_mutex> lock(mutex);
                error = err;
                rejected = !0;
                cond.notify_all(); // Notify any waiting threads
              }
            );
          }

          catch (...) {
            // Handle any exception by setting the error and marking as rejected
            std::lock_guard<std::recursive_mutex> lock(mutex);
            error = std::current_exception();
            rejected = !0;
            cond.notify_all(); // Notify any waiting threads
          }
        });
      }

      // ---

      bool done() const {
        return resolved || rejected;
      }

      auto wait() {
        std::unique_lock<std::recursive_mutex> lock(mutex);

        cond.wait(lock, [&]() -> bool {
          return done();
        });
      }
  
      // ---

      static chain<T> resolve(T input) {
        return chain<T>([input](chain_resolver_t<T> res, chain_rejector rej) mutable -> void {
          try {
            res(input);
          }

          catch (...) {
            rej(std::current_exception());
          }
        });
      }

      static chain<T> resolve(funktion<T()> next) {
        return chain<T>([next](chain_resolver_t<T> res, chain_rejector rej) mutable -> void {
          try {
            T result = next();
            res(result);
          }

          catch (...) {
            rej(std::current_exception());
          }
        });
      }

      // ---
  
      template <typename R>
      chain<R> then(funktion<R(T)> next) {
        return chain<R>([this, next](chain_resolver_t<R> res, chain_rejector rej) mutable -> void {
          wait();

          try {
            T value = it.cast<T>();
            R result = next(value);
            res(result);
          }

          catch (...) {
            rej(std::current_exception());
          }
        });
      }
  
      chain<void> then(funktion<void(T)> next) {
        return chain<void>([this, next](chain_resolver_t<void> res, chain_rejector rej) mutable -> void {
          wait();

          try {
            T value = it.cast<T>();
            next(value);
            res();
          }

          catch (...) {
            rej(std::current_exception());
          }
        });
      }
  
      // ---

      chain<void> then(funktion<chain<void>(T)> next) {
        return chain<void>([this, next](chain_resolver_t<void> res, chain_rejector rej) mutable -> void {
          wait();

          try {
            T value = it.cast<T>();
            chain<void> kedja = next(value);

            kedja.then([&]() -> void {
              res();
            });
          }

          catch (...) {
            rej(std::current_exception());
          }
        });
      }

      template <typename R>
      chain<R> then(funktion<chain<R>(T)> next) {
        return chain<R>([this, next](chain_resolver_t<R> res, chain_rejector rej) mutable -> void {
          wait();

          try {
            T value = it.cast<T>();
            chain<R> kedja = next(value);

            kedja.then([&](R result) -> void {
              res(result);
            });
          }

          catch (...) {
            rej(std::current_exception());
          }
        });
      }
  };
}