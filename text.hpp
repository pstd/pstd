#pragma once

// Copyright (C) 2024 Dave Perry (dbdii407)

#include "./type.hpp"
#include "./uses.hpp"
#include "./math.hpp"
#include "./list.hpp"
#include "./duo.hpp"
#include "./is.hpp"

#include <algorithm>
#include <limits.h>
#include <numeric>
#include <cstring>
#include <memory>
#include <cctype>
#include <cstdio>

namespace pstd {
  template <typename T>
  class basic_text {
    public:
      using difference_type = std::ptrdiff_t;
      using size_type = std::size_t;
      using value_type = T;

      using const_reference = const T&;
      using const_pointer = const T*;
      using reference = T&;
      using pointer = T*;

      // ---

      pstd_error(convert_error);
      pstd_error(out_of_range);
      pstd_error(split_error);

      // --- constructors

      template <is::iterator I>
      basic_text(I first, I last): len(std::distance(first, last)) {
        if (len > 0) {
          it = alloc.allocate(len + 1);
          std::copy(first, last, it);
          it[len] = '\0';
        }
        
        else
          it = nullptr;
      }

      basic_text(const_pointer input) {
        if (input == nullptr)
          len = 0;

        else
          len = traits::length(input);
          
        if (len > 0) {
          it = alloc.allocate(len + 1);
          traits::copy(it, input, len);
          it[len] = '\0';
        }
        
        else
          it = nullptr;
      }

      basic_text(const_pointer input, size_type size): len(size) {
        if (len > 0) {
          it = alloc.allocate(len + 1);
          std::copy(input, input + len, it);
          it[len] = '\0';
        }
        
        else
          it = nullptr;
      }

      basic_text(size_type size): len(size) {
        if (size > 0) {
          it = alloc.allocate(size + 1);
          std::fill(it, it + size, '\0');
          it[size] = '\0';
        }
        
        else
          it = nullptr;
      }

      basic_text(value_type input): len(1) {
        it = alloc.allocate(len + 1);
        traits::assign(it[0], input);
        it[len] = '\0';
      }

      basic_text(): it(nullptr), len(0) {

      }

      ~basic_text() {
        alloc.deallocate(it, len + 1);
      }

      // --- class constructors

      // class copy constructor
      basic_text(const basic_text &other): len(other.len) {
        if (len > 0) {
          it = alloc.allocate(len + 1);
          traits::copy(it, other.it, len);
          it[len] = '\0';
        }
        
        else
          it = nullptr;
      }

      // --- operators

      // equality operator (==)
      bool operator==(const basic_text &other) const {
        if (len != other.len)
          return !1;

        // Use std::equal to compare the content of the two strings
        return std::equal(it, it + len, other.it);
      }

      // inequality operator (!=)
      bool operator!=(const basic_text &other) const {
        return !(*this == other);
      }

      // Assignment operator
      basic_text& operator=(const basic_text &other) {
        if (this != &other) {
          // Deallocate existing memory
          if (it != nullptr)
            alloc.deallocate(it, len + 1);

          // Copy length
          len = other.len;

          // Allocate new memory
          it = alloc.allocate(len + 1);

          // Copy content
          std::copy(other.it, other.it + len, it);

          // Null-terminate the string
          it[len] = '\0';
        }

        return *this;
      }

      // const subscript operator []
      const_reference operator[](difference_type index) const {
        if (index >= len)
          throw out_of_range(
            basic_text("operator[] - index out of range: %d (length: %d)")
              .format(index, len)
              .raw()
          );

        return it[index];
      }

      // subscript operator[]
      reference operator[](difference_type index) {
        if (index >= len)
          throw out_of_range(
            basic_text("operator[] - index out of range: %d (length: %d)")
              .format(index, len)
              .raw()
          );

        return it[index];
      }

      basic_text operator+(const basic_text &other) const {
        // Calculate the total length for the new string
        size_type nyl = len + other.len;
        
        // cannot use alloc due to const
        pointer nyv = new value_type[nyl + 1];
        
        std::memcpy(nyv, it, len);
        
        std::memcpy(nyv + len, other.it, other.len);
        
        nyv[nyl] = '\0';
        
        // Create a new basic_text object with the concatenated result
        basic_text result(nyv);

        delete[] nyv;

        // result.len = nyl;
        // result.it = nyv;
        
        return result;
      }

      basic_text operator+(const_pointer input) {
        size_type il = traits::length(input);
        size_type nyl = len + il;

        // Allocate memory for the new text
        pointer nyt = alloc.allocate(nyl + 1);

        // Copy current text
        std::copy(
          it,
          it + len,
          nyt
        );

        // Copy input text
        std::copy(
          input,
          input + il,
          nyt + len
        );

        // Null-terminate the new text
        nyt[nyl] = '\0';

        // Create the new basic_text object
        basic_text result;

        result.it = nyt;
        result.len = nyl;

        return result;
      }

      // append another string to the end of the current string
      basic_text& operator+=(const basic_text &input) {
        size_type len2 = input.length();
        size_type len1 = length();

        pointer str = alloc.allocate(len1 + len2 + 1);

        if (it != nullptr) {
          std::uninitialized_copy(it, it + len1, str);
          alloc.deallocate(it, len1 + 1);
        }

        if (input.it != nullptr)
          std::uninitialized_copy(input.it, input.it + len2 + 1, str + len1);

        str[len1 + len2] = '\0';
        len = len1 + len2;
        it = str;

        return *this;
      }
      
      // const dereference operator
      const_reference operator*() const {
        return *it;
      }

      // dereference operator
      reference operator*() {
        return *it;
      }

      // const arrow operator
      const_pointer operator->() const {
        return it;
      }

      // arrow operator
      pointer operator->() {
        return it;
      }

      // ---

      operator bool() const {
        return it != nullptr;
      }

      // ---

      class const_iterator {
        friend class basic_text;

      public:
        using iterator_category = std::bidirectional_iterator_tag;
        using difference_type = std::ptrdiff_t;
        using const_reference = const T&;
        using const_pointer = const T*;
        using value_type = T;

        const_iterator(const_pointer input): it(input) {}

        const_iterator(): it(nullptr) {}

        // ---

        const_iterator& operator++() {
          ++it;
          return *this;
        }

        const_iterator operator++(int) {
          const_iterator temp = *this;
          ++it;
          return temp;
        }

        bool operator!=(const const_iterator& other) const {
          return it != other.it;
        }

        bool operator==(const const_iterator& other) const {
          return it == other.it;
        }

        const_iterator operator+(size_type offset) const {
          return const_iterator(it + offset);
        }

        const_reference operator*() const {
          return *it;
        }

        const_iterator operator-(size_type offset) const {
          return const_iterator(it - offset);
        }

        const_iterator& operator--() {
          --it;
          return *this;
        }

        const_iterator operator--(int) {
          const_iterator temp = *this;
          --it;
          return temp;
        }

        const_pointer base() const {
          return it;
        }

      private:
        const_pointer it;
      };

      class iterator {
        friend class basic_text;

        public:
          using iterator_category = std::bidirectional_iterator_tag;
          using difference_type = std::ptrdiff_t;
          using const_reference = const T&;
          using const_pointer = const T*;
          using value_type = T;
          using reference = T&;
          using pointer = T*;

          iterator(pointer input): it(input) {

          }

          iterator(): it(nullptr) {

          }

          // ---


          // is::iterator
          iterator& operator++() {
            ++it;
            return *this;
          }

          // is::iterator
          iterator operator++(int) {
            iterator temp = *this;
            ++it;
            return temp;
          }

          // is::iterator
          bool operator!=(const iterator& other) const {
            return it != other.it;
          }

          // is::iterator
          bool operator==(const iterator& other) const {
            return it == other.it;
          }

          iterator operator+(size_type offset) const {
            return iterator(it + offset);
          }

          reference operator*() const {
            return *it;
          }

          iterator operator-(size_type offset) const {
            return iterator(it - offset);
          }

          iterator& operator--() {
            --it;
            return *this;
          }

          iterator operator--(int) {
            iterator temp = *this;
            --it;
            return temp;
          }

          pointer base() const {
            return it;
          }

        private:
          pointer it;
      };

      // ---

      const_iterator cbegin() const {
        return it;
      }

      const_iterator cend() const {
        return it + len;
      }

      iterator begin() const {
        return it;
      }

      iterator end() const {
        return it + len;
      }

      // ---

      const_pointer raw() const {
        return it;
      }

      pointer raw() {
        return it;
      }

      // ---

      const_reference back() const {
        if (len == 0)
          throw out_of_range("back() - text is empty");

        return it[len - 1];
      }

      reference back() {
        if (len == 0)
          throw out_of_range("back() - text is empty");

        return it[len - 1];
      }

      const_reference front() const {
        if (len == 0)
          throw out_of_range("front() - text is empty");

        return it[0];
      }

      reference front() {
        if (len == 0)
          throw out_of_range("front() - text is empty");

        return it[0];
      }

      const_reference at(size_type pos) const {
        if (pos >= len)
          throw out_of_range("at() - position out of range");

        return it[pos];
      }

      reference at(size_type pos) {
        if (pos >= len)
          throw out_of_range("at() - position out of range");

        return it[pos];
      }

      // ---

      size_type length() const {
        return len;
      }

      bool empty() const {
        return len == 0;
      }

      // ---

      void push_front(const basic_text &input) {
        if (input.empty())
          return;  // Nothing to push

        size_type len2 = input.length();
        size_type len1 = length();

        // Allocate memory for the concatenated string
        pointer str = alloc.allocate(len1 + len2 + 1);

        // Copy the content of the input string
        std::copy(input.it, input.it + len2, str);

        // Copy the content of the current string
        std::copy(it, it + len1 + 1, str + len2);

        // Null-terminate the concatenated string
        str[len1 + len2] = '\0';

        // Deallocate the existing memory
        if (it != nullptr)
          alloc.deallocate(it, len1 + 1);

        // Update the current string's internal state
        it = str;
        len += len2;
      }

      void push_back(const basic_text &input) {
        if (input.empty())
          return;
          
        // Allocate memory for the concatenated string
        pointer str = alloc.allocate(len + input.len + 1);

        // Copy the content of the current string
        std::copy(it, it + len, str);

        // Copy the content of the input string
        std::copy(input.it, input.it + input.len, str + len);

        // Null-terminate the concatenated string
        str[len + input.len] = '\0';

        // Deallocate the existing memory
        if (it != nullptr)
          alloc.deallocate(it, len + 1);

        // Update the current string's internal state
        it = str;
        len += input.len;
      }

      void push_back(value_type input) {
        // Allocate memory for the new string, including the additional character
        pointer str = alloc.allocate(len + 2);

        // Copy the content of the current string
        if (it != nullptr)
          std::copy(it, it + len, str);

        // Add the new character at the end
        str[len] = input;

        // Null-terminate the string
        str[len + 1] = '\0';

        // Deallocate the existing memory
        if (it != nullptr)
          alloc.deallocate(it, len + 1);

        // Update the current string's internal state
        it = str;
        ++len;
      }

      // ---

      void pop_back() {
        if (len > 0) {
          // Allocate memory for the new string
          pointer ny = alloc.allocate(len);

          // Copy existing elements to the new buffer, excluding the last character
          std::copy(it, it + len - 1, ny);

          // Null-terminate the new string
          ny[len - 1] = '\0';

          // Deallocate old memory
          alloc.deallocate(it, len + 1);

          // Update pointers and length
          it = ny;
          --len;
        }
      }

      void pop_front() {
        if (len > 0) {
          // Allocate memory for the new string
          pointer ny = alloc.allocate(len - 1);

          // Copy the content, excluding the first character
          std::copy(it + 1, it + len, ny);

          // Null-terminate the new string
          ny[len - 2] = '\0';

          // Deallocate old memory
          alloc.deallocate(it, len + 1);

          // Update pointers and length
          it = ny;
          --len;
        }
      }

      // ---

      value_type pull_front() {
        if (len == 0)
          throw out_of_range("pull_front() - text is empty");

        value_type next = front();
        pop_front();

        return next;
      }

      value_type pull_back() {
        if (len == 0)
          throw out_of_range("pull_back() - text is empty");

        value_type next = back();
        pop_back();

        return next;
      }

      // ---

      void append(iterator first, iterator last) {
        size_type count = std::distance(first, last);

        if (count > 0) {
          pointer new_it = alloc.allocate(len + count + 1);

          // Copy existing elements
          std::copy(it, it + len, new_it);

          // Copy new elements
          std::copy(first, last, new_it + len);

          // Null-terminate the new string
          new_it[len + count] = '\0';

          // Deallocate old memory
          if (it != nullptr)
            alloc.deallocate(it, len + 1);

          // Update pointers and length
          it = new_it;
          len += count;
        }
      }

      // ---

      size_type find(const basic_text &substr, size_type offset = 0) const {
        if (offset >= len)
          return EOF;  // If pos is beyond the end of the string, return EOF

        if (len < substr.length())
          return EOF;

        for (size_type i = offset; i <= len - substr.length(); ++i)
          if (traits::compare(it + i, &substr[0], substr.length()) == 0)
            return i;

        return EOF;  // If not found, return EOF
      }

      // ---

      size_type find_first(const basic_text &chars, size_type offset = 0) const {
        if (offset >= len)
          return EOF;

        if (len < chars.length())
          return EOF;

        for (size_type i = offset; i < len; ++i)
          if (chars.find(it[i]) != EOF)
            return i;

        return EOF;
      }

      size_type find_last(const basic_text &substr, size_type offset = EOF) const {
        // If the offset exceeds the length of the text, set it to the end of the text
        if (offset == EOF || offset > len)
          offset = len;

        // Calculate the maximum starting position for the search
        size_type max_start_pos = len - substr.length();

        // If the substring is empty or longer than the text, return EOF
        if (substr.empty() || substr.length() > len)
          return EOF;

        // Iterate from the specified offset towards the beginning of the text
        for (size_type i = std::min(offset, max_start_pos); i > 0; --i) {
          // Check if the substring matches at the current position
          if (std::equal(it + i, it + i + substr.length(), substr.begin()))
            return i;
        }

        // If no match is found, return EOF
        return EOF;
      }

      // ---

      size_type find_first_of(const basic_text &chars, size_type offset = 0) const {
        if (offset >= len)
          return EOF;

        for (size_type i = offset; i < len; ++i) {
          if (chars.find(it[i]) != EOF)
            return i;
        }

        return EOF;
      }

      size_type find_last_of(const basic_text &chars, size_type offset = EOF) const {
        // If the offset exceeds the length of the text, set it to the end of the text
        if (offset == EOF || offset >= len)
          offset = len - 1;

        for (size_type i = offset; i != EOF; --i) {
          if (chars.find(it[i]) != EOF)
            return i;
        }

        return EOF;
      }

      // ---

      size_type find_first_not_of(const basic_text &chars, size_type offset = 0) const {
        if (offset >= len)
          return EOF;

        for (size_type i = offset; i < len; ++i) {
          if (chars.find(it[i]) == EOF)
            return i;
        }

        return EOF;
      }

      size_type find_last_not_of(const basic_text &chars, size_type offset = EOF) const {
        // If the offset exceeds the length of the text, set it to the end of the text
        if (offset == EOF || offset >= len)
          offset = len - 1;

        for (size_type i = offset; i != EOF; --i) {
          if (chars.find(it[i]) == EOF)
            return i;
        }

        return EOF;
      }

      // ---

      // replaces a single instance. for all, use basic_text::replace
      basic_text& replace(const basic_text &from, const basic_text &to, size_type offset = 0) {
        size_type start = find(from, offset);

        if (start != EOF) {
          erase(start, from.length());
          insert(start, to);
        }

        return *this;
      }

      // ---

      basic_text slice(size_type pos = 0, size_type count = EOF) const {
        if (pos >= len)
          return {};

        count = std::min(count, len - pos);

        return basic_text(it + pos, count);
      }

      // ---

      // returns duo. for all occurances, use basic_text::split
      duo<basic_text, basic_text> split(const basic_text &needle) const {
        auto found = find(needle);

        if (found != EOF)
          return {
            slice(0, found),
            slice(found + needle.length())
          };
        
        else
          return {
            {},
            {}
          };
      }

      // ---

      bool includes(const basic_text &input) const {
        return find(input) != EOF;
      }

      // ---

      void erase(size_type pos = 0, size_type count = 0) {
        if (pos >= len || count <= 0)
          return; // Nothing to erase

        count = std::min(count, len - pos);

        pointer p = alloc.allocate(len - count + 1);

        // Copy elements before the erased portion
        for (size_type i = 0; i < pos; ++i)
          traits::assign(p[i], it[i]);

        // Copy elements after the erased portion
        for (size_type i = pos + count; i < len; ++i)
          traits::assign(p[i - count], it[i]);

        // Deallocate old memory and update pointers
        alloc.deallocate(it, len + 1);
        len -= count;
        it = p;
      }

      iterator erase(iterator first, iterator last) {
        // Calculate the positions based on the iterators
        size_type start = std::distance(begin(), first);
        size_type count = std::distance(first, last);

        // Erase the characters in the range [start, start + count)
        erase(start, count);

        // Return an iterator pointing to the next character after the erased range
        return begin() + start;
      }

      // ---

      void resize(size_type new_size) {
        // Allocate a new buffer with the specified size
        pointer ny = alloc.allocate(new_size);

        // Copy existing elements to the new buffer
        for (size_type i = 0; i < len; ++i)
          traits::assign(ny[i], it[i]);

        // Deallocate old memory
        if (it != nullptr)
          alloc.deallocate(it, len + 1);

        // Update pointers and length
        it = ny;
        len = new_size;
      }

      // ---

      void clear() {
        if (it != nullptr)
          alloc.deallocate(it, len + 1);

        it = nullptr;
        len = 0;
      }

      // ---

      basic_text& insert(size_type pos, const basic_text &str) {
        // Insert the characters of 'str' at the specified position
        pointer p = alloc.allocate(len + str.length() + 1);

        // Copy elements before the insertion point
        for (size_type i = 0; i < pos; ++i)
          traits::assign(p[i], it[i]);

        // Copy elements from the insertion string
        for (size_type i = 0; i < str.length(); ++i)
          traits::assign(p[pos + i], str[i]);

        // Copy elements after the insertion point
        for (size_type i = pos; i < len; ++i)
          traits::assign(p[str.length() + i], it[i]);

        // Null-terminate the new string
        p[len + str.length()] = '\0';

        // Deallocate old memory and update pointers
        alloc.deallocate(it, len + 1);
        len += str.length();
        it = p;

        return *this;
      }

      template <is::iterator I>
      basic_text& insert(size_type pos, I begin, I end) {
        size_type count = std::distance(begin, end);

        // Insert the characters of the iterator range at the specified position
        pointer p = alloc.allocate(len + count + 1);

        // Copy elements before the insertion point
        for (size_type i = 0; i < pos; ++i)
            traits::assign(p[i], it[i]);

        // Copy elements from the iterator range
        std::copy(begin, end, p + pos);

        // Copy elements after the insertion point
        for (size_type i = pos; i < len; ++i)
          traits::assign(p[count + i], it[i]);

        // Null-terminate the new string
        p[len + count] = '\0';

        // Deallocate old memory and update pointers
        alloc.deallocate(it, len + 1);
        len += count;
        it = p;

        return *this;
      }

      template <is::iterator I>
      basic_text& insert(iterator pos, I begin, I end) {
        size_type count = std::distance(begin, end);

        // Calculate the position index based on the iterator
        size_type index = std::distance(it, pos.it);

        // Insert the characters of the iterator range at the specified position
        pointer p = alloc.allocate(len + count + 1);

        // Copy elements before the insertion point
        for (size_type i = 0; i < index; ++i)
          traits::assign(p[i], it[i]);

        // Copy elements from the iterator range
        std::copy(begin, end, p + index);

        // Copy elements after the insertion point
        for (size_type i = index; i < len; ++i)
          traits::assign(p[count + i], it[i]);

        // Null-terminate the new string
        p[len + count] = '\0';

        // Deallocate old memory and update pointers
        alloc.deallocate(it, len + 1);
        len += count;
        it = p;

        return *this;
      }

      basic_text& insert(value_type input, int iterations) {
        for (auto i = 0; i < iterations; ++i)
          push_back(input);

        return *this;
      }

      // ---

      bool starts_with(const basic_text &prefix) const {
        if (prefix.length() > len)
          return !1; // The text is shorter than the prefix, so it can't start with the prefix

        return std::equal(
          prefix.begin(),
          prefix.end(),
          begin()
        );
      }

      bool ends_with(const basic_text &suffix) const {
        if (len < suffix.length())
          return !1; // The text is shorter than the suffix, so it can't end with the suffix

        // Use std::equal to compare the content of the suffix with the end of the text
        return std::equal(
          suffix.begin(),
          suffix.end(),
          end() - suffix.length()
        );
      }

      // ---

      basic_text lower() const {
        basic_text result(*this); // uses copy constructor
        return result.lower();
      }

      basic_text& lower() {
        std::transform(it, it + len, it, [](value_type c) {
          return std::tolower(c);
        });

        return *this;
      }

      // ---

      basic_text upper() const {
        basic_text result(*this); // uses copy constructor
        return result.upper();
      }

      basic_text& upper() {
        std::transform(it, it + len, it, [](value_type c) {
          return std::toupper(c);
        });

        return *this;
      }

      // ---

      basic_text& rtrim() {
        iterator it = end();

        while (it != begin() && std::isspace(*(it - 1)))
          --it;

        // Erase the trailing whitespace characters
        erase(it, end());

        return *this;
      }

      basic_text& ltrim() {
        iterator it = begin();

        while (it != end() && std::isspace(*it))
          ++it;

        // Erase the leading whitespace characters
        erase(begin(), it);

        return *this;
      }

      basic_text& trim() {
        return ltrim().rtrim();
      }

      // ---

      basic_text remove_whitespace() {
        // Count non-whitespace characters
        size_type new_len = 0;
        for (size_type i = 0; i < len; ++i)
          if (!std::isspace(it[i]))
            ++new_len;

        // Allocate memory for the new string
        pointer new_it = alloc.allocate(new_len + 1);

        // Copy non-whitespace characters to the new string
        size_type j = 0;

        for (size_type i = 0; i < len; ++i)
          if (!std::isspace(it[i]))
            new_it[j++] = it[i];
        
        new_it[new_len] = '\0';

        // Deallocate old memory and update pointers
        if (it != nullptr)
          alloc.deallocate(it, len + 1);

        it = new_it;
        len = new_len;
        
        return *this;
      }

      // prepend another string to the beginning of the current string
      basic_text& prepend(const basic_text &input) {
        size_type len2 = input.length();
        size_type len1 = length();

        pointer str = alloc.allocate(len1 + len2 + 1);

        // Copy input text to the new memory
        std::uninitialized_copy(input.it, input.it + len2, str);
        
        if (it != nullptr) {
          std::uninitialized_copy(it, it + len1, str + len2);
          alloc.deallocate(it, len1 + 1);
        }

        str[len1 + len2] = '\0';
        len = len1 + len2;
        it = str;

        return *this;
      }

      // prepend a const_pointer input to the beginning of the current string
      basic_text& prepend(const_pointer input) {
        size_type il = traits::length(input);
        size_type len1 = length();

        pointer str = alloc.allocate(len1 + il + 1);

        // Copy input text to the new memory
        std::uninitialized_copy(input, input + il, str);
        
        if (it != nullptr) {
          std::uninitialized_copy(it, it + len1, str + il);
          alloc.deallocate(it, len1 + 1);
        }

        str[len1 + il] = '\0';
        len = len1 + il;
        it = str;

        return *this;
      }

      // ---

      template <typename ...A>
      basic_text format(A ...args) const {
        basic_text result(*this); // uses copy constructor
        result.format_implied(args ...);
        return result;
      }

      template <typename ...A>
      basic_text format(A ...args) {
        format_implied(args ...);
        return *this;
      }

      // ---

      template <template <uses::iteration> typename U>
      static basic_text concat(const U<basic_text> &input, const basic_text &sep) {
        return reduce<basic_text>(input, basic_text(""), [&sep](unsigned int i, basic_text current, basic_text next) {
          return (i == 0) ? next : current += basic_text("%s%s").format(sep, next);
        });
      }

      // ---

      // runs .replace multiple times
      static basic_text replace(const basic_text &input, const basic_text &from, const basic_text &to) {
        basic_text out = input;
        size_type start = 0;

        while ((start = out.find(from, start)) != EOF) {
          out.replace(from, to, start);
          start += to.length();
        }

        return out;
      }
      
      // runs .replace multiple times
      static basic_text& replace(basic_text &input, const basic_text &from, const basic_text &to) {
        size_type start = 0;

        while ((start = input.find(from, start)) != EOF) {
          input.replace(from, to, start);
          start += to.length();
        }

        return input;
      }

      // ---

      template <template <uses::iteration> typename C>
      static C<basic_text> split(const basic_text &input, const basic_text &delimiter) {
        if (delimiter.empty())
          throw split_error("delimiter can't be empty");
          
        C<basic_text> result;
        size_type start = 0;

        for (auto end = input.find(delimiter, start); end != EOF; end = input.find(delimiter, start)) {
          auto next = input.slice(start, end - start);
          result.push_back(next);

          start = end + delimiter.length();
        }

        // Add the final segment of text after the last delimiter
        result.push_back(input.slice(start));

        return result;
      }

      // ---

      template <typename U>
      requires type_allowed_v<U, short, u_short, int, u_int, long, u_long, long long, unsigned long long>
      static U to(const basic_text &input, int base = 10) {
        if (input.empty())
          throw invalid_argument("cannot do anything with an empty input");

        // ---

        basic_text::size_type index = 0;
        bool negative = (input[index] == '-');

        if (negative) {
          ++index;

          if (!std::is_signed_v<U>)
            throw invalid_argument("numeric type cannot be negative");
        }

        // ---

        U val = 0;

        for (; index < input.length(); ++index) {
            char c = input[index];
            int digit;

            if (c >= '0' && c <= '9')
              digit = c - '0';
            
            else if (c >= 'a' && c <= 'f')
              digit = c - 'a' + 10;
            
            else if (c >= 'A' && c <= 'F')
              digit = c - 'A' + 10;
            
            else {
              throw std::invalid_argument("input contains invalid character");
            }

            if (digit >= base)
              throw std::invalid_argument("digit out of range for the specified base");

            val = val * base + digit;
        }

        if (negative)
          val = -val;

        // ---

        auto min = std::numeric_limits<U>::min();
        auto max = std::numeric_limits<U>::max();

        if (val < min || val > max)
          throw out_of_range("result is out of range for type");

        // ---

        return val;
      }

      template <typename U>
      requires type_allowed_v<U, float, double, long double>
      static U to(const basic_text &input) {
        basic_text::size_type index = 0;
        bool negative = !1;
        bool dotted = !1;
        bool exponent = !1;
        U result = 0.0;
        U factor = 1.0;

        if (input.empty())
          throw invalid_argument("cannot do anything with an empty input");

        if (input[index] == '-') {
          negative = !0;
          ++index;
        }

        for (; index < input.length(); ++index) {
          if (input[index] == '.' && !dotted) {
            dotted = !0;
            continue;
          }

          if ((input[index] == 'e' || input[index] == 'E') && !exponent) {
            exponent = !0;
            ++index;

            result *= power(10, to<int>(input.slice(index)));
            break;
          }

          if (is_digit(input[index])) {
            if (dotted)
              factor *= 0.1, result += (input[index] - '0') * factor;

            else
              result = result * 10 + (input[index] - '0');
          }          
          
          else
            throw invalid_argument("input contains invalid character");
        }

        if (negative)
          result = -result;

        // ---

        return result;
      }

      // ---

      template <typename U>
      requires type_allowed_v<U, short, u_short, int, u_int, long, u_long, long long, unsigned long long>
      static bool valid(const basic_text &input) {
        if (input.empty())
          return !1;

        // ---

        basic_text::size_type index = 0;
        bool negative = (input[index] == '-');

        if (negative) {
          ++index;

          if (!std::is_signed_v<U>)
            return !1;
        }

        // ---

        U val = 0;

        for (; index < input.length(); ++index) {
          if (!is_digit(input[index]))
            return !1;

          val = val * 10 + (input[index] - '0');
        }

        if (negative)
          val = -val;

        // ---

        auto min = std::numeric_limits<U>::min();
        auto max = std::numeric_limits<U>::max();

        if (val < min || val > max)
          return !1;

        // ---

        return !0;
      }

      template <typename U>
      requires type_allowed_v<U, float, double, long double>
      static bool valid(const basic_text &input) {
        basic_text::size_type index = 0;
        bool negative = !1;
        bool dotted = !1;
        bool exponent = !1;
        U result = 0.0;
        U factor = 1.0;

        if (input.empty())
          return !1;

        if (input[index] == '-') {
          negative = !0;
          ++index;
        }

        for (; index < input.length(); ++index) {
          if (input[index] == '.' && !dotted) {
            dotted = !0;
            continue;
          }

          if ((input[index] == 'e' || input[index] == 'E') && !exponent) {
            exponent = !0;
            ++index;

            result *= power(10, to<int>(input.slice(index)));
            break;
          }

          if (is_digit(input[index])) {
            if (dotted)
              factor *= 0.1, result += (input[index] - '0') * factor;
              
            else
              result = result * 10 + (input[index] - '0');
          }          
          
          else
            return !1;
        }

        if (negative)
          result = -result;

        // ---

        auto min = std::numeric_limits<U>::min();
        auto max = std::numeric_limits<U>::max();

        if (result < min || result > max)
          return !1;

        // ---

        return !0;
      }

      // ---

      template <typename U>
      requires type_allowed_v<U, short, u_short, int, u_int, long, u_long, long long, unsigned long long, float, double, long double>
      static basic_text from(const U input) {
        return basic_text("%d").format(input);
      }

    private:
      using traits = std::char_traits<T>;

      std::allocator<T> alloc;
      size_type len;
      pointer it;

      // ---

      template <typename F, typename ...A>
      void format_implied(F arg, A ...args) {
        format_argument(arg);
        format_implied(args ...);
      }

      template <typename F>
      void format_implied(F arg) {
        format_argument(arg);
      }

      void format_implied() {

      }

      // ---

      template <typename X>
      void format_replace(const basic_text &snf, const basic_text &rpl, const X input) {
        size_type size = static_cast<size_type>(
          std::snprintf(nullptr, 0, &snf[0], input)
        );

        pointer buffer = alloc.allocate(size + 1);

        std::snprintf(buffer, size + 1, &snf[0], input);

        replace(rpl, buffer);

        alloc.deallocate(buffer, size + 1);
      }

      // ---

      void format_argument(const value_type input) {
        replace("%c", input);
      }

      void format_argument(const basic_text &input) {
        replace("%s", input);
      }
      
      // ---

      void format_argument(const u_short input) {
        format_replace("%hu", "%d", input);
      }

      void format_argument(const short input) {
        format_replace("%hd", "%d", input);
      }
      
      // ---

      void format_argument(const u_int input) {
        format_replace("%u", "%d", input);
      }

      void format_argument(const int input) {
        format_replace("%i", "%d", input);
      }

      // ---

      void format_argument(const u_long input) {
        format_replace("%lu", "%d", input);
      }

      void format_argument(const long input) {
        format_replace("%ld", "%d", input);
      }

      // ---

      void format_argument(const unsigned long long input) {
        format_replace("%llu", "%d", input);
      }

      void format_argument(const long long input) {
        format_replace("%lld", "%d", input);
      }

      // ---

      void format_argument(const float input) {
        format_replace("%f", "%d", input);
      }

      void format_argument(const double input) {
        format_replace("%f", "%d", input);
      }

      template <typename U>
      requires (std::is_same_v<U, long double>)
      void format_argument(const U input) {
        format_replace("%Lf", "%d", input);
      }

      // ---

      template <typename X>
      requires (std::is_same_v<bool, X>)
      void format_argument(const X input) {
        if (input)
          format_replace("%s", "%b", "(true)");

        else
          format_replace("%s", "%b", "(false)");
      }
  };

  // ---

  using text = basic_text<char>;
}