#pragma once

#include "./pointer/basic.hpp"
#include "./maybe.hpp"
#include "./type.hpp"
#include "./uses.hpp"
#include "./duo.hpp"

#ifndef EOF
  #define EOF -1
#endif

namespace pstd {
  template <typename T>
  struct is_lambda {
    private:
      template <typename U>
      static auto check(U*) -> decltype(&U::operator(), std::true_type());

      template <typename>
      static std::false_type check(...);

    public:
      static constexpr bool value = std::is_class<T>::value && decltype(check<T>(nullptr))::value;
  };

  template<typename T>
  inline constexpr bool is_lambda_v = is_lambda<T>::value;

  template <typename T>
  concept lambda_c = is_lambda_v<T>;

  // ---

  template <typename S>
  struct signature {
    private:
      // Nested implied struct to handle various function signatures
      template <typename T, typename = void>
      struct implied;

      template <typename R, typename ...A>
      struct implied<R(A...)> {
        using type = R(A...);
      };

      template <typename R, typename ...A>
      struct implied<R(A...) &> {
        using type = R(A...);
      };

      template <typename R, typename ...A>
      struct implied<R(A...) const &> {
        using type = R(A...);
      };

      template <typename R, typename ...A>
      struct implied<R(A...) volatile &> {
        using type = R(A...);
      };

      template <typename R, typename ...A>
      struct implied<R(A...) const volatile &> {
        using type = R(A...);
      };

      template <typename R, typename ...A>
      struct implied<R(A...) &&> {
        using type = R(A...);
      };

      template <typename R, typename ...A>
      struct implied<R(A...) const &&> {
        using type = R(A...);
      };

      template <typename R, typename ...A>
      struct implied<R(A...) volatile &&> {
        using type = R(A...);
      };

      template <typename R, typename ...A>
      struct implied<R(A...) const volatile &&> {
        using type = R(A...);
      };

      // Specialization for member funktions
      template <typename R, typename T, typename ...A>
      struct implied<R(T::*)(A...)> {
        using type = R(A...);
      };

      template <typename R, typename T, typename ...A>
      struct implied<R(T::*)(A...) const> {
        using type = R(A...);
      };

      template <typename R, typename T, typename ...A>
      struct implied<R(T::*)(A...) volatile> {
        using type = R(A...);
      };

      template <typename R, typename T, typename ...A>
      struct implied<R(T::*)(A...) const volatile> {
        using type = R(A...);
      };

      template <lambda_c T>
      struct implied<T> : implied<decltype(&T::operator())> {

      };

    public:
      using type = typename implied<S>::type;
  };

  // Alias template for easier usage
  template <typename T>
  using signature_t = typename signature<T>::type;

  // ---

  template <typename T>
  concept callable_c = requires {
    typename signature_t<std::decay_t<T>>;
  };

  // ---

  template <typename T>
  struct signature_decay {
    private:
      template <typename R, typename = void>
      struct implied;

      template <typename R, typename ...A>
      struct implied<R(A...)> {
        using type = R(std::decay_t<A>...);
      };

    public:
      using type = implied<signature_t<T>>::type;
  };

  // Alias template for easier usage
  template <typename T>
  using signature_decay_t = typename signature_decay<T>::type;

  // ---

  template <typename T>
  struct signature_return {
    private:
      template <typename R, typename = void>
      struct implied;

      template <typename R, typename ...A>
      struct implied<R(A...)> {
        using type = R;
      };

    public:
      using type = implied<signature_t<T>>::type;
  };

  template <typename T>
  using signature_return_t = typename signature_return<T>::type;
  
  // ---

  template <typename T, typename S, typename = void>
  struct is_suitable_signature : std::false_type {

  };

  template <typename T, typename S>
  struct is_suitable_signature<T, S, std::void_t<signature_t<T>>> {
    private:
      template <typename, typename>
      struct implied : std::false_type {

      };

      template <typename TR, typename ...TA, typename SR, typename ...SA>
      struct implied<TR(TA...), SR(SA...)> : std::bool_constant<std::is_convertible_v<SR, TR> && (std::is_convertible_v<SA, TA> && ...)> {

      };

    public:
      static constexpr bool value = implied<signature_t<T>, S>::value;
  };

  template <typename T, typename S>
  inline constexpr bool is_suitable_signature_v = is_suitable_signature<T, S>::value;

  template <typename T, typename S>
  concept suitable_signature = is_suitable_signature<T, S>::value;

  // ---

  template <typename F, typename S>
  struct is_same_signature : std::is_same<signature_t<F>, S> {

  };

  template <typename F, typename S>
  inline constexpr bool is_same_signature_v = is_same_signature<F, S>::value;

  template <typename F, typename S>
  concept same_signature = is_same_signature_v<F, S>;

  // ---

  template <typename S>
  class funktion;

  // ---

  template <typename T>
  struct is_funktion : std::false_type {

  };

  template <typename R, typename ...A>
  struct is_funktion<funktion<R(A ...)>> : std::true_type {

  };

  template <typename T>
  inline constexpr bool is_funktion_v = is_funktion<T>::value;

  // Concept to check if a type is pstd::funktion
  template <typename T>
  concept funktion_c = is_funktion_v<T>;

  // ---

  template <typename R, typename T, typename... A>
  funktion<R(A...)> bind(T* that, R(T::*func)(A...)) {
    return funktion<R(A...)>(
      that,
      func
    );
  }

  template <typename R, typename T, typename... A>
  funktion<R(A...)> bind(const T* that, R(T::*func)(A...) const) {
    return funktion<R(A...)>(
      const_cast<T*>(that),
      const_cast<R(T::*)(A...)>(func)
    );
  }

  template <typename T, typename R, typename ...A, typename ...X>
  requires (suitable_signature<R(A...), R(X...)>)
  auto call(T* that, R(T::*func)(A...), X&& ...args) {
    auto bound = bind(that, func);
    
    return bound(
      std::forward<X>(args) ...
    );
  }

  // ---

  template <typename R, typename ...A>
  class funktion<R(A ...)> {
    private:
      class base_implied {
        public:
          virtual basic_pointer<base_implied> clone() const = 0; // Clone method
          virtual ~base_implied() = default;
          virtual R invoke(A ...args) = 0;
        };

      template <typename L>
      class lambda : public base_implied {
        private:
          L it;

        public:
          lambda(L input) : it(std::forward<L>(input)) {}

          R invoke(A ...args) override {
            return it(args ...);
          }

          basic_pointer<base_implied> clone() const override {
            return make_pointer<lambda<L>>(it); // Copy the lambda
          }
      };

      template <typename T>
      class member_function_pointer : public base_implied {
        private:
          R(T::*it)(A ...);
          T* that;

        public:
          member_function_pointer(T* instance, R(T::*func)(A ...)): it(func), that(instance) {

          }

          R invoke(A ...args) override {
            return (that->*it)(args...);
          }

          basic_pointer<base_implied> clone() const override {
            return make_pointer<member_function_pointer<T>>(that, it);
          }
      };

      // ---

      basic_pointer<base_implied> it;

    public:
      template <lambda_c T>
      // requires (std::is_same_v<signature_t<std::decay_t<T>>, R(A...)>)
      requires (same_signature<std::decay_t<T>, R(A...)>)
      funktion(T&& input) {
        it = make_pointer<lambda<std::decay_t<T>>>(std::forward<std::decay_t<T>>(input));
      }

      template <typename T>
      funktion(T* that, R(T::*func)(A ...)) noexcept {
        it = make_pointer<member_function_pointer<T>>(that, func);
      }

      // ---

      R operator()(A ...args) {
        return it->invoke(std::forward<A>(args)...);
      }

      // ---

      // nullptr constructor
      funktion(std::nullptr_t) noexcept: it() {

      }

      // Default constructor
      funktion() = default;

      // ---

      // Copy constructor
      funktion(const funktion &other): it(other.it ? other.it->clone() : nullptr) {

      }

      // Move constructor
      funktion(funktion&& other) noexcept: it(std::move(other.it)) {
        
      }

      // ---

      // Copy assignment operator
      funktion& operator=(const funktion &other) {
        if (this != &other)
          it = other.it ? other.it->clone() : nullptr;

        return *this;
      }

      // Move assignment operator
      funktion& operator=(funktion&& other) noexcept {
        if (this != &other)
          it = std::move(other.it);

        return *this;
      }

      // nullptr assignment operator
      funktion& operator=(std::nullptr_t) noexcept {
        it.reset();
        return *this;
      }

      // ---

      // Comparison operators
      bool operator==(const funktion &other) const {
        return it == other.it;
      }

      bool operator!=(const funktion &other) const {
        return !(*this == other);
      }

      bool operator==(std::nullptr_t) const {
        return empty();
      }

      bool operator!=(std::nullptr_t) const {
        return !empty();
      }

      bool empty() const {
        return !it;
      }

      explicit operator bool() const noexcept {
        return !empty();
      }
  };

  // ---

  // this allows usage of funktion as a make_funktion

  template <typename T>
  funktion(T) -> funktion<signature_t<T>>;

  // ---

  template <uses::const_iteration T>
  duo<typename T::const_iterator, typename T::const_iterator> const_ranges(T &input) {
    return {
      input.cbegin(),
      input.cend()
    };
  }

  template <uses::iteration T>
  duo<typename T::iterator, typename T::iterator> ranges(T &input) {
    return {
      input.begin(),
      input.end()
    };
  }

  // ---

  template <uses::iteration T>
  requires uses::erase<T>
  void remove(T &input, funktion<bool(typename T::value_type &)> func) {
    for (auto iter = input.begin(); iter != input.end(); ) {
      if (func(*iter))
        iter = input.erase(iter);

      else
        iter++;
    }
  }

  template <uses::iteration T>
  requires uses::erase<T>
  void remove(T &input, funktion<bool(typename T::value_type)> func) {
    for (auto iter = input.begin(); iter != input.end(); ) {
      if (func(*iter))
        iter = input.erase(iter);

      else
        iter++;
    }
  }

  // ---

  template <typename T>
  concept indexed_container = uses::size<T> && uses::indexing_operator<T>;

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T &input, funktion<bool(unsigned int, const typename T::value_type &)> func) {
    for (auto i = 0; i < input.size(); i++)
      if (func(i, input[i]))
        break;
  }

  template <uses::iteration T>
  void each(T &input, funktion<bool(unsigned int, const typename T::value_type &)> func) {
    auto begin = input.begin();

    for (auto iter = begin; iter != input.end(); iter++) {
      auto distance = std::distance(begin, iter);
      
      if (func(distance, *iter))
        break;
    }
  }

  // ---

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T &input, funktion<bool(unsigned int, typename T::value_type &)> func) {
    for (auto i = 0; i < input.size(); i++)
      if (func(i, input[i]))
        break;
  }

  template <uses::iteration T>
  void each(T &input, funktion<bool(unsigned int, typename T::value_type &)> func) {
    auto begin = input.begin();

    for (auto iter = begin; iter != input.end(); iter++) {
      auto distance = std::distance(begin, iter);
      
      if (func(distance, *iter))
        break;
    }
  }

  // ---

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T &input, funktion<bool(unsigned int, typename T::value_type)> func) {
    for (auto i = 0; i < input.size(); i++)
      if (func(i, input[i]))
        break;
  }

  template <uses::iteration T>
  void each(T &input, funktion<bool(unsigned int, typename T::value_type)> func) {
    auto begin = input.begin();

    for (auto iter = begin; iter != input.end(); iter++) {
      auto distance = std::distance(begin, iter);
      
      if (func(distance, *iter))
        break;
    }
  }

  // ---

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T &input, funktion<bool(const typename T::value_type &)> func) {
    for (auto i = 0; i < input.size(); i++)
      if (func(input[i]))
        break;
  }

  template <uses::iteration T>
  void each(T &input, funktion<bool(const typename T::value_type &)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++) {
      if (func(*iter))
        break;
    }
  }

  // ---

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T &input, funktion<bool(typename T::value_type &)> func) {
    for (auto i = 0; i < input.size(); i++)
      if (func(input[i]))
        break;
  }

  template <uses::iteration T>
  void each(T &input, funktion<bool(typename T::value_type &)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++) {
      if (func(*iter))
        break;
    }
  }

  // ---

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T &input, funktion<bool(typename T::value_type)> func) {
    for (auto i = 0; i < input.size(); i++)
      if (func(input[i]))
        break;
  }

  template <uses::iteration T>
  void each(T &input, funktion<bool(typename T::value_type)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++) {
      if (func(*iter))
        break;
    }
  }

  // --- &&

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T&& input, funktion<bool(const typename T::value_type &)> func) {
    for (auto i = 0; i < input.size(); i++)
      if (func(input[i]))
        break;
  }

  template <uses::iteration T>  // l-value?
  void each(T&& input, funktion<bool(const typename T::value_type &)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++) {
      if (func(*iter))
        break;
    }
  }

  // ---

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T&& input, funktion<bool(typename T::value_type &)> func) {
    for (auto i = 0; i < input.size(); i++)
      if (func(input[i]))
        break;
  }

  template <uses::iteration T>  // l-value?
  void each(T&& input, funktion<bool(typename T::value_type &)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++) {
      if (func(*iter))
        break;
    }
  }

  // ---

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T&& input, funktion<bool(typename T::value_type)> func) {
    for (auto i = 0; i < input.size(); i++)
      if (func(input[i]))
        break;
  }

  template <uses::iteration T>  // l-value?
  void each(T&& input, funktion<bool(typename T::value_type)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++) {
      if (func(*iter))
        break;
    }
  }

  // ---------- void versions ----------

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T &input, funktion<void(unsigned int, const typename T::value_type &)> func) {
    for (auto i = 0; i < input.size(); i++)
      func(i, input[i]);
  }
  
  template <uses::iteration T>
  void each(T &input, funktion<void(unsigned int, const typename T::value_type &)> func) {
    auto begin = input.begin();

    for (auto iter = begin; iter != input.end(); iter++) {
      auto distance = std::distance(begin, iter);
      func(distance, *iter);
    }
  }

  // ---

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T &input, funktion<void(unsigned int, typename T::value_type &)> func) {
    for (auto i = 0; i < input.size(); i++)
      func(i, input[i]);
  }

  template <uses::iteration T>
  void each(T &input, funktion<void(unsigned int, typename T::value_type &)> func) {
    auto begin = input.begin();

    for (auto iter = begin; iter != input.end(); iter++) {
      auto distance = std::distance(begin, iter);
      func(distance, *iter);
    }
  }

  // ---

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T &input, funktion<void(unsigned int, typename T::value_type)> func) {
    for (auto i = 0; i < input.size(); i++)
      func(i, input[i]);
  }

  template <uses::iteration T>
  void each(T &input, funktion<void(unsigned int, typename T::value_type)> func) {
    auto begin = input.begin();

    for (auto iter = begin; iter != input.end(); iter++) {
      auto distance = std::distance(begin, iter);
      func(distance, *iter);
    }
  }

  // ---

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T &input, funktion<void(const typename T::value_type &)> func) {
    for (auto i = 0; i < input.size(); i++)
      func(input[i]);
  }

  template <uses::iteration T>
  void each(T &input, funktion<void(const typename T::value_type &)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++)
      func(*iter);
  }

  // ---

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T &input, funktion<void(typename T::value_type &)> func) {
    for (auto i = 0; i < input.size(); i++)
      func(input[i]);
  }

  template <uses::iteration T>
  void each(T &input, funktion<void(typename T::value_type &)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++)
      func(*iter);
  }

  // ---

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T &input, funktion<void(typename T::value_type)> func) {
    for (auto i = 0; i < input.size(); i++)
      func(input[i]);
  }

  template <uses::iteration T>
  void each(T &input, funktion<void(typename T::value_type)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++)
      func(*iter);
  }

  // ---

  template <uses::iteration T> // l-value?
  void each(T&& input, funktion<void(const typename T::value_type &)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++)
      func(*iter);
  }

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T&& input, funktion<void(const typename T::value_type &)> func) {
    for (auto i = 0; i < input.size(); i++)
      func(input[i]);
  }

  template <uses::iteration T> // l-value?
  void each(T&& input, funktion<void(typename T::value_type &)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++)
      func(*iter);
  }

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T&& input, funktion<void(typename T::value_type &)> func) {
    for (auto i = 0; i < input.size(); i++)
      func(input[i]);
  }

  template <uses::iteration T> // l-value?
  void each(T&& input, funktion<void(typename T::value_type)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++)
      func(*iter);
  }

  template <typename T>
  requires (indexed_container<T> && !uses::iteration<T>)
  void each(T&& input, funktion<void(typename T::value_type)> func) {
    for (auto i = 0; i < input.size(); i++)
      func(input[i]);
  }

  // ---

  template <uses::iteration T>
  auto iterator_start(const T &input, funktion<bool(const typename T::value_type &)> func) -> decltype(input.begin()) {
    auto [begin, end] = ranges(input);

    for (auto iter = begin; iter != end; iter++)
      if (func(*iter))
        return iter;

    return end;
  }

  template <uses::iteration T>
  auto iterator_start(const T &input, funktion<bool(const typename T::value_type)> func) -> decltype(input.begin()) {
    auto [begin, end] = ranges(input);

    for (auto iter = begin; iter != end; iter++)
      if (func(*iter))
        return iter;

    return end;
  }

  template <uses::iteration T>
  auto iterator_start(T &input, funktion<bool(typename T::value_type &)> func) -> decltype(input.begin()) {
    auto [begin, end] = ranges(input);

    for (auto iter = begin; iter != end; iter++)
      if (func(*iter))
        return iter;

    return end;
  }

  template <uses::iteration T>
  auto iterator_start(T &input, funktion<bool(typename T::value_type)> func) -> decltype(input.begin()) {
    auto [begin, end] = ranges(input);

    for (auto iter = begin; iter != end; iter++)
      if (func(*iter))
        return iter;

    return end;
  }

  // ---

  template <uses::iteration T>
  auto iterator_end(T &input, funktion<bool(typename T::value_type &)> func) -> decltype(input.begin()) {
    auto [begin, end] = ranges(input);

    for (auto iter = end; iter != begin; iter--)
      if (func(*iter))
        return iter;

    return end;
  }

  template <uses::iteration T>
  auto iterator_end(T &input, funktion<bool(typename T::value_type)> func) -> decltype(input.begin()) {
    auto [begin, end] = ranges(input);

    for (auto iter = end; iter != begin; iter--)
      if (func(*iter))
        return iter;

    return end;
  }

  // ---

  template <uses::iteration T>
  int index(const T &input, funktion<bool(const typename T::value_type &)> func) {
    auto iter = iterator_start(input, func);

    if (iter == input.end())
      return EOF;

    // ---

    return std::distance(
      input.begin(),
      iter
    );
  }

  template <uses::iteration T>
  int index(T &input, funktion<bool(typename T::value_type &)> func) {
    auto iter = iterator_start(input, func);

    if (iter == input.end())
      return EOF;

    // ---

    return std::distance(
      input.begin(),
      iter
    );
  }

  template <uses::iteration T>
  int index(T &input, funktion<bool(typename T::value_type)> func) {
    auto iter = iterator_start(input, func);

    if (iter == input.end())
      return EOF;

    // ---

    return std::distance(
      input.begin(),
      iter
    );
  }

  // ---

  template <typename R, uses::iteration T>
  R reduce(T &input, R initial, funktion<R(unsigned int, R, const typename T::value_type &)> func) {
    auto begin = input.begin();
    R accumulator = initial;

    for (auto iter = begin; iter != input.end(); iter++) {
      auto distance = std::distance(begin, iter);
      accumulator = func(distance, accumulator, *iter);
    }
      
    return accumulator;
  }

  template <typename R, uses::iteration T>
  R reduce(T &input, R initial, funktion<R(unsigned int, R, typename T::value_type &)> func) {
    auto begin = input.begin();
    R accumulator = initial;

    for (auto iter = begin; iter != input.end(); iter++) {
      auto distance = std::distance(begin, iter);
      accumulator = func(distance, accumulator, *iter);
    }
      
    return accumulator;
  }

  template <typename R, uses::iteration T>
  R reduce(T &input, R initial, funktion<R(unsigned int, R, typename T::value_type)> func) {
    auto begin = input.begin();
    R accumulator = initial;

    for (auto iter = begin; iter != input.end(); iter++) {
      auto distance = std::distance(begin, iter);
      accumulator = func(distance, accumulator, *iter);
    }
      
    return accumulator;
  }

  // ---

  template <typename R, uses::iteration T>
  R reduce(T &&input, R initial, funktion<R(unsigned int, R, const typename T::value_type &)> func) {
    auto begin = input.begin();
    R accumulator = initial;

    for (auto iter = begin; iter != input.end(); iter++) {
      auto distance = std::distance(begin, iter);
      accumulator = func(distance, accumulator, *iter);
    }
      
    return accumulator;
  }

  template <typename R, uses::iteration T>
  R reduce(T &input, R initial, funktion<R(R, typename T::value_type &)> func) {
    R accumulator = initial;

    for (auto iter = input.begin(); iter != input.end(); iter++)
      accumulator = func(accumulator, *iter);

    return accumulator;
  }

  template <typename R, uses::iteration T>
  R reduce(T &input, R initial, funktion<R(R, typename T::value_type)> func) {
    R accumulator = initial;

    for (auto iter = input.begin(); iter != input.end(); iter++)
      accumulator = func(accumulator, *iter);

    return accumulator;
  }

  // ---

  template <typename R, uses::iteration T>
  R reduce(T &&input, R initial, funktion<R(R, typename T::value_type &)> func) {
    R accumulator = initial;

    for (auto iter = input.begin(); iter != input.end(); iter++)
      accumulator = func(accumulator, *iter);

    return accumulator;
  }

  // ---

  // Template to detect if a type T is an lvalue reference.
  template <typename T>
  constexpr T&& forward(typename std::remove_reference_t<T>& arg) noexcept {
    return static_cast<T&&>(arg);
  }

  // Overload for when T is an rvalue reference.
  template <typename T>
  constexpr T&& forward(typename std::remove_reference_t<T>&& arg) noexcept {
    static_assert(!std::is_lvalue_reference<T>::value, "cannot forward an rvalue as an lvalue");
    return static_cast<T&&>(arg);
  }

  // ---

  template <uses::iteration T>
  bool exists(T &input, funktion<bool(typename T::value_type &)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++) {
      if (func(*iter))
        return !0;
    }
    return !1;
  }

  template <uses::iteration T>
  bool exists(T &input, funktion<bool(typename T::value_type)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++) {
      if (func(*iter))
        return !0;
    }

    return !1;
  }

  // ---

  template <uses::iteration T>
  maybe<typename T::value_type> find(T &input, funktion<bool(typename T::value_type &)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++) {
      if (func(*iter))
        return *iter;
    }
    return {};
  }

  template <uses::iteration T>
  maybe<typename T::value_type> find(T &input, funktion<bool(typename T::value_type)> func) {
    for (auto iter = input.begin(); iter != input.end(); iter++) {
      if (func(*iter))
        return *iter;
    }
    return {};
  }
}