#pragma once

#include "./funktion.hpp"
#include "./duration.hpp"

#include <condition_variable>
#include <thread>
#include <queue>
#include <mutex>

namespace pstd {
  template <typename R, typename P>
  void sleep(std::chrono::duration<R, P> input) {
    std::this_thread::sleep_for(input);
  }

  void sleep(const duration &input) {
    std::this_thread::sleep_for(*input);
  }
}

namespace pstd::threading {
  template <lambda_c L, typename ...A>
  requires (suitable_signature<std::decay_t<L>, void(A...)>)
  void call(L&& lambda, A&& ...args) {
    funktion<signature_t<std::decay_t<L>>> funkt = funktion(std::forward<L>(lambda));
    std::thread(funkt, std::forward<A>(args) ...).detach();
  }

  template <typename T, typename R, typename ...A, typename ...X>
  requires (suitable_signature<R(A...), R(X...)>)
  auto call(T* that, R(T::*func)(A...), X&& ...args) {
    auto bound = bind(that, func);
    std::thread(bound, std::forward<X>(args) ...).detach();
  }

  // ---

  template <typename T, lambda_c L>
  void each(T &input, L&& lambda) {
    for (auto &item : input)
      std::thread(std::forward<L>(lambda), std::ref(item)).detach();
  }

  template <typename T, typename ...A>
  void each(T &input, const funktion<void(A ...)> &func) {
    for (auto &item : input)
      std::thread(func, std::ref(item)).detach();
  }

  // ---

  class loop {
    private:
      variant<funktion<void()>, funktion<bool()>> task;
      std::condition_variable_any cond;
      std::atomic<bool> enabled;
      std::recursive_mutex it;

      bool check() {
        return !running();
      }

    public:
      static basic_pointer<loop> create(funktion<bool()> task) {
        return make_pointer<loop>(task);
      }

      static basic_pointer<loop> create(funktion<void()> task) {
        return make_pointer<loop>(task);
      }

      // ---

      loop(variant<funktion<void()>, funktion<bool()>> task): task(task), enabled(!1) {

      }

      ~loop() {
        stop();
      }

      void run(duration &timeout) {
        if (enabled)
          return;

        enabled = !0;

        call([&](duration timeout) {
          while (enabled) {
            std::unique_lock<std::recursive_mutex> latch(it);

            cond.wait_for(latch, *timeout, bind(this, &loop::check));

            if (!enabled)
              break;

            if (task.index() == 0)
              task.cast<funktion<void()>>()();

            else if (task.index() == 1)
              if (!task.cast<funktion<bool()>>()())
                enabled = !1;
          }
        }, timeout);
      }

      void stop() {
        std::lock_guard<std::recursive_mutex> latch(it);

        enabled = !1;

        cond.notify_all();
      }

      bool running() {
        return enabled;
      }
  };
}