#pragma once

#include "./funktion.hpp"
#include "./vector.hpp"
#include "./text.hpp"

namespace pstd {
  // Print text to the terminal. Supports text's format's specifiers.
  template <typename ...A>
  void out(text input, A ...args) {
    if (sizeof... (A) > 0)
      input.format(args ...);

    // ---

    text::replace(input, "%", "%%");

    // ---

    printf(input.raw());

    if (!input.ends_with("\n"))
      putchar('\n');
  }

  // ---

  template <typename ...A>
  text in(std::size_t size, text prompt, A ...args) {
    if (sizeof... (A) > 0)
      prompt.format(args ...);

    // ---

    printf(prompt.raw());
    printf(": ");

    // ---

    vector<text::value_type> buffer(size);

    fgets(&buffer[0], size, stdin);

    // ---

    auto begin = buffer.begin();

    return text(
      begin,

      iterator_end(buffer, [&](text::value_type next) -> bool {
        return next == '\n' ? !0 : !1;
      })
    );
  }

  // ---

  template <typename ...A>
  duo<bool, text> run(text input, A ...args) {
    if (sizeof... (A) > 0)
      input.format(args ...);

    // ---

    text::replace(input, "%", "%%");

    // ---

    FILE* pipe = popen(
      input.raw(),
      "r"
    );

    if (!pipe)
      return {1, {}};

    std::array<char, 128> buffer;
    text result;

    // ---

    while (fgets(buffer.data(), buffer.size(), pipe) != nullptr)
      result += buffer.data();

    // ---

    auto begin = result.begin();

    return {
      WEXITSTATUS(pclose(pipe)) == 0,  // Command exit status

      text(
        begin,

        iterator_end(result, [&](text::value_type next) -> bool {
          return next == '\n' ? !0 : !1;
        })
      )
    };
  }
}