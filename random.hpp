#pragma once

#include "./text.hpp"

#include <random>

namespace pstd::random {
  static auto engine = std::default_random_engine(std::random_device{}()); 

  inline uint number(uint start, uint end) {
    auto distro = std::uniform_int_distribution<uint>(start, end);
    return distro(engine);
  }

  inline text string(uint length, text charlist) {
    auto out = text();

    for (auto i = 0; i < length; i++) {
      auto position = number(0, charlist.length() - 1);
      auto next = charlist.at(position);

      out.push_back(next);
    }

    return out;
  }
}