#pragma once

#include "./vector.hpp"
#include "./text.hpp"

namespace pstd {
  class arguments {
    private:
      vector<text> list;

    public:
      arguments(int argc, char* argv[]) {
        for (auto i = 0; i < argc; ++i)
          list.push_back(argv[i]);
      }

      // ---

      std::size_t size() const {
        return list.size();
      }

      text at(std::size_t index) {
        return list.at(index);
      }

      const text& operator[](std::size_t index) const {
        return list[index];
      }

      text& operator[](std::size_t index) {
        return list[index];
      }
  };
}