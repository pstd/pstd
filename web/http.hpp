#pragma once

#include "../variant.hpp"
#include "../nest.hpp"
#include "../toss.hpp"
#include "../map.hpp"
#include "./url.hpp"
#include "./net.hpp"

#include "../duration.hpp"

namespace pstd::web::http {
  enum class code : uint {
    // Informational
    CONTINUE = 100,
    SWITCHING_PROTOCOLS = 101,
    PROCESSING = 102,
    EARLY_HINTS = 103,

    // Successful
    OK = 200,
    CREATED = 201,
    ACCEPTED = 202,
    NON_AUTHORITATIVE_INFORMATION = 203,
    NO_CONTENT = 204,
    RESET_CONTENT = 205,
    PARTIAL_CONTENT = 206,
    MULTI_STATUS = 207,
    ALREADY_REPORTED = 208,
    IM_USED = 226,

    // Redirection
    MULTIPLE_CHOICES = 300,
    MOVED_PERMANENTLY = 301,
    FOUND = 302,
    SEE_OTHER = 303,
    NOT_MODIFIED = 304,
    USE_PROXY = 305,
    TEMPORARY_REDIRECT = 307,
    PERMANENT_REDIRECT = 308,

    // Client Errors
    BAD_REQUEST = 400,
    UNAUTHORIZED = 401,
    PAYMENT_REQUIRED = 402,
    FORBIDDEN = 403,
    NOT_FOUND = 404,
    METHOD_NOT_ALLOWED = 405,
    NOT_ACCEPTABLE = 406,
    PROXY_AUTHENTICATION_REQUIRED = 407,
    REQUEST_TIMEOUT = 408,
    CONFLICT = 409,
    GONE = 410,
    LENGTH_REQUIRED = 411,
    PRECONDITION_FAILED = 412,
    PAYLOAD_TOO_LARGE = 413,
    URI_TOO_LONG = 414,
    UNSUPPORTED_MEDIA_TYPE = 415,
    RANGE_NOT_SATISFIABLE = 416,
    EXPECTATION_FAILED = 417,
    IM_A_TEAPOT = 418,
    MISDIRECTED_REQUEST = 421,
    UNPROCESSABLE_ENTITY = 422,
    LOCKED = 423,
    FAILED_DEPENDENCY = 424,
    TOO_EARLY = 425,
    UPGRADE_REQUIRED = 426,
    PRECONDITION_REQUIRED = 428,
    TOO_MANY_REQUESTS = 429,
    REQUEST_HEADER_FIELDS_TOO_LARGE = 431,
    UNAVAILABLE_FOR_LEGAL_REASONS = 451,

    // Server Errors
    INTERNAL_SERVER_ERROR = 500,
    NOT_IMPLEMENTED = 501,
    BAD_GATEWAY = 502,
    SERVICE_UNAVAILABLE = 503,
    GATEWAY_TIMEOUT = 504,
    HTTP_VERSION_NOT_SUPPORTED = 505,
    VARIANT_ALSO_NEGOTIATES = 506,
    INSUFFICIENT_STORAGE = 507,
    LOOP_DETECTED = 508,
    NOT_EXTENDED = 510,
    NETWORK_AUTHENTICATION_REQUIRED = 511
  };

  inline text get_status(code status_code) {
    switch (status_code) {
      // Informational
      case code::CONTINUE: return "Continue";
      case code::SWITCHING_PROTOCOLS: return "Switching Protocols";
      case code::PROCESSING: return "Processing";
      case code::EARLY_HINTS: return "Early Hints";

      // Successful
      case code::OK: return "OK";
      case code::CREATED: return "Created";
      case code::ACCEPTED: return "Accepted";
      case code::NON_AUTHORITATIVE_INFORMATION: return "Non-Authoritative Information";
      case code::NO_CONTENT: return "No Content";
      case code::RESET_CONTENT: return "Reset Content";
      case code::PARTIAL_CONTENT: return "Partial Content";
      case code::MULTI_STATUS: return "Multi-Status";
      case code::ALREADY_REPORTED: return "Already Reported";
      case code::IM_USED: return "IM Used";

      // Redirection
      case code::MULTIPLE_CHOICES: return "Multiple Choices";
      case code::MOVED_PERMANENTLY: return "Moved Permanently";
      case code::FOUND: return "Found";
      case code::SEE_OTHER: return "See Other";
      case code::NOT_MODIFIED: return "Not Modified";
      case code::USE_PROXY: return "Use Proxy";
      case code::TEMPORARY_REDIRECT: return "Temporary Redirect";
      case code::PERMANENT_REDIRECT: return "Permanent Redirect";

      // Client Errors
      case code::BAD_REQUEST: return "Bad Request";
      case code::UNAUTHORIZED: return "Unauthorized";
      case code::PAYMENT_REQUIRED: return "Payment Required";
      case code::FORBIDDEN: return "Forbidden";
      case code::NOT_FOUND: return "Not Found";
      case code::METHOD_NOT_ALLOWED: return "Method Not Allowed";
      case code::NOT_ACCEPTABLE: return "Not Acceptable";
      case code::PROXY_AUTHENTICATION_REQUIRED: return "Proxy Authentication Required";
      case code::REQUEST_TIMEOUT: return "Request Timeout";
      case code::CONFLICT: return "Conflict";
      case code::GONE: return "Gone";
      case code::LENGTH_REQUIRED: return "Length Required";
      case code::PRECONDITION_FAILED: return "Precondition Failed";
      case code::PAYLOAD_TOO_LARGE: return "Payload Too Large";
      case code::URI_TOO_LONG: return "URI Too Long";
      case code::UNSUPPORTED_MEDIA_TYPE: return "Unsupported Media Type";
      case code::RANGE_NOT_SATISFIABLE: return "Range Not Satisfiable";
      case code::EXPECTATION_FAILED: return "Expectation Failed";
      case code::IM_A_TEAPOT: return "I'm a teapot";
      case code::MISDIRECTED_REQUEST: return "Misdirected Request";
      case code::UNPROCESSABLE_ENTITY: return "Unprocessable Entity";
      case code::LOCKED: return "Locked";
      case code::FAILED_DEPENDENCY: return "Failed Dependency";
      case code::TOO_EARLY: return "Too Early";
      case code::UPGRADE_REQUIRED: return "Upgrade Required";
      case code::PRECONDITION_REQUIRED: return "Precondition Required";
      case code::TOO_MANY_REQUESTS: return "Too Many Requests";
      case code::REQUEST_HEADER_FIELDS_TOO_LARGE: return "Request Header Fields Too Large";
      case code::UNAVAILABLE_FOR_LEGAL_REASONS: return "Unavailable For Legal Reasons";

      // Server Errors
      case code::INTERNAL_SERVER_ERROR: return "Internal Server Error";
      case code::NOT_IMPLEMENTED: return "Not Implemented";
      case code::BAD_GATEWAY: return "Bad Gateway";
      case code::SERVICE_UNAVAILABLE: return "Service Unavailable";
      case code::GATEWAY_TIMEOUT: return "Gateway Timeout";
      case code::HTTP_VERSION_NOT_SUPPORTED: return "HTTP Version Not Supported";
      case code::VARIANT_ALSO_NEGOTIATES: return "Variant Also Negotiates";
      case code::INSUFFICIENT_STORAGE: return "Insufficient Storage";
      case code::LOOP_DETECTED: return "Loop Detected";
      case code::NOT_EXTENDED: return "Not Extended";
      case code::NETWORK_AUTHENTICATION_REQUIRED: return "Network Authentication Required";

      default: return "Unknown Status";
    }
  }

  // ---

  class method {
    public:
      static const method NONE;
      static const method GET;
      static const method POST;
      static const method PUT;
      static const method DELETE;
      static const method PATCH;
      static const method OPTIONS;
      static const method HEAD;

      // ---

      bool operator==(const method &other) const {
        return name == other.name;
      }

      bool operator!=(const method &other) const {
        return !(*this == other);
      }

      // ---

      bool operator==(const text &input) const {
        return name == input;
      }

      bool operator!=(const text &input) const {
        return name != input;
      }

      // ---

      method& operator=(const text &input) {
        name = input.lower();
        return *this;
      }

      // ----

      operator pstd::text() const {
        return name;
      }

      explicit method() = default;

      // ---

      text::const_pointer raw() const {
        return name.raw();
      }

    private:
      text name;

      explicit method(pstd::text name): name(name.lower()) {

      }
  };

  const method method::OPTIONS("options");
  const method method::DELETE("delete");
  const method method::PATCH("patch");
  const method method::HEAD("head");
  const method method::POST("post");
  const method method::GET("get");
  const method method::PUT("put");
  const method method::NONE("");

  // ---

  struct header : public map<text, text> {
    private:
      using map<text, text>::operator[];
      using map<text, text>::insert;

    public:
      header(std::initializer_list<value_type> list = {}) {
        for (auto [property, value] : list)
          set(property, value);
      }

      bool exists(text property) const {
        property.lower();

        return find(property) != end();
      }

      maybe<text> get(text property) const {
        property.lower();

        auto it = find(property);

        if (it != end())
          return it->b;

        else
          return {};
      }

      void set(text property, text value) {
        property.lower();

        auto it = find(property);

        if (it != end())
          it->b = value;

        else
          insert({
            property,
            value
          });
      }
  };

  // ---

  struct request {
    public:
      text path, dest, username, password;
      variant<text, list<text>> body;
      map<text, text> params;
      struct header header;
      url::queries queries;
      class method method;
  };

  struct response {
    public:
      variant<text, list<text>> body;
      struct header header;
      text status;
      int code;
  };

  // ---

  pstd_error(exception);

  // ---

  maybe<response> parse_response(basic_pointer<net::socket> socket, duration timeout = duration::seconds(0)) {
    auto start = timestamp::now();
    text buffer;

    while (!0) {
      socket->recv(buffer);

      if (timeout.count() > 0 && (timestamp::now() - start > timeout))
        return {};

      // ---

      auto end = buffer.find("\r\n\r\n");

      if (end == EOF)
        continue;

      // ---

      response res;

      auto headers = buffer.slice(0, end);

      auto lines = text::split<pstd::list>(headers, "\r\n");

      auto top = lines.pull_front();

      // ---

      each(lines, [&](text &line) -> void {
        auto [property, value] = line.split(": ");
        res.header.set(property, value);
      });

      // ---

      text code;

      nest(ignore, code, res.status) = text::split<pstd::list>(top, " ");

      res.code = text::to<int>(code);

      // ---

      if (auto length = res.header.get("content-length")) {
        auto i = text::to<std::size_t>(*length);
        auto start = end + 4;
        text body;

        while (buffer.length() - start < i)
          socket->recv(buffer);

        body = buffer.slice(start, start + i);

        // ---

        auto type = res.header.get("content-type");

        if (!type)
          return res;

        // ---

        if (auto i = type->find(";"))
          type = type->slice(0, i);

        type->lower();

        if (type == "text/html") {
          body = body.slice(body.find_first("<"));
          body = body.slice(0, body.find_last(">") + 1);

          res.body = body;
        }

        else if (type == "application/json") {
          auto first = body.find_first_of("{[");
          body = body.slice(first);

          auto last = body.find_last_of("]}");
          body = body.slice(0, last + 1);

          res.body = body;
        }
      }

      else if (res.header.get("transfer-encoding") == "chunked") {
        list<text> chunks;

        auto start = end + 4;

        while (!0) {
          auto chunk_size_end = buffer.find("\r\n", start);

          if (chunk_size_end == EOF) {
            socket->recv(buffer);
            continue;
          }

          auto chunk_size_str = buffer.slice(start, chunk_size_end - start);
          auto chunk_size = text::to<std::size_t>(chunk_size_str, 16); // Hexadecimal size

          if (chunk_size == 0)
            break;

          start = chunk_size_end + 2; // Skip "\r\n"

          while (buffer.length() - start < chunk_size + 2) // Include "\r\n" after chunk
            socket->recv(buffer);

          // ---

          auto chunk = buffer.slice(start, start + chunk_size);

          chunks.push_back(chunk);

          start += chunk_size + 2; // Move past the chunk and "\r\n"
        }

        res.body = chunks;
      }

      else {
        text body;

        while (socket->connected())
          socket->recv(body);

        res.body = body;
      }

      return res;
    }
  }

  maybe<request> parse_request(basic_pointer<net::socket> socket, duration timeout = duration::seconds(0)) {
    auto start = timestamp::now();
    text buffer;

    while (!0) {
      socket->recv(buffer);

      if (timeout.count() > 0 && (timestamp::now() - start > timeout))
        return {};

      // ---

      auto end = buffer.find("\r\n\r\n");

      if (end == EOF)
        continue;

      // ---

      request req;

      auto headers = buffer.slice(0, end);

      auto lines = text::split<pstd::list>(headers, "\r\n");

      auto top = lines.pull_front();

      // ---

      each(lines, [&](text &line) -> void {
        auto [property, value] = line.split(": ");
        req.header.set(property, value);
      });

      // ---

      text path;

      nest(req.method, path) = text::split<pstd::list>(top, " ");

      // ---

      auto comps = url::parse(path);

      req.username = comps.username;
      
      req.password = comps.password;

      req.queries = comps.queries;

      req.path = comps.path;

      req.dest = comps.path;

      // ---

      if (auto length = req.header.get("content-length")) {
        auto i = text::to<std::size_t>(*length);
        auto start = end + 4;
        text body;

        while (buffer.length() - start < i)
          socket->recv(buffer);

        body = buffer.slice(start, start + i);

        // ---

        auto type = req.header.get("content-type");

        if (!type)
          return req;

        // ---

        if (auto i = type->find(";"))
          type = type->slice(0, i);

        type->lower();

        if (type == "text/html") {
          body = body.slice(body.find_first("<"));
          body = body.slice(0, body.find_last(">") + 1);


        }

        else if (type == "application/json") {
          auto first = body.find_first_of("{[");
          body = body.slice(first);

          auto last = body.find_last_of("]}");
          body = body.slice(0, last + 1);
        }

        else if (type->starts_with("application/x-www-form-urlencoded")) {
          auto parts = text::split<pstd::list>(body, "&");

          using ty = decltype(req.params);

          req.params = reduce<ty>(parts, {}, [](ty it, text next) -> ty {
              auto [parameter, value] = next.split("=");
              it[parameter] = value;
              return it;
            }
          );
        }

        // ---

        req.body = body;
      }
      
      else if (req.header.get("transfer-encoding") == "chunked") {
        list<text> chunks;

        auto start = end + 4;

        while (!0) {
          auto chunk_size_end = buffer.find("\r\n", start);

          if (chunk_size_end == EOF) {
            socket->recv(buffer);
            continue;
          }

          auto chunk_size_str = buffer.slice(start, chunk_size_end - start);
          auto chunk_size = text::to<std::size_t>(chunk_size_str, 16); // Hexadecimal size

          if (chunk_size == 0)
            break;

          start = chunk_size_end + 2; // Skip "\r\n"

          while (buffer.length() - start < chunk_size + 2) // Include "\r\n" after chunk
            socket->recv(buffer);

          // ---

          auto chunk = buffer.slice(start, start + chunk_size);

          chunks.push_back(chunk);

          start += chunk_size + 2; // Move past the chunk and "\r\n"
        }

        req.body = chunks;
      }

      else {
        text body;

        while (socket->connected())
          socket->recv(body);

        req.body = body;
      }

      return req;
    }
  }
}

