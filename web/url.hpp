#pragma once

// Copyright (C) 2024 Dave Perry (dbdii407)

#include "../funktion.hpp"
#include "../maybe.hpp"
#include "../text.hpp"
#include "../map.hpp"

namespace pstd::web::url {
  pstd_error(exception);

  // ---

  bool is_valid(const text &input) {
    if (input.empty())
      return !1;

    if (input.find(' ') != EOF)
      return !1;

    auto dot_pos = input.find('.');

    if (dot_pos == EOF)
      return !1;

    if (dot_pos == 0 || dot_pos == input.length() - 1)
      return !1;

    auto i = index(input, [&](typename text::value_type next) -> bool {
      if (!isalnum(next) && next != '.' && next != '/' && next != ':' && next != '-' && next != '_')
        return !0;

      return !1;
    });

    return i != EOF ? !1 : !0;
  }

  // ---

  struct queries : public map<text, text> {
    private:

    public:
      queries(std::initializer_list<value_type> list = {}) {
        for (auto [key, value] : list)
          set(key, value);
      }

      bool exists(text key) const {
        key.lower();
        
        return find(key) != end();
      }

      maybe<text> get(text key) const {
        const_iterator it = find(key);

        if (it != end())
          return it->b;

        else
          return {};
      }

      void set(text key, text value) {
        key.lower();

        iterator it = find(key);

        if (it != end())
          it->b = value;

        else
          insert({
            key,
            value
          });
      }

      operator text() const {
        auto list = pstd::list<text>();

        for (auto [key, value] : *this)
          list.push_back(
            text("%s=%s").format(key, value)
          );

        return text::concat(list, "&");
      }
  };

  // ---

  struct components {
    public:
      struct queries queries;
      text username;
      text password;
      text fragment;
      text scheme;
      text host;
      text path;
      uint16_t port;

      text absolute_path() const {
        if (!queries.size())
          return path;

        return text("%s?%s")
          .format(path, text(queries));
      }

      text directory() const {
        auto d = path;

        if (d.ends_with("/"))
          d = d.slice(0, path.find_last("/") - 1);

        // ---

        auto pos = d.find_last("/");

        if (pos != EOF)
          return d.slice(pos + 1);

        else
          return {};
      }

      // ---

      struct iterator {
        private:
          const components* comp;
          int id;

        public:
          using iterator_category = std::forward_iterator_tag;
          using value_type = duo<text, text>;
          using difference_type = std::ptrdiff_t;
          using reference = value_type&;
          using pointer = value_type*;

          iterator(const components* c, int index) : comp(c), id(index) {

          }

          value_type operator*() const {
            switch (id) {
              case 0: return {"username", comp->username};
              case 1: return {"password", comp->password};
              case 2: return {"fragment", comp->fragment};
              case 3: return {"scheme", comp->scheme};
              case 4: return {"host", comp->host};
              case 5: return {"path", comp->path};
              case 6: return {"port", text::from(comp->port)};
              case 7: return {"absolute_path", comp->absolute_path()};
              case 8: return {"directory", comp->directory()};
              // Additional cases for queries if necessary
              default: return {"", ""};
            }
          }

          iterator& operator++() {
            id++;
            return *this;
          }

          iterator operator++(int) {
            iterator tmp = *this;
            ++(*this);
            return tmp;
          }

          bool operator==(const iterator& other) const {
            return id == other.id && comp == other.comp;
          }

          bool operator!=(const iterator& other) const {
            return !(*this == other);
          }
      };

      // ---

      iterator begin() const {
        return iterator(this, 0);
      }

      iterator end() const {
        return iterator(this, 9);
      }
  };

  // Function to parse a URL and extract its components
  components parse(text input) {
    components result;
    
    // Find the scheme
    std::size_t pos_scheme = input.find("://");

    if (pos_scheme != EOF) {
      result.scheme = input.slice(0, pos_scheme);
      input = input.slice(pos_scheme + 3); // Move past "://"
    }

    // Find username and password
    std::size_t pos_userpass = input.find('@');
    
    if (pos_userpass != EOF) {
      text userpass = input.slice(0, pos_userpass);
      std::size_t pos_user = userpass.find(':');

      if (pos_user != EOF) {
        result.username = userpass.slice(0, pos_user);
        result.password = userpass.slice(pos_user + 1);
      }
      
      else
        result.username = userpass;

      input = input.slice(pos_userpass + 1); // Move past '@'
    }

    // Find host and port
    std::size_t pos_host = input.find('/');
    auto host_port = (pos_host != EOF) ? input.slice(0, pos_host) : input;
    std::size_t pos_port = host_port.find(':');

    if (pos_port != EOF) {
      result.host = host_port.slice(0, pos_port);
      result.port = text::to<int>(host_port.slice(pos_port + 1));
    }
    
    else {
      result.host = host_port;
      // Default port according to scheme
      if (result.scheme == "http")
        result.port = 80;
      
      else if (result.scheme == "https" || result.scheme == "wss")
        result.port = 443;

      else if (result.scheme == "sftp")
        result.port = 22;
      
      else
        result.port = 443; // Unknown scheme, let it be 0
    }

    // Find path and fragment
    input = input.slice(pos_host);
    
    std::size_t pos_fragment = input.find('#');

    if (pos_fragment != EOF) {
      result.fragment = input.slice(pos_fragment + 1);
      input = input.slice(0, pos_fragment);
    }

    // Find queries
    std::size_t pos_query = input.find('?');

    if (pos_query != EOF) {
      text query_string = input.slice(pos_query + 1);

      for (const auto& pair : text::split<list>(query_string, '&')) {
        std::size_t pos_equal = pair.find('=');
        
        if (pos_equal != EOF)
          result.queries.set(
            pair.slice(0, pos_equal),
            pair.slice(pos_equal + 1)
          );
      }

      input = input.slice(0, pos_query); // Remove query part from path
    }

    input.rtrim();

    result.path = input.empty() ? "/" : input;

    return result;
  }

  text create(components comp) {
    text url;

    if (!comp.scheme.empty())
      url += text("%s://")
        .format(comp.scheme);

    if (!comp.username.empty()) {
      url += comp.username;

      if (!comp.password.empty())
        url += text(":%s")
          .format(comp.password);

      url += "@";
    }

    if (!comp.host.empty())
      url += comp.host;

    if (comp.port != 0 && ((comp.scheme == "http" && comp.port != 80) || 
                          (comp.scheme == "https" && comp.port != 443) || 
                          (comp.scheme == "wss" && comp.port != 443) || 
                          (comp.scheme == "sftp" && comp.port != 22))) 

      url += text(":%d")
        .format(comp.port);

    if (!comp.path.empty())
      url += comp.path;

    if (comp.queries.size())
      url += text("?%s")
        .format(text(comp.queries));

    if (!comp.fragment.empty())
      url += text("#%s")
        .format(comp.fragment);

    return url;
  }

  // ---

  text concat(text first, text second) {
    // Check if the first path ends with "/", if yes, remove it
    if (!first.empty() && first.back() == '/')
      first.pop_back();
    
    // Check if the second path starts with "/", if yes, remove it
    if (!second.empty() && second.front() == '/')
      second = second.slice(1);

    // Concatenate the paths with "/"
    return first + "/" + second;
  }

  text concat(std::initializer_list<text> paths) {
    text result;

    for (auto& path : paths)
      result = concat(result, path);

    return result;
  }
}