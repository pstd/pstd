#pragma once

#include "../json.hpp"
#include "./http.hpp"
#include "./ssl.hpp"
#include "./tcp.hpp"

namespace pstd::web::www {
  class socket {
    friend class router;

    private:
      basic_pointer<pstd::web::net::socket> it;
      web::http::request request;

    public:
      socket(basic_pointer<pstd::web::net::socket> &socket, web::http::request &req): request(req), it(socket) {

      }

      // ---

      bool connected() const {
        return it->connected();
      }

      // ---

      uint16_t port() const {
        return it->port();
      }

      text ip() const {
        auto found = header("x-forwarded-for");

        if (found)
          return *found;

        return it->ip();
      }

      // ---

      maybe<text> header(const text &input) const {
        return request.header.get(input);
      }

      maybe<text> query(const text &input) const {
        return request.queries.get(input);
      }

      maybe<text> param(const text &input) const {
        if (!request.params.contains(input))
          return {};

        return request.params[input];
      }

      // ---

      auto headers() const {
        return request.header;
      }

      text dest() const {
        return request.dest;
      }

      text path() const {
        return request.path;
      }

      text body() const {
        return request.body;
      }

      http::method method() const {
        return request.method;
      }

      // ---

      struct response_options {
        maybe<web::http::code> code;
        web::http::header header;
      };

      // ---

      void out(const text &input, response_options opts = {}) {
        if (!opts.code)
          opts.code = web::http::code::OK;

        // ---

        list<text> out;

        auto top = text("HTTP/1.1 %d %s").format(
          std::underlying_type_t<web::http::code>(*opts.code),
          web::http::get_status(*opts.code)
        );

        out.push_back(top);

        // ---

        if (!opts.header.contains("content-length"))
          opts.header.set("content-length", text::from(input.length()));

        if (!opts.header.contains("content-type"))
          opts.header.set("content-type", "text/plain");

        if (!opts.header.contains("connection"))
          opts.header.set("connection", "close");

        // ---

        for (auto [property, value] : opts.header) {
          auto line = text("%s: %s").format(property, value);
          out.push_back(line);
        }

        // ---

        out.push_back({});

        out.push_back(input);

        // ---

        auto line = text::concat(out, "\r\n");

        it->write(line);

        it->close();
      }

      void out(const json &input, response_options opts = {}) {
        auto str = text(input);

        opts.header.set("content-type", "application/json");

        out(str, opts);
      }

      // ---

      void redirect(const text &location, response_options opts = {}) {
        opts.header.set("location", location);

        // Call the `out` method with an empty body for the redirect
        out("", opts);
      }

      void html(const text &html, response_options opts = {}) {
        opts.header.set("content-type", "text/html");

        out(html, opts);
      }
  };

  // ---

  class router {
    friend class server;

    private:
      using next_type = funktion<void()>;
      using middleware_type = funktion<void(socket, next_type)>;
      using method_type = funktion<void(socket)>;

      using variant_type = variant<
        middleware_type,
        method_type,
        router
      >;

      triple<web::http::method, text, variant_type> list;

      // ---

      void handle(socket &inc) {
        if (!inc.connected()) // if disconnected, then stop.
          return;

        std::size_t index = 0;

        funktion<void()> next = [&]() -> void {
          if (index == list.size())
            return;

          // ---

          auto [method, path, vari] = list[index++];

          if (method == web::http::method::NONE) {
            if (path.empty()) { // .use([](inc, next));
              auto middle = vari.cast<middleware_type>();

              middle(inc, [&]() -> void {
                next();
              });
            }

            else { // .use(path, [](router))
              auto route = vari.cast<router>();

              // ---

              auto original = inc.request.path;

              inc.request.path = inc.request.path.slice(path.length());

              if (!inc.request.path.starts_with("/"))
                inc.request.path.prepend("/");

              route.handle(inc);

              inc.request.path = original;

              // ---

              if (!inc.connected()) // if route handled it, then stop
                return;

              next();
            }
          }

          else {
            if (method != inc.request.method)
              return next();

            if (path != inc.request.path)
              return next();

            // ---

            auto func = vari.cast<method_type>();

            func(inc);

            // ---

            if (inc.connected())
              printf("expr - warning: method request \"%s\" was not handled/closed\n", inc.request.method.raw());
          }
        };  

        next();
      }

    public:
      void get(const text &path, method_type func) {
        list.push_back({
          web::http::method::GET,
          path,
          func
        });
      }

      void post(const text &path, method_type func) {
        list.push_back({
          web::http::method::POST,
          path,
          func
        });
      }

      void put(const text &path, method_type func) {
        list.push_back({
          web::http::method::PUT,
          path,
          func
        });
      }

      void del(const text &path, method_type func) {
        list.push_back({
          web::http::method::DELETE,
          path,
          func
        });
      }

      void patch(const text &path, method_type func) {
        list.push_back({
          web::http::method::PATCH,
          path,
          func
        });
      }

      void options(const text &path, method_type func) {
        list.push_back({
          web::http::method::OPTIONS,
          path,
          func
        });
      }

      void head(const text &path, method_type func) {
        list.push_back({
          web::http::method::HEAD,
          path,
          func
        });
      }

      struct with_opts {
        web::http::method method;
        text path;
        method_type func;
      };

      void with(with_opts opts) {
        list.push_back({
          opts.method,
          opts.path,
          opts.func
        });
      }

      // ---

      void use(const text &path, router router) {
        list.push_back({
          web::http::method::NONE,
          path,
          router
        });
      }

      void use(middleware_type func) {
        list.push_back({
          web::http::method::NONE,
          {},
          func
        });
      }

      // ---

      bool operator==(const router &other) const {
        return list == other.list;
      }
  };

  // ---

  class server : public router {
    private:
      unique_pointer<net::server> it;
      std::chrono::seconds to;

      // ---

      void incoming(basic_pointer<net::socket> inc) {
        auto start = std::chrono::steady_clock::now();
        text buffer;

        // ---

        while (!0) {
          inc->recv(buffer);
          // ---

          // timeout connection if not finished within time period
          if (to.count() > 0 && std::chrono::steady_clock::now() - start > to) {
            http::request request;

            socket s(inc, request);

            return s.out("timeout reached", {
              .code = web::http::code::REQUEST_TIMEOUT
            });
          }

          // ---

          auto end = buffer.find("\r\n\r\n");

          if (end != EOF) {
            auto headers = buffer.slice(0, end);

            // ---

            auto lines = text::split<pstd::list>(headers, "\r\n");

            auto top = lines.pull_front();

            // ---

            web::http::request request;

            each(lines, [&](text &line) -> void {
              auto [property, value] = line.split(": ");
              request.header.set(property, value);
            });

            // ---

            auto parts = text::split<pstd::list>(top, " ");

            request.method = parts[0];

            // ---

            auto comps = url::parse(parts[1]);

            request.username = comps.username;
            
            request.password = comps.password;

            request.queries = comps.queries;

            request.path = comps.path;

            request.dest = comps.path;

            // ---

            auto length = request.header.get("content-length");

            if (length) {
              auto i = text::to<std::size_t>(*length);
              auto start = end + 4;

              while (buffer.length() - start < i)
                inc->recv(buffer);

              request.body = buffer.slice(start, start + i);

              // ---

              auto type = request.header.get("content-type");

              if (!type)
                continue;

              // ---

              if (type->starts_with("application/x-www-form-urlencoded")) {
                auto parts = text::split<pstd::list>(request.body, "&");

                using ty = decltype(request.params);

                request.params = reduce<ty>(parts, {}, [](ty it, text next) -> ty {
                    auto [parameter, value] = next.split("=");
                    it[parameter] = value;
                    return it;
                  }
                );
              }
            }

            // ---

            socket s(inc, request);

            handle(s);

            // ---

            if (!inc->connected())
              return;

            // ---

            s.out("path not found", {
              .code = web::http::code::NOT_FOUND
            });
          }
        }
      }

    public:
      server(const text &cert_pem, const text &key_pem): to(5) {
        it = make_unique<ssl::server>(cert_pem, key_pem);
      }

      server(): to(5) {
        it = make_unique<tcp::server>();
      }

      // ---

      void timeout(int seconds) {
        to = std::chrono::seconds(seconds);
      }

      // ---

      void listen(uint16_t port, text ip_address = "127.0.0.1") {
        it->listen(port, ip_address);
      }

      void run() {
        auto timeout = std::chrono::milliseconds(250);

        while (!0) {
          threading::sleep(timeout);

          auto next = it->accept();

          if (!next)
            continue;

          threading::call(
            this,
            &server::incoming,
            next
          );
        }
      }
  };
}