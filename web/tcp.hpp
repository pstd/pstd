#pragma once

#include "./net.hpp"

#include <mutex>

namespace pstd::web::tcp {
  class socket : public net::socket {
    private:
      mutable std::mutex mutex;
      net::addrdata ad;
      bool linked;
      int id;

      void reset() {
        std::lock_guard<std::mutex> lock(mutex);

        if (id >= 0) {
          net::close(id);
          id = EOF;
        }

        linked = !1;
      }

    public:
      pstd_error(connect_error);
      pstd_error(write_error);

      // ---

      // copy constructor
      socket(const socket &other): linked(other.linked), id(other.id), ad(other.ad) {

      }

      // ---

      socket(int id, net::addrdata ad): linked(!0), id(id), ad(ad) {

      }


      socket(): linked(!1), id(EOF) {

      }

      ~socket() {
        std::lock_guard<std::mutex> lock(mutex);

        close();
      }

      // ---

      bool connected() const override {
        std::lock_guard<std::mutex> lock(mutex);
        return linked;
      }

      bool connect(uint16_t port, text address) override {
        std::lock_guard<std::mutex> lock(mutex);

        if (linked)
          throw connect_error("socket is already connected");

        // ---

        pstd::list<text> ips;

        if (!net::is_ip(address))
          ips = net::resolve(address);

        else
          ips.push_back(address);

        // ---

        ad = net::addrdata();

        ad.protocol(IPPROTO_TCP);
        ad.type(SOCK_STREAM);

        // ---

        for (auto ip : ips) {
          ad.ip_address(ip);
          ad.port(port);

          // ---

          id = net::open(ad);

          if (id == EOF) {
            reset();
            continue;
          }

          // ---

          if (!net::connect(id, ad)) {
            reset();
            continue;
          }

          // ---

          fcntl::set(id, O_NONBLOCK);

          linked = !0;

          break;
        }

        return linked;
      }

      void close() override {
        reset();
      }

      // ---

      int write(const text &input) override {
        std::lock_guard<std::mutex> lock(mutex);

        if (!linked)
          return EOF;

        return net::write(id, input);
      }

      int recv(text &buffer, std::size_t length = 1024) override {
        std::lock_guard<std::mutex> lock(mutex);

        if (!linked)
          return EOF;

        // ---

        auto bytes = net::recv(id, buffer, length);

        if (bytes == EOF) {
          linked = !1;
          return EOF;
        }

        // ---

        return bytes;
      }

      uint16_t port() const override {
        std::lock_guard<std::mutex> lock(mutex);

        return ad.port();
      }

      text ip() const override {
        std::lock_guard<std::mutex> lock(mutex);
        
        return ad.ip_address();
      }
  };

  // ---

  class endpoint {
    private:
      net::addrdata ad;
      int id;

    public:
      pstd_error(accept_error);

      // ---

      endpoint(int id, net::addrdata ad): id(id), ad(ad) {

      }

      // ---

      basic_pointer<socket> accept() {
        auto [cid, cad] = net::accept(id, ad);

        if (cid == EOF)
          throw accept_error(
            text("::accept() error: %d\n")
              .format(errno)
              .raw()
          );

        if (cid == 0)
          return {};

        // ---

        fcntl::set(cid, O_NONBLOCK);

        return make_pointer<socket>(cid, cad);
      }

      void close() {
        net::close(id);
      }
  };

  // ---

  class server : public net::server {
    private:
      list<endpoint> ends;

    public:
      pstd_error(accept_error);
      pstd_error(listen_error);

      // ---

      void listen(uint16_t port, text ip_address = "127.0.0.1") override {
        auto ad = net::addrdata();

        ad.protocol(IPPROTO_TCP);
        ad.type(SOCK_STREAM);
        ad.ip_address(ip_address);
        ad.port(port);

        // ---

        auto id = net::open(ad);

        if (id == EOF)
          throw listen_error("unable to open server socket");

        fcntl::set(id, O_NONBLOCK);

        if (!net::bind(id, ad))
          throw listen_error(
            text("unable to bind server socket at %s:%d")
              .format(ip_address, port)
              .raw()
          );

        if (!net::listen(id))
          throw listen_error("unable to listen to server socket");

        // ---
        
        auto optval = 1;
        
        setsockopt(id, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

        // ---

        auto ep = endpoint(
          id,
          ad
        );

        ends.push_back(ep);
      }

      basic_pointer<net::socket> accept() override {
        for (endpoint &ep : ends) {
          basic_pointer<socket> next = ep.accept();

          if (!next)
            continue;

          return next;
        }

        return {};
      }
  };
}