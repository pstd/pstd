#pragma once

#include "./http.hpp"
#include "./ssl.hpp"

namespace pstd::web::request {
  pstd_error(request_error);

  namespace internal {
    basic_pointer<net::socket> create_socket(const url::components &parts) {
      if (parts.scheme == "https" || parts.port == 443)
        return make_pointer<ssl::socket>();

      return make_pointer<tcp::socket>();
    }

    void set_headers(http::header &headers, const url::components &parts) {
      if (!headers.exists("connection"))
        headers.set("connection", "close");

      if (!headers.exists("host"))
        headers.set("host", parts.host);
    }
 
    // ---
    
    http::response make_request(const text &method, const text &address, http::header &headers, const text &data) {
      auto parts = url::parse(address);

      if (parts.scheme.empty())
        throw request_error("no scheme provided");

      // ---

      auto socket = create_socket(parts);

      if (!socket->connect(parts.port, parts.host))
        throw request_error(
          text("unable to establish connection %s (%d)")
            .format(parts.host, parts.port)
            .raw()
        );

      // ---
      
      set_headers(headers, parts);

      // ---

      auto list = pstd::list<text>();

      list.push_back(
        text("%s %s HTTP/1.1").format(
          method.upper(),
          parts.absolute_path()
        )
      );

      for (auto [property, value] : headers)
        list.push_back(
          text("%s: %s").format(property, value)
        );

      list.push_back({});

      if (!data.empty())
        list.push_back(data);

      list.push_back({});

      // ---

      auto out = text::concat(list, "\r\n");

      socket->write(out);

      // ---

      auto resp = http::parse_response(socket);

      if (!resp)
        throw request_error(
          text("unable to parse response")
            .raw()
        );

      return *resp;
    }
  }

  // ---

  http::response get(text address, http::header headers) {
    return internal::make_request("get", address, headers, {});
  }

  http::response post(text address, http::header headers, text data) {
    return internal::make_request("post", address, headers, data);
  }

  http::response del(text address, http::header headers) {
    return internal::make_request("delete", address, headers, {});
  }

  http::response put(text address, http::header headers, text data) {
    return internal::make_request("put", address, headers, data);
  }

  // ---

  class instance {
    private:
      http::header default_headers; 
      text base_url;

    public:
      instance(text url, http::header headers = {}): base_url(url), default_headers(headers) {

      }

      http::response get(text path, http::header headers) const {
        auto combined_headers = default_headers;

        for (auto [property, value] : headers)
          combined_headers.set(property, value);

        // ---

        return request::get(
          url::concat(base_url, path),
          combined_headers
        );
      }

      http::response post(text path, http::header headers, text data) const {
        auto combined_headers = default_headers;

        for (auto [property, value] : headers)
          combined_headers.set(property, value);

        // ---

        return request::post(
          url::concat(base_url, path),
          combined_headers,
          data
        );
      }

      http::response del(text path, http::header headers) const {
        auto combined_headers = default_headers;

        for (auto [property, value] : headers)
          combined_headers.set(property, value);

        // ---

        return request::del(
          url::concat(base_url, path),
          combined_headers
        );
      }

      http::response put(text path, http::header headers, text data) const {
        auto combined_headers = default_headers;

        for (auto [property, value] : headers)
          combined_headers.set(property, value);

        // ---

        return request::put(
          url::concat(base_url, path),
          combined_headers,
          data
        );
      }
  };
}