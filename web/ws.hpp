#pragma once

#include "../crypto/base64.hpp"
#include "../crypto/sha1.hpp"
#include "../random.hpp"
#include "../json.hpp"
#include "./http.hpp"
#include "./net.hpp"
#include "./ssl.hpp"

namespace pstd::web::ws {
  constexpr int OPCODE_CONTINUATION = 0b00000000;
  constexpr int OPCODE_TEXT = 0b00000001;
  constexpr int OPCODE_BINARY = 0b00000010;
  constexpr int OPCODE_CLOSE = 0b00001000;
  constexpr int OPCODE_PING = 0b00001001;
  constexpr int OPCODE_PONG = 0b00001010;

  constexpr int MASK = 0b10000000;
  constexpr int FIN = 0b10000000;
  constexpr int MASKLEN = 4;

  enum class opcode {
    CONTINUATION = OPCODE_CONTINUATION,
    TEXT,
    BINARY,
    CLOSE = OPCODE_CLOSE,
    PING = OPCODE_PING,
    PONG = OPCODE_PONG
  };

  // -----

  constexpr int STATUS_NORMAL = 1000;
  constexpr int STATUS_GOING_AWAY = 1001;
  constexpr int STATUS_PROTO_ERROR = 1002;
  constexpr int STATUS_UNSUPPORTED_DATA = 1003;
  constexpr int STATUS_NO_STATUS = 1005;
  constexpr int STATUS_RESERVED = 1006; // not to be used
  constexpr int STATUS_INVALID_PAYLOAD = 1007;
  constexpr int STATUS_POLICY_VIOLATION = 1008;
  constexpr int STATUS_MSG_TOO_BIG = 1009;
  constexpr int STATUS_EXT_REQUIRED = 1010;
  constexpr int STATUS_INTERNAL_EP_ERROR = 1011;
  constexpr int STATUS_SERVICE_RESTART = 1012;
  constexpr int STATUS_TRY_AGAIN_LATER = 1013;
  constexpr int STATUS_BAD_GATEWAY = 1014;
  constexpr int STATUS_TLS_HANDSHAKE = 1015;

  constexpr int STATUS_SUBPROTO_ERROR = 3000;
  constexpr int STATUS_INVALID_SUBPROTO_DATA = 3001;

  enum class response {
    NORMAL = STATUS_NORMAL,
    GOING_AWAY,
    PROTO_ERROR,
    UNSUPPORTED_DATA,

    NO_STATUS = STATUS_NO_STATUS,
    RESERVED,
    INVALID_PAYLOAD,
    POLICY_VIOLATION,
    MSG_TOO_BIG,
    EXT_REQUIRED,
    INTERNAL_EP_ERROR,
    SERVICE_RESTART,
    TRY_AGAIN_LATER,
    BAD_GATEWAY,
    TLS_HANDSHAKE,

    SUBPROTO_ERROR = STATUS_SUBPROTO_ERROR,
    INVALID_SUBPROTO_DATA
  };

  // ---

  text createHandshakeKey() {
    auto rand = random::string(12, "ABCDEFGHIJKLMNOPQRSTUVQXYZabcdefghijklmnopqrstuvwxyz0123456789");
    auto hash = crypto::sha1(rand + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11");
    return crypto::base64(hash);
  }

  // ---

  uint16_t UInt16FromUInt8(vector<uint8_t> &vect, int &pos) {
    return ((uint16_t) vect[pos++] << 8) |
           ((uint16_t) vect[pos++] << 0);
  }

  uint64_t UInt64FromUInt8(vector<uint8_t> &vect, int &pos) {
    return ((uint64_t) vect[pos++] << 56) |
           ((uint64_t) vect[pos++] << 48) |
           ((uint64_t) vect[pos++] << 40) |
           ((uint64_t) vect[pos++] << 32) |
           ((uint64_t) vect[pos++] << 24) |
           ((uint64_t) vect[pos++] << 16) |
           ((uint64_t) vect[pos++] <<  8) |
           ((uint64_t) vect[pos++] <<  0);
  }

  map<opcode, text> decode(text recvd, vector<uint8_t> &rxbuf) {
    vector<uint8_t> frame(
      recvd.begin(),
      recvd.end()
    );

    // ---

    // If we put stuff on the backburner, we need to put it back
    // to the front and try again.

    if (rxbuf.size()) {
      frame.insert(
        frame.begin(),
        rxbuf.begin(),
        rxbuf.end()
      );

      rxbuf.clear();
    }

    // ---

    map<opcode, text> out;

    while (!0) {
      if (frame.size() < 2) {
        rxbuf.insert(
          rxbuf.end(),
          frame.begin(),
          frame.end()
        );

        break;
      }

      // ---

      int pos = 0;

      auto byte1 = frame.at(pos++);
      auto byte2 = frame.at(pos++);

      // ---

      auto op = static_cast<opcode>(byte1 & 0x0F);
      auto masked = (byte2 & MASK) != 0;
      // auto fin = (byte1 & FIN) != 0;
      auto length = byte2 & 0x7f;

      // ---

      if (length == 0x7F)
        length = UInt64FromUInt8(frame, pos);

      else if (length == 0x7E)
        length = UInt16FromUInt8(frame, pos);

      // ---

      // If the data we receive is not big enough, we need
      // to put it on the backburner.

      auto total = pos + length;

      // ---

      if (frame.size() < total) {
        rxbuf.insert(
          rxbuf.end(),
          frame.begin(),
          frame.end()
        );

        break;
      }

      // ----

      if (masked) // I wouldn't be able to care... less
        break;

      else {
        if (op == opcode::TEXT) {
          text payload(
            frame.begin() + pos,
            frame.begin() + total
          );

          out.insert({
            opcode::TEXT,
            payload
          });
        }

        else if (op == opcode::CLOSE) {
          out.insert({
            opcode::CLOSE,
            {}
          });
        }

        else {
          
        }

        // ---

        frame.erase(
          frame.begin(),

          std::next(
            frame.begin(),
            total
          )
        );
      }
    }

    return out;
  }

  // ---

  vector<uint8_t> UInt64ToUInt8(ulong i) {
    auto vect = vector<uint8_t>();

    vect.push_back((i >> 56) & 0xFF);
    vect.push_back((i >> 48) & 0xFF);
    vect.push_back((i >> 40) & 0xFF);
    vect.push_back((i >> 32) & 0xFF);
    vect.push_back((i >> 24) & 0xFF);
    vect.push_back((i >> 16) & 0xFF);
    vect.push_back((i >>  8) & 0xFF);
    vect.push_back((i >>  0) & 0xFF);

    return vect;
  }

  vector<uint8_t> UInt16ToUInt8(ulong i) {
    auto vect = vector<uint8_t>();

    vect.push_back((i >> 8) & 0xFF);
    vect.push_back((i >> 0) & 0xFF);

    return vect;
  }

  vector<uint8_t> createMaskingKey() {
    auto out = vector<uint8_t>();

    for (auto i = 0; i < MASKLEN; i++) {
      auto next = random::number(0, 255);
      out.push_back(next);
    }

    return out;
  }

  text encode(bool masked, opcode op, text input) {
    text frame;

    frame.push_back(
      FIN | static_cast<uint8_t>(op)
    );

    // ---

    auto length = input.length();

    if (length <= 125)
      frame.push_back(
        static_cast<uint8_t>(length) | (masked ? MASK : 0)
      );

    else if (length >= 126 && length <= 65565) {
      frame.push_back(0x7E | (masked ? MASK : 0));

      for (auto i : UInt16ToUInt8(length))
        frame.push_back(i);
    }

    else {
      frame.push_back(0x7F | (masked ? MASK : 0));

      for (auto i : UInt64ToUInt8(length))
        frame.push_back(i);
    }

    // ---

    if (masked) {
      for (auto next : vector<uint8_t>({0, 0, 0, 0}))
        frame.push_back(next);
    }

    frame.insert(
      frame.end(),
      input.begin(),
      input.end()
    );
    
    return frame;
  }

  // ---

  class socket {
    private:
      basic_pointer<net::socket> it;
      url::components parts;
      vector<uint8_t> rxbuf;
      text recvd;

    public:
      socket(const text& href) {
        parts = url::parse(href);
      }

      bool connected() {
        return it->connected();
      }

      bool connect() {
        if (it && it->connected())
          return !1;

        // ---

        if (parts.scheme == "wss")
          it = make_pointer<ssl::socket>();
          
        else
          it = make_pointer<tcp::socket>();

        // ---

        if (!it->connect(parts.port, parts.host))
          return !1;

        // ---

        list<text> lines;

        lines.push_back(
          text("GET %s HTTP/1.1")
            .format(parts.absolute_path())
        );

        // ---

        auto headers = http::header({
          {"host", parts.host},
          {"connection", "upgrade"},
          {"upgrade", "websocket"},
          {"sec-websocket-key", createHandshakeKey()},
          {"sec-websocket-version", "13"}
        });

        for (auto [property, value] : headers)
          lines.push_back(
            text("%s: %s").format(property, value)
          );

        // ---

        lines.push_back({});

        lines.push_back({});

        // ---

        auto out = text::concat(lines, "\r\n");

        it->write(out);

        // ---

        text buffer;

        while (!0) {
          it->recv(buffer);

          if (buffer.length())
            break;
        }

        // ---

        text top, line;

        nest(top) = buffer.split("\r\n\r\n");

        nest(line) = text::split<list>(top, "\r\n");

        if (line != "HTTP/1.1 101 Switching Protocols")
          return !1;

        return !0;
      }
  
      void write(json data) {
        auto out = ws::encode(
          !0,
          ws::opcode::TEXT,
          data
        );

        it->write(out);
      }

      int recv(map<ws::opcode, text> &map) {
        auto len = it->recv(recvd);

        if (len != EOF) {
          for (auto [op, next] : decode(recvd, rxbuf))
            map.insert({
              op,
              next
            });

          recvd.clear();
        }

        return len;
      }

      void close() {
        it->close();
      }
  };
}