#pragma once

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/poll.h>
#include <unistd.h>
#include <cstring>
#include <netdb.h>
#include <errno.h>
#include <list>

#include "../pointer/basic.hpp"
#include "../vector.hpp"
#include "../fcntl.hpp"
#include "../text.hpp"
#include "../map.hpp"

namespace pstd::web::net {
  pstd_error(exception);

  // ---

  bool is_ipv6(const text &input) {
    struct sockaddr_in6 sa6;
    return inet_pton(AF_INET6, &input[0], &(sa6.sin6_addr)) != 0;
  }

  bool is_ipv4(const text &input) {
    struct sockaddr_in sa;
    return inet_pton(AF_INET, &input[0], &(sa.sin_addr)) != 0;
  }

  bool is_ip(const text &input) {
    return is_ipv6(input) || is_ipv4(input);
  }

  // ---

  struct addrdata {
    private:
      sockaddr_storage ai_storage;
      int ai_protocol;         // use 0 for "any"
      int ai_family;           // AF_INET, AF_INET6, AF_UNSPEC
      int ai_type;             // SOCK_STREAM, SOCK_DGRAM

      // ---

      template <typename T>
      const T* cast() const {
        return reinterpret_cast<const T*>(&ai_storage);
      }

      template <typename T>
      T* cast() {
        return reinterpret_cast<T*>(&ai_storage);
      }

    public:
      addrdata& operator=(const addrdata& other) {
        if (this != &other) {
          std::memcpy(&ai_storage, &other.ai_storage, sizeof(sockaddr_storage));

          ai_protocol = other.ai_protocol;
          ai_family = other.ai_family;
          ai_type = other.ai_type;
        }

        return *this;
      }

      addrdata(const addrdata& other) {
        std::memcpy(&ai_storage, &other.ai_storage, sizeof(sockaddr_storage));

        ai_protocol = other.ai_protocol;
        ai_family = other.ai_family;
        ai_type = other.ai_type;
      }

      // ---

      addrdata(const sockaddr_storage& storage) {
        std::memcpy(&ai_storage, &storage, sizeof(sockaddr_storage));
        
        ai_protocol = 0; // Default value for protocol
        ai_family = storage.ss_family; // Extract the family from storage
        ai_type = 0; // Default value for type
      }

      addrdata(addrinfo* ai) {
        if (ai->ai_family == AF_INET6)
          std::memcpy(
            &ai_storage,
            ai->ai_addr,
            sizeof(sockaddr_in6)
          );
        
        else if (ai->ai_family == AF_INET)
          std::memcpy(
            &ai_storage,
            ai->ai_addr,
            sizeof(sockaddr_in)
          );
        
        else
          throw exception("addrdata: unsupported address family");
    
        // ---

        ai_protocol = ai->ai_protocol;
        ai_family = ai->ai_family;
        ai_type = ai->ai_socktype;
      }

      addrdata(): ai_protocol(0), ai_family(AF_UNSPEC), ai_type(0) {
        std::memset(&ai_storage, 0, sizeof(sockaddr_storage));
      }

      // ---

      struct iterator {
        private:
          const addrdata* ad;
          int id;
        
        public:
          using iterator_category = std::forward_iterator_tag;
          using difference_type = std::ptrdiff_t;
          using value_type = duo<text, int>;
          using reference = value_type&;
          using pointer = value_type*;

          iterator(const addrdata* ad, int index) : ad(ad), id(index) {

          }

          value_type operator*() const {
            switch (id) {
              case 0: return {"protocol", ad->ai_protocol};
              case 1: return {"family", ad->ai_family};
              case 2: return {"type", ad->ai_type};
              default: return {"", EOF};
            }
          }

          iterator& operator++() {
            id++;
            return *this;
          }

          iterator operator++(int) {
            iterator tmp = *this;
            ++(*this);
            return tmp;
          }

          bool operator==(const iterator& other) const {
            return id == other.id && ad == other.ad;
          }

          bool operator!=(const iterator& other) const {
            return !(*this == other);
          }
      };

      // ---

      iterator begin() const {
        return iterator(this, 0);
      }

      iterator end() const {
        return iterator(this, 3);
      }

      // ---

      void protocol(int input) {
        ai_protocol = input;
      }

      void family(int family) {
        ai_family = family;
      }

      void type(int input) {
        ai_type = input;
      }

      void ip_address(const text &input) {
        if (is_ipv6(input)) {
          auto addr6 = cast<sockaddr_in6>();

          addr6->sin6_family = AF_INET6;

          ai_family = AF_INET6;

          inet_pton(
            AF_INET6,
            &input[0],
            &(addr6->sin6_addr)
          );
        }

        else if (is_ipv4(input)) {
          auto addr4 = cast<sockaddr_in>();

          addr4->sin_family = AF_INET;

          ai_family = AF_INET;

          inet_pton(
            AF_INET,
            &input[0],
            &(addr4->sin_addr)
          );
        }

        else
          throw exception("addrdata: invalid ip address");
      }

      void port(uint16_t port) {
        switch (ai_family) {
          case AF_INET6: {
            auto addr6 = cast<sockaddr_in6>();
            addr6->sin6_port = htons(port);
            break;
          }

          case AF_INET: {
            auto addr4 = cast<sockaddr_in>();
            addr4->sin_port = htons(port);
            break;
          }

          default:
            throw exception("addrdata: invalid address family");
        }
      }
  
      // ---

      int protocol() const {
        return ai_protocol;
      }

      int type() const {
        return ai_type;
      }

      // This will be set when the ip address is set
      int family() const {
        return ai_family;
      }

      text ip_address() const {
        text::value_type ip[INET6_ADDRSTRLEN];

        if (ai_family == AF_INET6) {
          auto addr6 = cast<sockaddr_in6>();

          inet_ntop(
            AF_INET6,
            &(addr6->sin6_addr),
            ip,
            INET6_ADDRSTRLEN
          );
        }

        else if (ai_family == AF_INET) {
          auto addr4 = cast<sockaddr_in>();

          inet_ntop(
            AF_INET,
            &(addr4->sin_addr),
            ip,
            INET6_ADDRSTRLEN
          );
        }

        else
          throw exception("ip_address(): invalid address family");

        return ip;
      }

      uint16_t port() const {
        if (ai_family == AF_INET6) {
          const sockaddr_in6* addr6 = reinterpret_cast<const sockaddr_in6*>(&ai_storage);
          return ntohs(addr6->sin6_port);
        }
        
        else if (ai_family == AF_INET) {
          const sockaddr_in* addr4 = reinterpret_cast<const sockaddr_in*>(&ai_storage);
          return ntohs(addr4->sin_port);
        }

        else
          throw exception("port(): invalid address family");
      }
  
      const ::sockaddr* addr() const {
        return cast<::sockaddr>();
      }

      ::sockaddr* addr() {
        return cast<::sockaddr>();
      }

      socklen_t addrlen() const {
        if (ai_family == AF_INET6)
          return sizeof(sockaddr_in6);
        
        else if (ai_family == AF_INET)
          return sizeof(sockaddr_in);
        
        else
          return sizeof(sockaddr_storage);
          // throw exception("addrlen(): unsupported address family");
      }
  };

  // ---

  list<text> resolve(text domain) {
    if (is_ip(domain))
      throw exception(
        text("resolve: invalid domain provided: %s")
          .format(domain)
          .raw()
      );

    // ---

    addrinfo* result = nullptr;
    addrinfo hints;

    std::memset(&hints, 0, sizeof(struct addrinfo));

    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_CANONNAME;
    hints.ai_family = AF_UNSPEC;

    int ret = getaddrinfo(
      &domain[0],
      nullptr,
      &hints,
      &result
    );
    
    if (ret != 0) {
      freeaddrinfo(result);

      throw exception(
        text("resolve: getaddrinfo failed for %s: %d (%s)")
          .format(domain, ret, gai_strerror(ret))
          .raw()
      );
    }

    // ---

    auto out = pstd::list<text>();

    for (auto ai = result; ai != nullptr; ai = ai->ai_next) {
      auto ip = addrdata(ai).ip_address();
      out.push_back(ip);
    }

    // ---

    freeaddrinfo(result);

    return out;
  }

  // ---

  int open(const addrdata &ad) {
    int id = ::socket(
      ad.family(),
      ad.type(),
      ad.protocol()
    );

    return id;
  }

  bool close(int id) {
    auto i = ::close(id);
    return i != EOF;
  }

  // ---

  bool connect(int id, const addrdata &ad) {
    auto i = ::connect(
      id,
      ad.addr(),
      ad.addrlen()
    );

    return i != EOF;
  }

  bool bind(int id, const addrdata &ad) {
    auto i = ::bind(
      id,
      ad.addr(),
      ad.addrlen()
    );

    return i != EOF;
  }

  bool listen(int id, int max = SOMAXCONN) {
    auto i = ::listen(id, max);
    return i != EOF;
  }

  duo<int, addrdata> accept(int id, addrdata &ad) {
    auto cad = addrdata();

    auto cal = cad.addrlen();

    auto cid = ::accept(
      id,
      cad.addr(),
      &cal
    );

    if (cid == EOF) {
      if (errno == EWOULDBLOCK || errno == EAGAIN)
        return {0, {}};

      return {EOF, {}};
    }

    // ---

    // needed for cad.port()
    cad.family(ad.family());

    return {
      cid,
      cad
    };
  }

  // ---

  int recv(int id, text &buffer, std::size_t length = 1024) {
    vector<text::value_type> recvd(length);

    auto bytes = ::recv(id, &recvd[0], length, 0);

    if (bytes == EOF) {
      if (errno == EWOULDBLOCK || errno == EAGAIN)
        return 0; // No data available, non-blocking mode

      // An actual error occurred
      return EOF;
    }

    // Connection closed by the peer
    if (bytes == 0)
      return EOF;

    // ---

    auto begin = recvd.begin();

    buffer += text(
      begin,

      std::next(
        begin,
        bytes
      )
    );

    return bytes;
  }

  int write(int id, const text &input) {
    return ::send(id, &input[0], input.length(), 0);
  }

  // ---

  class socket {
    public:
      virtual int recv(text &buffer, std::size_t length = 1024) = 0;
      virtual bool connect(uint16_t port, text host) = 0;
      virtual int write(const text &input) = 0;
      virtual bool connected() const = 0;
      virtual void close() = 0;

      virtual uint16_t port() const = 0;
      virtual text ip() const = 0;

      virtual ~socket() = default;
  };

  // ---

  class server {
    public:
      virtual void listen(uint16_t port, text ip_address = "127.0.0.1") = 0;
      virtual basic_pointer<socket> accept() = 0;

      virtual ~server() = default;
  };
}