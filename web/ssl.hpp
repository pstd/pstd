#pragma once

#include "../thread.hpp"
#include "./tcp.hpp"

#include <openssl/ssl.h>
#include <openssl/err.h>

namespace pstd::web::ssl {
  text error_text(int error) {
    switch (error) {
      case SSL_ERROR_NONE:
        return "No error occurred";

      case SSL_ERROR_ZERO_RETURN:
        return "The connection was closed cleanly";

      case SSL_ERROR_WANT_READ:
        return "The operation did not complete and needs to be called again for reading";

      case SSL_ERROR_WANT_WRITE:
        return "The operation did not complete and needs to be called again for writing";

      case SSL_ERROR_WANT_CONNECT:
        return "The operation did not complete and needs to be called again to connect";

      case SSL_ERROR_WANT_ACCEPT:
        return "The operation did not complete and needs to be called again to accept";

      case SSL_ERROR_WANT_X509_LOOKUP:
        return "The operation did not complete because a certificate lookup is needed";

      case SSL_ERROR_SYSCALL:
        return "A system call error occurred";

      case SSL_ERROR_SSL:
        return "A protocol error occurred within the SSL/TLS library";

      case SSL_ERROR_WANT_ASYNC:
        return "The operation did not complete because asynchronous operations are needed";

      case SSL_ERROR_WANT_ASYNC_JOB:
        return "The operation did not complete because an asynchronous job is pending";

      case SSL_ERROR_WANT_CLIENT_HELLO_CB:
        return "The operation did not complete because a client hello callback is needed";

      case SSL_ERROR_WANT_RETRY_VERIFY:
        return "The operation did not complete because a retry of certificate verification is needed";

      default:
        return "Unknown SSL error";
    }
  }

  // ---

  static bool initialized = !1;

  void init() {
    if (initialized)
      return;

    SSL_load_error_strings();
    SSL_library_init();
  }

  SSL* open(int id, SSL_CTX* ctx) {
    auto sid = ::SSL_new(ctx);

    if (!sid)
      return nullptr;

    // ---

    auto i = ::SSL_set_fd(sid, id);

    if (i != 1)
      return nullptr;

    return sid;
  }

  void close(SSL* sid) {
    SSL_shutdown(sid);
    SSL_free(sid);
  }

  bool connect(SSL* sid) {
    auto i = ::SSL_connect(sid);
    return i == 1;
  }

  int recv(SSL* sid, text &buffer, std::size_t length = 1024) {
    vector<text::value_type> recvd(length);

    auto bytes = ::SSL_read(sid, &recvd[0], length);

    if (bytes <= 0) {
      int err = SSL_get_error(sid, bytes);

      if (err == SSL_ERROR_WANT_READ || err == SSL_ERROR_WANT_WRITE)
        return 0; // Try again later

      // An actual error occurred
      return EOF;
    }

    // Connection closed by the peer
    if (bytes == 0)
      return EOF;

    // ---

    auto begin = recvd.begin();

    auto stop = std::next(
      begin,
      bytes
    );

    // ---

    buffer += text(
      begin,
      stop
    );

    return bytes;
  }

  int write(SSL* sid, const text &input) {
    return ::SSL_write(sid, &input[0], input.length());
  }

  bool accept(SSL* sid) {
    return ::SSL_accept(sid) == 1;
  }

  // ---

  class socket : public net::socket {
    private:
      mutable std::recursive_mutex mutex;
      net::addrdata ad;
      SSL_CTX* ctx;
      bool linked;
      SSL* sid;
      int id;

      void reset() {
        std::lock_guard<std::recursive_mutex> lock(mutex);

        linked = !1;

        if (sid) {
          ssl::close(sid);
          sid = nullptr;
        }

        if (id >= 0) {
          net::close(id);
          id = EOF;
        }
      }

    public:
      pstd_error(connect_error);
      pstd_error(write_error);
      pstd_error(exception);

      // ---

      // copy constructor
      socket(const socket &other): linked(other.linked), id(other.id), ctx(other.ctx), sid(other.sid) {

      }

      // ---

      // to be used by server
      socket(int id, net::addrdata ad, SSL* sid): linked(!0), id(id), ad(ad), ctx(nullptr), sid(sid) {
        init();

        ctx = SSL_CTX_new(SSLv23_client_method());

        if (!ctx)
          throw exception("failed to create SSL context");
      }

      socket(): linked(!1), id(EOF), ctx(nullptr), sid(nullptr) {
        init();

        ctx = SSL_CTX_new(SSLv23_client_method());

        if (!ctx)
          throw exception("failed to create SSL context");
      }

      ~socket() {
        std::lock_guard<std::recursive_mutex> lock(mutex);

        reset();

        if (ctx) {
          SSL_CTX_free(ctx);
          ctx = nullptr;
        }
      }

      // ---

      bool connected() const override {
        std::lock_guard<std::recursive_mutex> lock(mutex);
        return linked;
      }

      bool connect(uint16_t port, text address) override {
        std::lock_guard<std::recursive_mutex> lock(mutex);

        if (linked)
          throw connect_error("socket is already connected");

        // ---

        pstd::list<text> ips;

        if (!net::is_ip(address))
          ips = net::resolve(address);

        else
          ips.push_back(address);

        // ---

        ad = net::addrdata();

        ad.protocol(IPPROTO_TCP);
        ad.type(SOCK_STREAM);

        // ---

        for (auto ip : ips) {
          ad.ip_address(ip);
          ad.port(port);

          // ---

          id = net::open(ad);

          if (id == EOF) {
            reset();
            continue;
          }

          // ---

          if (!net::connect(id, ad)) {
            reset();
            continue;
          }

          // ---

          sid = ssl::open(id, ctx);

          if (!sid) {
            reset();
            continue;
          }

          // ---

          if (!ssl::connect(sid)) {
            reset();
            continue;
          }

          // ---

          fcntl::set(id, O_NONBLOCK);

          linked = !0;

          break;
        }

        return linked;
      }

      void close() {
        reset();
      }

      // ---

      int write(const text &input) override {
        std::lock_guard<std::recursive_mutex> lock(mutex);

        if (!linked)
          return EOF;

        return ssl::write(sid, input);
      }

      int recv(text &buffer, std::size_t length = 1024) override {
        std::lock_guard<std::recursive_mutex> lock(mutex);

        if (!linked)
          return EOF;

        // ---

        auto bytes = ssl::recv(sid, buffer, length);

        if (bytes == EOF) {
          linked = !1;
          return EOF;
        }

        // ---

        return bytes;
      }

      uint16_t port() const override {
        std::lock_guard<std::recursive_mutex> lock(mutex);
        return ad.port();
      }

      text ip() const override {
        std::lock_guard<std::recursive_mutex> lock(mutex);
        return ad.ip_address();
      }
  };

  // ---

  bool context_certificate_pem(SSL_CTX *ctx, const text &path) {
    auto i = SSL_CTX_use_certificate_file(ctx, &path[0], SSL_FILETYPE_PEM);
    return i > 0;
  }

  bool context_private_key_pem(SSL_CTX *ctx, const text &path) {
    auto i = SSL_CTX_use_PrivateKey_file(ctx, &path[0], SSL_FILETYPE_PEM);
    return i > 0;
  }

  // ---

  class endpoint {
    private:
      net::addrdata ad;
      SSL_CTX* ctx;
      int id;

    public:
      pstd_error(accept_error);

      // ---

      endpoint(int id, SSL_CTX* ctx, net::addrdata ad): id(id), ctx(ctx), ad(ad) {

      }

      basic_pointer<socket> accept() {
        auto [cid, cad] = net::accept(id, ad);

        if (cid == EOF)
          throw accept_error(
            text("::accept() error: %i\n")
              .format(errno)
              .raw()
          );

        if (cid == 0)
          return {};

        // ---

        fcntl::set(cid, O_NONBLOCK);

        auto csid = open(cid, ctx);

        if (!csid) {
          net::close(cid);
          return {};
        }

        // ---

        if (!ssl::accept(csid)) {
          close(csid);
          net::close(cid);
          return {};
        }

        // ---

        return make_pointer<socket>(cid, cad, csid);
      }
  };

  // ---

  class server : public net::server {
    private:
      list<endpoint> ends;
      SSL_CTX* ctx;

    public:
      pstd_error(accept_error);
      pstd_error(listen_error);

      // ---

      server(const text &cert_pem, const text &key_pem) {
        init();

        ctx = SSL_CTX_new(SSLv23_server_method());

        if (!ctx)
          throw exception("unable to establish ssl server context");

        // ---

        if (!context_certificate_pem(ctx, cert_pem))
          throw exception("failed to load ssl server certificate");

        if (!context_private_key_pem(ctx, key_pem))
          throw exception("failed to load ssl server private key");
      }

      void listen(uint16_t port, text ip_address = "127.0.0.1") override {
        auto ad = net::addrdata();

        ad.protocol(IPPROTO_TCP);
        ad.type(SOCK_STREAM);
        ad.ip_address(ip_address);
        ad.port(port);

        // ---

        auto id = net::open(ad);

        if (id == EOF)
          throw listen_error("unable to create ssl server socket");

        fcntl::set(id, O_NONBLOCK);

        if (!net::bind(id, ad))
          throw listen_error("unable to bind ssl server socket");

        if (!net::listen(id))
          throw listen_error("unable to listen to ssl server socket");

        // ---
        
        auto optval = 1;
        
        setsockopt(id, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

        // ---

        auto ep = endpoint(
          id,
          ctx,
          ad
        );

        ends.push_back(ep);
      }

      basic_pointer<net::socket> accept() override {
        for (endpoint &ep : ends) {
          basic_pointer<socket> next = ep.accept();

          if (!next)
            continue;

          return next;
        }

        return {};
      }
  };
}