#pragma once

// Copyright (C) 2024 Dave Perry (dbdii407)

#include <filesystem>
#include <unistd.h>

#include "./maybe.hpp"
#include "./text.hpp"

namespace pstd::fs {
  maybe<text> open(text file) {
    auto fp = fopen(
      file.raw(),
      "r"
    );

    auto out = text();

    while (!0) {
      auto next = fgetc(fp);

      if (next == EOF)
        break;

      out += (char) next;
    }

    auto i = fclose(fp);

    if (i == EOF)
      return {};

    return out;
  }

  text concat(std::initializer_list<text> list) {
    text out;

    for (auto next : list) {
      if (!out.empty() && out.back() != '/')
        out += '/';

      out += next;
    }

    return out;
  }

  text pwd(const text &input) {
    auto pos = input.find_last_of("/");

    if (pos == EOF)
      return {};

    return input.slice(0, pos);
  }

  text file(const text &input) {
    auto pos = input.find_last_of("/");

    if (pos == EOF)
      return {};

    return input.slice(++pos);
  }

  text cwd() {
    return std::filesystem::current_path().c_str();
  }

  // Function to join two text objects with a separator "/" and resolve "." and ".."
  text join(text first, text second) {
    // Use std::filesystem::path to resolve the paths
    std::filesystem::path path1(first.raw());
    std::filesystem::path path2(second.raw());

    // Resolve both paths and concatenate them
    std::filesystem::path resolved_path = (path1 / path2).lexically_normal();

    return resolved_path.c_str();
  }

  // Function to join multiple text objects with a separator "/"
  text join(std::initializer_list<text> paths) {
    text out;

    for (auto& path : paths) {
      out = join(out, path);  // Use the first join function to append each part
    }

    return out;
  }
}