#pragma once

#include "./list.hpp"

#include <fcntl.h>
#include <stdio.h>

namespace pstd::fcntl {
  pstd::list<int> list(int id) {
    pstd::list<int> out;
    
    int fd_flags = ::fcntl(id, F_GETFL);

    if (fd_flags == EOF)
      return out;
    
    // Check each flag using bitwise operations
    if (fd_flags & O_APPEND)
      out.push_back(O_APPEND);

    if (fd_flags & O_ASYNC)
      out.push_back(O_ASYNC);

    if (fd_flags & O_CLOEXEC)
      out.push_back(O_CLOEXEC);

    if (fd_flags & O_CREAT)
      out.push_back(O_CREAT);

    if (fd_flags & O_DIRECT)
      out.push_back(O_DIRECT);

    if (fd_flags & O_DIRECTORY)
      out.push_back(O_DIRECTORY);

    if (fd_flags & O_EXCL)
      out.push_back(O_EXCL);

    if (fd_flags & O_LARGEFILE)
      out.push_back(O_LARGEFILE);

    if (fd_flags & O_NOATIME)
      out.push_back(O_NOATIME);

    if (fd_flags & O_NOCTTY)
      out.push_back(O_NOCTTY);

    if (fd_flags & O_NOFOLLOW)
      out.push_back(O_NOFOLLOW);

    if (fd_flags & O_NONBLOCK)
      out.push_back(O_NONBLOCK);

    if (fd_flags & O_PATH)
      out.push_back(O_PATH);

    if (fd_flags & O_SYNC)
      out.push_back(O_SYNC);

    if (fd_flags & O_TMPFILE)
      out.push_back(O_TMPFILE);

    if (fd_flags & O_TRUNC)
      out.push_back(O_TRUNC);

    if (fd_flags & O_DSYNC)
      out.push_back(O_DSYNC);

    if (fd_flags & O_RSYNC)
      out.push_back(O_RSYNC);

    if (fd_flags & O_NDELAY)
      out.push_back(O_NDELAY);

    if (fd_flags & O_FSYNC)
      out.push_back(O_FSYNC);
    
    return out;
  }

  // ---

  pstd_error(set_error);

  void set(int id, int input) {
    auto vect = list(id);

    if (vect.contains(input))
      throw set_error("flag is already set");

    // ---

    ::fcntl(
      id,
      F_SETFL,
      ::fcntl(id, F_GETFL) | input
    );
  }
}