#pragma once

#include "../text.hpp"

#include <openssl/sha.h>

namespace pstd::crypto {
  text sha1(const text &input) {
    unsigned char hash[SHA_DIGEST_LENGTH];
    
    SHA1(
      reinterpret_cast<const unsigned char *>(input.raw()),
      input.length(),
      hash
    );
    
    text out;

    for (int i = 0; i < SHA_DIGEST_LENGTH; ++i) {
      char buf[3];

      snprintf(
        buf,
        sizeof(buf),
        "%02x",
        hash[i]
      );

      out.push_back(buf);
    }
    
    return out;
  }
};