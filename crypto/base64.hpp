#pragma once

#include "../text.hpp"

namespace pstd::crypto {
  static text b64chars
    = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

  text base64(text input) {
    auto valb = -6;
    auto vala = 0;
    text out;

    for (auto next : input) {
      vala = (vala << 8) + next;
      valb += 8;

      while (valb >= 0) {
        out.push_back(b64chars[(vala >> valb) & 0x3F]);
        valb -= 6;
      }
    }

    if (valb >- 6)
      out.push_back(b64chars[((vala << 8) >> (valb + 8)) & 0x3F]);
    
    while (out.length() % 4)
      out.push_back('=');
      
    return out;
  }
}