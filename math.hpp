#pragma once

namespace pstd {
  template <typename T>
  T power(T base, int exp) {
    if (exp == 0)
      return 1;

    bool negative = (exp < 0);
    T result = 1;

    if (negative)
      exp = -exp;

    while (exp > 0) {
      if (exp % 2 == 1) // If exp is odd
        result *= base;

      base *= base; // Square the base
      exp /= 2;     // Divide the exponent by 2
    }

    if (negative)
      return 1 / result;

    return result;
  }

  inline bool is_digit(char c) {
    return c >= '0' && c <= '9';
  }
}