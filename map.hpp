#pragma once

#include "./vector.hpp"
#include "./duo.hpp"

namespace pstd {
  template <typename K, typename V>
  class map {
    private:
      struct node {
        duo<K, V> it;
        node* prev;
        node* next;

        node(const K& key, const V& value): it(key, value), prev(nullptr), next(nullptr) {

        }
      };

      std::size_t len;
      node* head;
      node* tail;

      node* find_node(const K& key) const {
        for (node* it = head; it != nullptr; it = it->next) {
          if (it->it.a == key)
            return it;
        }

        return nullptr;
      }

    public:
      using value_type = duo<K, V>;

      pstd_error(out_of_range);

      // ---

      map(std::initializer_list<value_type> init): head(nullptr), tail(nullptr), len(0) {
        for (auto [key, value] : init)
          insert(key, value);
      }

      map(): head(nullptr), tail(nullptr), len(0) {

      }

      ~map() {
        clear();
      }

      // ---

      // copy constructor
      map(const map& other): head(nullptr), tail(nullptr), len(0) {
        for (auto [key, value] : other)
          insert(key, value);
      }

      // move constructor
      map(map&& other) noexcept: head(other.head), tail(other.tail), len(other.len) {
        other.head = nullptr;
        other.tail = nullptr;
        other.len = 0;
      }

      // ---

      // copy assignment
      map& operator=(const map& other) {
        if (this == &other)
          return *this;

        clear();

        for (auto [key, value] : other)
          insert(key, value);

        return *this;
      }

      // move assignment
      map& operator=(map&& other) noexcept {
        if (this == &other)
          return *this;

        clear();

        head = other.head;
        tail = other.tail;
        len = other.len;

        other.head = nullptr;
        other.tail = nullptr;
        other.len = 0;

        return *this;
      }

      // ---

      const V& operator[](const K& key) const {
        node* n = find_node(key);

        if (n)
          return n->it.b;

        else
          throw out_of_range("key not found");
      }

      V& operator[](const K& key) {
        auto n = find_node(key);

        if (n)
          return n->it.b;

        // ---

        insert(key, V());

        return tail->it.b;
      }

      // ---

      const V& at(const K& key) const {
        node* n = find_node(key);

        if (n)
          return n->it.b;

        else
          throw out_of_range("key not found");
      }

      V& at(const K& key) {
        node* n = find_node(key);

        if (n)
          return n->it.b;

        else
          throw out_of_range("key not found");
      }
      
      // ---

      std::size_t size() const {
        return len;
      }

      bool empty() const {
        return len == 0;
      }

      void insert(const K& key, const V& value) {
        node* n = find_node(key);

        if (n) {
          n->it.b = value;
          return;
        }

        else {
          node* ny = new node(key, value);
          len++;

          if (!head) {
            head = ny;
            tail = ny;
          }

          else {
            tail->next = ny;
            ny->prev = tail;
            tail = ny;
          }
        }
      }

      void insert(const value_type& value) {
        insert(value.a, value.b);
      }

      void clear() {
        while (head) {
          node* to_delete = head;
          head = head->next;
          delete to_delete;
        }

        tail = nullptr;
        len = 0;
      }
  
      bool contains(const K& key) const {
        return find_node(key) != nullptr;
      }

      vector<K> keys() const {
        vector<K> out;

        for (node* it = head; it; it = it->next)
          out.push_back(it->it.a);

        return out;
      }

      // ---

      class iterator {
        private:
          node* it;

        public:
          using iterator_category = std::bidirectional_iterator_tag;
          using value_type = map<K, V>::value_type;
          using difference_type = std::ptrdiff_t;
          using reference = value_type&;
          using pointer = value_type*;

          // ---

          iterator(node* it): it(it) {

          }

          // ---

          pointer operator->() {
            return &it->it;
          }

          reference operator*() {
            return it->it;
          }

          iterator& operator++() {
            it = it->next;
            return *this;
          }

          iterator operator++(int) {
            iterator out = *this;
            it = it->next;
            return out;
          }

          iterator& operator--() {
            it = it->prev;
            return *this;
          }

          iterator operator--(int) {
            iterator out = *this;
            it = it->prev;
            return out;
          }

          // ---

          bool operator==(const iterator& other) const {
            return it == other.it;
          }

          bool operator!=(const iterator& other) const {
            return it != other.it;
          }
      };

      iterator begin() {
        return iterator(head);
      }

      iterator end() {
        return iterator(nullptr);
      }

      iterator find(const K& key) {
        node* n = find_node(key);

        if (n)
          return iterator(n);

        return end();
      }

      // ---

      class const_iterator {
        private:
          node* it;

        public:
          using iterator_category = std::bidirectional_iterator_tag;
          using value_type = map<K, V>::value_type;
          using difference_type = std::ptrdiff_t;
          using reference = value_type&;
          using pointer = value_type*;

          // ---

          const_iterator(node* it): it(it) {

          }

          // ---

          pointer operator->() {
            return &it->it;
          }

          reference operator*() {
            return it->it;
          }

          const_iterator& operator++() {
            it = it->next;
            return *this;
          }

          const_iterator operator++(int) {
            const_iterator out = *this;
            it = it->next;
            return out;
          }

          const_iterator& operator--() {
            it = it->prev;
            return *this;
          }

          const_iterator operator--(int) {
            const_iterator out = *this;
            it = it->prev;
            return out;
          }

          // ---

          bool operator==(const const_iterator& other) const {
            return it == other.it;
          }

          bool operator!=(const const_iterator& other) const {
            return it != other.it;
          }
      };
  
      const_iterator begin() const {
        return const_iterator(head);
      }

      const_iterator end() const {
        return const_iterator(nullptr);
      }

      // ---

      const_iterator find(const K& key) const {
        node* n = find_node(key);

        if (n)
          return const_iterator(n);

        return end();
      }
  };
}