#pragma once

namespace pstd {
  template <typename A, typename B>
  class duo {
    public:
      using a_type = A;
      using b_type = B;
      
      A a;
      B b;

      template <typename T1, typename T2>
      duo(T1 t1, T2 t2): a(t1), b(t2) {

      }

      duo(A a, B b): a(a), b(b) {

      }

      duo() = default;

      // ---

      bool operator==(const duo &other) const {
        return a == other.a && b == other.b;
      }

      bool operator!=(const duo &other) const {
        return !(*this == other);
      }
  };
}