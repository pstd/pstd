#pragma once

#include "./pointer/unique.hpp"
#include "./error.hpp"
#include "./type.hpp"

namespace pstd {
  class value {
    private:
      struct base {
        virtual bool equals(const base& other) const = 0;
        virtual unique_pointer<base> clone() const = 0;
        virtual ~base() = default;
      };

      template <typename T>
      struct holder : base {
        holder(const T &val): it(val) {

        }

        unique_pointer<base> clone() const override {
          return make_unique<holder<T>>(it);
        }

        bool equals(const base& other) const override {
          if (auto other_holder = dynamic_cast<const holder<T>*>(&other))
            return &it == &(other_holder->it);

          return !1;
        }

        // ---

        T it;
      };

      // ---

      unique_pointer<base> it;
      pstd::type type;
      bool valued;

    public:
      pstd_error(invalid_cast);
      pstd_error(logic_error);

      // ---

      template <typename T>
      value(const T& input): type(typeid(T)), valued(!0) {
        it = make_unique<holder<T>>(input); // Construct `unique_pointer` directly
      }

      // Default constructor
      value() : valued(!1), it(nullptr), type(typeid(void)) {

      }

      // ---

      // Copy constructor
      value(const value& other): it(other.it ? other.it->clone() : nullptr), type(other.type), valued(other.valued) {

      }

      // Move constructor
      value(value&& other) noexcept : it(std::move(other.it)), type(other.type), valued(other.valued) {
        other.type = typeid(void);
        other.valued = !1;
      }

      // ---

      value& operator=(const value &other) {
        if (this != &other) {
          it = other.it ? other.it->clone() : nullptr;
          valued = other.valued;
          type = other.type;
        }

        return *this;
      }

      value& operator=(value &&other) noexcept {
        if (this != &other) {
          it = std::move(other.it);
          valued = other.valued;
          type = other.type;

          other.type = typeid(void);
          other.valued = !1;
        }

        return *this;
      }

      // ---

      // Equality operator to compare with std::type_info
      bool operator==(const std::type_info& other_type) const {
        return type == other_type;
      }

      bool operator==(const pstd::type& input) const {
        return type == input;
      }

      // Equality operator to compare two pstd::value instances
      bool operator==(const value& other) const {
        if (this == &other)
          return !0;
        
        if (type != other.type)
          return !1;
        
        if (empty() && other.empty())
          return !0;
        
        if (empty() || other.empty())
          return !1;
        
        return it->equals(*other.it);
      }

      bool operator!=(const value& other) const {
        return !(*this == other);
      }

      // ---

      bool empty() const {
        return !valued;
      }

      template <typename T>
      T cast() const {
        if (empty())
          throw logic_error("value is empty");

        if (type != typeid(T))
          throw invalid_cast("invalid type provided");

        auto* h = dynamic_cast<const holder<T>*>(it.raw());

        if (!h)
          throw invalid_cast("dynamic cast failed");

        return h->it;
      }

      template <typename T>
      T& cast() {
        if (empty())
          throw logic_error("value is empty");

        if (type != typeid(T))
          throw invalid_cast("invalid type provided");

        auto* h = dynamic_cast<holder<T>*>(it.raw());

        if (!h)
          throw invalid_cast("dynamic cast failed");

        return h->it;
      }

      void reset() {
        type = typeid(void);
        valued = !1;
        it.reset(nullptr);
      }

      template <typename T>
      bool is_type() const {
        return type == typeid(T);
      }
  };
}
