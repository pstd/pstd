#pragma once

namespace pstd {
  class toss {
    public:
      template <typename T>
      toss& operator=(const T&) { // Discard the value
        return *this;
      }

      // Allow chaining assignments for toss
      toss& operator=(const toss&) {
        return *this;
      }
  };

  // ---

  [[maybe_unused]] static toss ignore;
}