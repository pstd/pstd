#pragma once

#include "./variant.hpp"
#include "./maybe.hpp"
#include "./list.hpp"
#include "./map.hpp"
#include "./fs.hpp"

#include <cstddef>

namespace pstd {
  static bool __json_reporting = !1;

  class json {
    private:
      using object_type = map<text, json>;

      using array_type = list<json>;

      using variant_type = variant<
        std::nullptr_t,
        bool,
        uint,
        int,
        float,
        double,
        text,
        object_type,
        array_type
      >;

      // ---

      variant_type it;

      // ---

      template <typename T>
      T cast() const {
        return it.cast<T>();
      }

      // needed for if changes want to be made to the object
      template <typename T>
      T& cast() {
        return it.cast<T>();
      }

      // ---

      template <typename T>
      bool is_type() const {
        return it.is_type<T>();
      }

      // ---

      static void textify_add_indentation(text &result, text indentation, int spaces, int level) {
        if (spaces > 0)
          result += indentation.insert(' ', spaces * level);

        else
          result += text("").insert(' ', spaces * level);
      }

      static void textify_recursive(const json &json, text &result, text indentation, int spaces, int level) {
        if (json.is_null())
          result += "null";
        
        else if (json.is_bool())
          result += json.cast<bool>() ? "true" : "false";
        
        else if (json.is_uint())
          result += text::from(
            json.cast<uint>()
          );
        
        else if (json.is_int())
          result += text::from(
            json.cast<int>()
          );
        
        else if (json.is_float())
          result += text::from(
            json.cast<float>()
          );

        else if (json.is_double())
          result += text::from(
            json.cast<double>()
          );

        else if (json.is_text()) {
          result += json.cast<text>().replace("\"", "\\\"");
        }

        else if (json.is_object()) {
          auto object = json.cast<object_type>();

          result += "{";

          for (auto iter = object.begin(); iter != object.end(); ++iter) {
            if (iter != object.begin())
              result += ",";

            textify_add_indentation(
              result,
              indentation,
              spaces,
              level + 1
            );

            result += text("\"%s\":%s").format(
              iter->a,
              spaces > 0 ? " " : ""
            );

            if (iter->b.is_text())
              result += "\"";
            
            textify_recursive(
              iter->b,
              result,
              indentation,
              spaces,
              level + 1
            );

            if (iter->b.is_text())
              result += "\"";
          }

          textify_add_indentation(
            result,
            indentation,
            spaces,
            level
          );

          result += "}";
        }

        else if (json.is_array()) {
          auto array = json.cast<array_type>();

          result += "[";

          for (std::size_t i = 0; i < array.size(); ++i) {
            if (i != 0)
              result += ",";

            textify_add_indentation(
              result,
              indentation,
              spaces,
              level + 1
            );

            if (array[i].is_text())
              result += "\"";
            
            textify_recursive(
              array[i],
              result,
              indentation,
              spaces,
              level + 1
            );

            if (array[i].is_text())
              result += "\"";
          }

          textify_add_indentation(
            result,
            indentation,
            spaces,
            level
          );

          result += "]";
        }
      }

      // ---

      static void parse_log(const text& type, const text &input, std::size_t &pos) {
        if (!__json_reporting)
          return;

        auto part = input.slice(pos, 50);
        
        part.remove_whitespace();

        printf("%s: %s\n", type.raw(), part.raw());
      }

      static void parse_increment(const text &input, std::size_t &pos) {
        parse_log("increment", input, ++pos);
      }

      static void parse_skip_whitespace(const text& input, std::size_t& pos) {
        parse_log("skip_whitespace", input, pos);

        while (pos < input.length() && std::isspace(input[pos]))
          parse_increment(input, pos);
      }

      static json parse_object(const text& input, std::size_t& pos) {
        parse_log("parse_object", input, pos);

        parse_skip_whitespace(input, pos);

        if (input[pos] != '{')
          throw parse_error(
            text("parse_object: expected '{' at position %d")
              .format(pos)
              .raw()
          );
        
        parse_increment(input, pos);

        object_type obj;

        bool first = !0;

        while (pos < input.length() && input[pos] != '}') {
          if (!first) {
            parse_skip_whitespace(input, pos);

            // double check
            if (input[pos] == '}')
              break;

            if (input[pos] != ',')
              throw parse_error(
                text("parse_object: expected ',' at position %d")
                  .format(pos)
                  .raw()
              );
            
            parse_increment(input, pos);
          }

          parse_skip_whitespace(input, pos);

          if (first && input[pos] == ']')
            break;

          text key = parse_string(input, pos);

          parse_skip_whitespace(input, pos);

          if (input[pos] != ':')
            throw parse_error(
              text("parse_object: expected ':' at position %d")
                .format(pos).raw()
            );
          
          parse_increment(input, pos);

          parse_skip_whitespace(input, pos);

          json value = parse_json(input, pos);
        
          obj[key] = value;

          first = !1;
        }

        if (pos >= input.length() || input[pos] != '}')
          throw parse_error(
            text("parse_object: expected '}' at position %d")
              .format(pos)
              .raw()
          );
        
        parse_increment(input, pos);

        return obj;
      }

      static json parse_array(const text& input, std::size_t& pos) {
        parse_log("parse_array", input, pos);

        parse_skip_whitespace(input, pos);

        if (input[pos] != '[')
          throw parse_error(
            text("parse_array: expected '[' at position %d")
              .format(pos)
              .raw()
          );
        
        parse_increment(input, pos);

        array_type arr;

        bool first = !0;

        while (pos < input.length() && input[pos] != ']') {
          if (!first) {
            parse_skip_whitespace(input, pos);

            // double check
            if (input[pos] == ']')
              break;

            if (input[pos] != ',')
              throw parse_error(
                text("parse_array: expected ',' at position %d")
                  .format(pos)
                  .raw()
              );
            
            parse_increment(input, pos);
          }

          parse_skip_whitespace(input, pos);
          
          if (first && input[pos] == ']')
            break;

          json value = parse_json(input, pos);

          arr.push_back(value);

          first = !1;
        }

        if (pos >= input.length() || input[pos] != ']')
          throw parse_error(
            text("parse_array: expected ']' at position %d")
              .format(pos)
              .raw()
          );
        
        parse_increment(input, pos);
        
        return arr;
      }

      static json parse_string(const text &input, std::size_t &pos) {
        parse_log("parse_string", input, pos);

        parse_skip_whitespace(input, pos);

        if (input[pos] != '"')
          throw parse_error(
            text("parse_string: expected '\"' at position %d")
              .format(pos)
              .raw()
          );
        
        parse_increment(input, pos); // skip the "

        auto end = input.find('"', pos);
        auto start = pos;

        while (end != EOF) {
          if (input[end - 1] == '\\')
            end = input.find('"', end + 1);

          else
            break;
        }

        auto str = input.slice(start, end - start);

        pos = end;

        if (pos >= input.length() || input[pos] != '"')
          throw parse_error(
            text("parse_string: expected '\"' at position %d")
              .format(pos).raw()
          );
        
        parse_increment(input, pos);

        return text::replace(str, "\\\"", "\"");
      }

      static json parse_number(const text& input, std::size_t& pos) {
        parse_log("parse_number", input, pos);

        parse_skip_whitespace(input, pos);

        std::size_t start = pos;

        while (pos < input.length() && (input[pos] == '-' || input[pos] == '+' || input[pos] == '.' || (input[pos] >= '0' && input[pos] <= '9') || input[pos] == 'e' || input[pos] == 'E'))
          parse_increment(input, pos);

        if (start == pos)
          throw parse_error(
            text("parse_number: expected number at position %d")
              .format(pos)
              .raw()
          );

        // Convert string to number
        text i = input.slice(start, pos - start);

        if (text::valid<int>(i))
          return text::to<int>(i);

        else if (text::valid<float>(i))
          return text::to<float>(i);

        else
          return text::to<double>(i);
      }

      static json parse_boolean(const text& input, std::size_t& pos) {
        parse_log("parse_boolean", input, pos);

        parse_skip_whitespace(input, pos);

        if (input[pos] == 't') {
          if (pos + 4 <= input.length() && input.slice(pos, 4) == "true") {
            pos += 4;
            return !0;
          }
        }
        
        else if (input[pos] == 'f') {
          if (pos + 5 <= input.length() && input.slice(pos, 5) == "false") {
            pos += 5;
            return !1;
          }
        }

        throw parse_error(
          text("parse_boolean: expected 'true' or 'false' at position %d")
            .format(pos)
            .raw()
        );
      }

      static json parse_null(const text& input, std::size_t& pos) {
        parse_log("parse_null", input, pos);

        parse_skip_whitespace(input, pos);

        if (pos + 4 <= input.length() && input.slice(pos, 4) == "null") {
          pos += 4;
          return nullptr;
        }

        throw parse_error(
          text("parse_null: expected 'null' at position %d")
            .format(pos)
            .raw()
        );
      }

      static json parse_json(const text& input, std::size_t& pos) {
        parse_log("parse_json", input, pos);

        parse_skip_whitespace(input, pos);

        if (pos >= input.length())
          throw parse_error(
            text("parse_json: unexpected end of input")
              .raw()
          );

        if (input[pos] == ']' || input[pos] == '}')
          throw parse_error(
            text("recieved unexpected character: %s")
              .format(input[pos])
              .raw()
          );
        
        switch (input[pos]) {
          case '{':
            return parse_object(input, pos);

          case '[':
            return parse_array(input, pos);

          case '"':
            return parse_string(input, pos);

          case 't':
          case 'f':
            return parse_boolean(input, pos);

          case 'n':
            return parse_null(input, pos);

          default:
            return parse_number(input, pos);
        }
      }

      // ---

      maybe<json> get_recursive(const text &input, std::size_t &pos) const {
        if (pos >= input.length())
          return {};

        if (is_object()) {
          const auto &object = it.cast<object_type>();

          std::size_t dot_pos = input.find('.', pos);
          std::size_t bracket_pos = input.find('[', pos);
          std::size_t end_pos = std::min(dot_pos, bracket_pos);

          if (end_pos == EOF)
            end_pos = input.length();

          text key = input.slice(pos, end_pos - pos);
          pos = end_pos;

          if (!object.contains(key))
            return {};

          const json &obj = object.at(key);

          if (pos < input.length()) {
            if (input[pos] == '.') ++pos;
            else if (input[pos] == '[') ++pos;
            return obj.get_recursive(input, pos);
          }

          return obj;
        }

        if (is_array()) {
          if (input[pos] != '[')
            return {};

          std::size_t bracket_close = input.find(']', pos);

          if (bracket_close == EOF)
            return {};

          std::size_t index = text::to<int>(
            input.slice(pos + 1, bracket_close - pos - 1)
          );
          
          pos = bracket_close + 1;

          const auto &array = it.cast<array_type>();

          if (index >= array.size())
            return {};

          const json &arr_elem = array.at(index);

          if (pos < input.length()) {
            if (input[pos] == '.') ++pos;
            else if (input[pos] == '[') ++pos;
            return arr_elem.get_recursive(input, pos);
          }

          return arr_elem;
        }

        return {};
      }

    public:
      pstd_error(parse_error);
      pstd_error(clear_error);
      pstd_error(open_error);
      pstd_error(get_error);
      pstd_error(set_error);

      // ---

      static void reporting(bool enabled = !0) {
        __json_reporting = enabled;
      }

      // ---

      static json object(object_type input) {
        return json(input);
      }

      static json array(array_type input) {
        return json(input);
      }

      static json parse(const text &input) {
        std::size_t pos = 0;
        return parse_json(input, pos);
      }

      static json open(const text &path) {
        auto content = fs::open(path);

        if (!content)
          throw open_error(
            text("unable to open %s")
              .format(path)
              .raw()
          );

        return parse(*content);
      }

      // ---

      static json merge(const json &a, const json &b) {
        if (a.is_object() && b.is_object()) {
          auto obj_a = a.cast<object_type>();
          auto obj_b = b.cast<object_type>();

          for (auto [key, value] : obj_b)
            obj_a[key] = value;

          return obj_a;
        }

        if (a.is_array() && b.is_array()) {
          auto arr_a = a.cast<array_type>();
          auto arr_b = b.cast<array_type>();

          for (auto &value : arr_b)
            arr_a.push_back(value);

          return arr_a;
        }

        return b;
      }

      // ---

      operator bool() const {
        return !it.empty();
      }

      operator text() const {
        return textify(0, "\n");
      }

      // ---

      json(std::initializer_list<object_type::value_type> init) {
        it = object_type(init);
      }

      json(std::initializer_list<array_type::value_type> init) {
        it = array_type(init);
      }

      json(const object_type &input): it(input) {

      }

      json(const array_type &input): it(input) {

      }

      json(std::nullptr_t input): it(input) {

      }

      json(const text &input): it(input) {

      }

      json(double input) : it(input) {

      }

      template <typename T>
      requires std::is_same_v<T, bool>
      json(T input): it(input) {

      }

      json(float input) : it(input) {

      }

      json(uint input): it(input) {
        
      }

      json(int input): it(input) {

      }

      json(): it() {

      }

      // ---

      json(const json &a, const json &b) {
        *this = merge(a, b);
      }

      // ---

      // copy constructor
      json(const json &other): it(other.it) {

      }

      // ---

      // equality operator
      bool operator==(const json &other) const {
        return other.it == it;
      }

      // move assignment operator
      json& operator=(json&& other) noexcept {
        if (this != &other)
          it = std::move(other.it);

        return *this;
      }

      // copy assignment operator
      json& operator=(const json& other) {
        if (this != &other)
          it = other.it;

        return *this;
      }

      // ---

      json& operator[](text::const_pointer key) {
        if (!is_object())
          throw get_error("JSON value is not an object");

        auto& obj = cast<object_type>();

        if (!obj.contains(key))
          obj[key] = json();

        return obj[key];
      }

      // Access an element by key
      json& operator[](const text& key) {
        if (!is_object())
          throw get_error("JSON value is not an object");

        auto& obj = cast<object_type>();

        if (!obj.contains(key))
          obj[key] = json();

        return obj[key];
      }

      // Access an element by key (const version)
      const json& operator[](const text& key) const {
        if (!is_object())
          throw get_error("JSON value is not an object");

        return cast<object_type>().at(key);
      }

      // Access an element by index
      json& operator[](std::size_t index) {
        if (!is_array())
          throw get_error("JSON value is not an array");

        return cast<array_type>()[index];
      }

      // Access an element by index (const version)
      const json& operator[](std::size_t index) const {
        if (!is_array())
          throw get_error("JSON value is not an array");

        return cast<array_type>().at(index);
      }

      // ---

      bool is_null() const {
        return is_type<std::nullptr_t>();
      }

      bool is_bool() const {
        return is_type<bool>();
      }

      bool is_uint() const {
        return is_type<uint>();
      }

      bool is_int() const {
        return is_type<int>();
      }

      bool is_float() const {
        return is_type<float>();
      }

      bool is_double() const {
        return is_type<double>();
      }

      bool is_text() const {
        return is_type<text>();
      }

      bool is_object() const {
        return is_type<object_type>();
      }

      bool is_array() const {
        return is_type<array_type>();
      }

      bool empty() const {
        return it.empty();
      }

      // ---

      text textify(int spaces, text indentation = "") const {
        text result;

        textify_recursive(*this, result, indentation, spaces, 0);

        return result;
      }

      // ---

      list<text> keys() const {
        if (!is_object())
          throw get_error("keys(): json type is not an object");

        auto obj = cast<object_type>();

        list<text> keys;

        for (auto [key, value] : obj)
          keys.push_back(key);

        return keys;
      }

      std::size_t size() const {
        if (!is_array())
          throw get_error("size(): json type is not an array");

        return cast<array_type>().size();
      }

      // ---

      void push_back(const json& value) {
        if (!is_array())
          throw set_error("push_back(): json type is not an array");

        cast<array_type>().push_back(value);
      }

      // --- 

      // maybe<const json> get(text input) const {
      //   std::size_t pos = 0;
      //   return get_recursive(input, pos);
      // }

      maybe<json> get(text input) const {
        std::size_t pos = 0;
        return get_recursive(input, pos);
      }

      // ---

      // maybe<const json> get(int i) const {
      //   if (i >= size())
      //     throw get_error("get(int): outside of range");

      //   auto input = text("[%d]").format(i);
      //   std::size_t pos = 0;

      //   return get_recursive(input, pos);
      // }

      maybe<json> get(int i) const {
        if (i >= size())
          throw get_error("get(int): outside of range");
          
        auto input = text("[%d]").format(i);
        std::size_t pos = 0;

        return get_recursive(input, pos);
      }

      // ---

      template <typename T>
      requires type_allowed_v<T, std::nullptr_t, bool, uint, int, float, double, text>
      T value() const {
        return cast<T>();
      }

      // ---

      template <typename T>
      requires type_allowed_v<T, std::nullptr_t, bool, uint, int, float, double, text>
      auto value(text input) {
        std::size_t pos = 0;
        auto result = get_recursive(input, pos);
        
        if (!result)
          throw get_error(
            text("value(): No such value: %s")
              .format(input)
              .raw()
          );
          
        return result->cast<T>();
      }

      template <typename T>
      requires type_allowed_v<T, std::nullptr_t, bool, uint, int, float, double, text>
      auto value(text input) const {
        std::size_t pos = 0;
        auto result = get_recursive(input, pos);

        if (!result)
          throw get_error(
            text("value(): No such value: %s")
              .format(input)
              .raw()
          );

        return result->cast<T>();
      }

      // ---

      template <typename T>
      requires type_allowed_v<T, std::nullptr_t, bool, uint, int, float, double, text>
      auto value(int i) {
        if (i >= size())
          throw get_error("value(int): outside of range");
          
        auto input = text("[%d]").format(i);
        std::size_t pos = 0;

        auto result = get_recursive(input, pos);
        
        if (!result)
          throw get_error(
            text("value(int): No such value: %s")
              .format(input)
              .raw()
          );
          
        return result->cast<T>();
      }

      template <typename T>
      requires type_allowed_v<T, std::nullptr_t, bool, uint, int, float, double, text>
      auto value(int i) const {
        if (i >= size())
          throw get_error("value(int): outside of range");
          
        auto input = text("[%d]").format(i);
        std::size_t pos = 0;

        auto result = get_recursive(input, pos);

        if (!result)
          throw get_error(
            text("value(int): No such value: %s")
              .format(input)
              .raw()
          );

        return result->cast<T>();
      }
  };
}