#pragma once

#include "./funktion.hpp"
#include "./triple.hpp"
#include "./value.hpp"
#include "./text.hpp"

#include <utility>

#if defined(emit) // qt will interfere with this
  #undef emit
#endif

namespace pstd {
  class events {
    private:
      triple<bool, text, value> list;

      // ---

    public:
      pstd_error(invalid_cast);

      // ---

      // copy assignment operator
      events& operator=(const events& other) {
        if (this != &other)
          list = other.list;

        return *this;
      }

      // move assignment operator
      events& operator=(events&& other) noexcept {
        if (this != &other)
          list = std::move(other.list);

        return *this;
      }

      // ---

      template <typename ...A>
      void once(text ident, funktion<void(A...)> funkt) {
        list.push_back({
          !0,
          ident,
          funkt
        });
      }

      template <typename ...A>
      void on(text ident, funktion<void(A...)> funkt) {
        list.push_back({
          !1,
          ident,
          funkt
        });
      }

      // ---

      template <lambda_c L>
      void once(text ident, L&& lambda) {
        once(ident, funktion(std::forward<L>(lambda)));
      }

      template <lambda_c L>
      void on(text ident, L&& lambda) {
        on(ident, funktion(std::forward<L>(lambda)));
      }

      // ---

      template <typename ...A>
      void emit(text ident, A ...args) {
        remove(list, [&](triplet<bool, text, value> &next) -> bool {
          if (next.b != ident)
            return !1;

          try {
            funktion<void(A...)> func = next.c.cast<funktion<void(A...)>>();

            func(args ...);

            return next.a;
          }

          catch (value::invalid_cast &err) {
            throw invalid_cast(
              text("invalid event type cast provided for event \"%s\"")
                .format(ident)
                .raw()
            );
          }

          catch (...) {
            throw;
          }
        });
      }

      template <typename ...A>
      void operator()(text ident, A ...args) {
        emit(ident, args ...);
      }
  };
}