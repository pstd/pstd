#pragma once

#include "./list.hpp"

namespace pstd {
  template <typename A, typename B, typename C>
  struct triplet {
    public:
      A a;
      B b;
      C c;

      template <typename T1, typename T2, typename T3>
      triplet(T1 t1, T2 t2, T3 t3): a(t1), b(t2), c(t3) {

      }

      triplet(A a, B b, C c): a(a), b(b), c(c) {

      }

      triplet() = default;

      // ---

      bool operator==(const triplet &other) const {
        return other.a == a && 
          other.b == b &&
          other.c == c;
      }

      bool operator!=(const triplet &other) const {
        return !(*this == other);
      }
  };

  template <typename A, typename B, typename C>
  using triple = list<triplet<A, B, C>>;
}